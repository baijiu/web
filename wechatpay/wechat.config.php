<?php


define('IN_ECTOUCH', true);
require_once ("../include/init.php");

//获得微信支付配置信息
$info = $db->getOne("select pay_config from ecs_touch_payment where pay_id=9 and enabled = 1");
$info = unserialize($info);
if(empty($info)){
    $smarty->assign('ectouch_themes', '/themes/' . $_CFG['template']);
    show_message('支付插件未安装！');
}

$appid = $info[0]['value'];
$mchid= $info[1]['value'];
$key = $info[2]['value'];
$appsecret = $info[3]['value'];


define("WXAPPID", $appid);//公众号的appid
define("WXMCHID", $mchid);//微信支付商户号
define("WXKEY", $key);//微信支付的密钥
define("WXAPPSECRET", $appsecret);//公众号的密钥
define("WXCURL_TIMEOUT", 30);
define('WXNOTIFY_URL', "http://".$_SERVER['HTTP_HOST']."/wechatpay/notify_url.php");//通知地址

define('WXSSLCERT_PATH',dirname(__FILE__).'/WxPayPubHelper/cacert/apiclient_cert.pem');
define('WXSSLKEY_PATH',dirname(__FILE__).'/WxPayPubHelper/cacert/apiclient_key.pem');


?>