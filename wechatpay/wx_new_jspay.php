<?php


      require_once("wechat.config.php");
      
      require_once(dirname(__FILE__)."/WxPayPubHelper/WxPayPubHelper.php");
      $jsApi = new JsApi_pub();

        /* 查询：判断是否登录 */
        if ($_SESSION['user_id'] <= 0)
        {
            $smarty->assign('ectouch_themes', '/themes/' . $_CFG['template']);
            show_message("请先登录再充值！", array("立即登录"), array("user.php"), 'error');
        }
        $order_sn = $_GET['order_sn'];

        $order = $db->getrow("select * from ecs_recharge where order_sn = '".$order_sn."' and paystate=0");
        if(empty($order)){
            $smarty->assign('ectouch_themes', '/themes/' . $_CFG['template']);
            show_message('订单不存在！');
        }



/*****************步骤一**********/
        if (!isset($_GET['code']))
        {
            $redirect = urlencode($GLOBALS['ecs']->url().'wx_new_jspay.php?order_sn='.$order_sn);
            $url = $jsApi->createOauthUrlForCode($redirect);
            Header("Location: $url"); 
        }
		else
        {
            $code = $_GET['code'];
            $jsApi->setCode($code);
            $openid = $jsApi->getOpenId();
        }
       /*****************步骤二**********/ 
        if($openid)
        {
            $out_trade_no = $order_sn.'-'.time();
            $unifiedOrder = new UnifiedOrder_pub();
            $unifiedOrder->setParameter("openid","$openid");//商品描述
            $unifiedOrder->setParameter("body",$order['user_id']);//商品描述
            $unifiedOrder->setParameter("out_trade_no",$out_trade_no);//商户订单号
            $unifiedOrder->setParameter("total_fee",$order['amount']*100);//总金额
            $unifiedOrder->setParameter("notify_url",WXNOTIFY_URL);//通知地址 
            $unifiedOrder->setParameter("trade_type","JSAPI");//交易类型


            $prepay_id = $unifiedOrder->getPrepayId();

            $jsApi->setPrepayId($prepay_id);

            $jsApiParameters = $jsApi->getParameters();
			
		}
?>
<!--*************步骤三****************-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/> 
    <title>微信支付-支付</title>
    <script type="text/javascript">
	function jsApiCall(){
		WeixinJSBridge.invoke(
			'getBrandWCPayRequest',
			<?php echo $jsApiParameters;?>,
			function(res){
				WeixinJSBridge.log(res.err_msg);
				if(res.err_msg == 'get_brand_wcpay_request:ok'){
					window.location.href='/user.php';
					
				}else{
                    window.history.go(-1);
                }
			}
		);
	}
	function callpay(){
		
		if (typeof WeixinJSBridge == "undefined"){
		    if( document.addEventListener ){
		        document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
		    }else if (document.attachEvent){
		        document.attachEvent('WeixinJSBridgeReady', jsApiCall); 
		        document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
		    }
		}else{
		    jsApiCall();
		}
	}
	</script>
	
</head>
<body  onload="callpay()">
</body>
</html>