<?php
class wechatCallbackapi 
{
	
	public function valid($db) 
	{
	
		$echoStr = $_GET["echostr"];
		if ($this -> checkSignature($db)) 
		{
			echo $echoStr;
		} 
	}
	public function msgError($error) 
	{
		$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
		if (isset($postStr)) 
		{
			$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
			$fromUsername = $postObj -> FromUserName;
			$msgType = $postObj -> MsgType;
			$toUsername = $postObj -> ToUserName;
			if($msgType=="voice")
			{
				$keyword = trim($postObj -> Recognition);
			}
			else
			{
				$keyword = trim($postObj -> Content);
			}
			$time = time();
			$textTpl = "<xml>
                            <ToUserName><![CDATA[%s]]></ToUserName>
                            <FromUserName><![CDATA[%s]]></FromUserName>
                            <CreateTime>%s</CreateTime>
                            <MsgType><![CDATA[%s]]></MsgType>
                            <Content><![CDATA[%s]]></Content>
                            <FuncFlag>0</FuncFlag>
                            </xml>";
			$contentStr = $error;
			$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
			echo $resultStr;
			exit;
		} 
	} 
	
	
	public function responseMsg($db, $user, $base_url) 
	{	
			
		$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
		$debug = 0;
		if ($_GET['debug'] == 1) 
		{
			$debug = 1;
		}
		if ($_GET['de_base']) 
		{
			$de_base = 1;
		}
		if (!empty($postStr) or $debug == 1) 
		{
			$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
			$fromUsername = $postObj -> FromUserName;
			$msgType = $postObj -> MsgType;
			$toUsername = $postObj -> ToUserName;
			$keyword = trim($postObj -> Content);
            //$this->text_log('7654321',$postObj);
			if (empty($keyword)) 
			{
				$keyword = $_GET['keyword'];
			}
			if (empty($fromUsername)) 
			{
				if ($_GET['wxid']) 
				{
					$fromUsername = $_GET['wxid'];
				} 
				else 
				{
					$fromUsername = 'oIM-ajhetL3OwUfIm2DNgC1pW9Uk';
				} 
			}
			$textTpl = "<xml>
                            <ToUserName><![CDATA[%s]]></ToUserName>
                            <FromUserName><![CDATA[%s]]></FromUserName>
                            <CreateTime>%s</CreateTime>
                            <MsgType><![CDATA[%s]]></MsgType>
                            <Content><![CDATA[%s]]></Content>
                            <FuncFlag>0</FuncFlag>
                        </xml>";
			$newsTpl = "<xml>
                         <ToUserName><![CDATA[%s]]></ToUserName>
                         <FromUserName><![CDATA[%s]]></FromUserName>
                         <CreateTime>%s</CreateTime>
                         <MsgType><![CDATA[%s]]></MsgType>
                         <ArticleCount>%s</ArticleCount>
                         <Articles>%s</Articles>
                         <FuncFlag>0</FuncFlag>
                         </xml>";
			$serviceTpl = "<xml>
                        <ToUserName><![CDATA[%s]]></ToUserName>
                        <FromUserName><![CDATA[%s]]></FromUserName>
                        <CreateTime>%s</CreateTime>
                        <MsgType><![CDATA[%s]]></MsgType>
                        </xml>";
			$imageTpl = "<xml>
                            <ToUserName><![CDATA[%s]]></ToUserName>
                            <FromUserName><![CDATA[%s]]></FromUserName>
                            <CreateTime>%s</CreateTime>
                            <MsgType><![CDATA[%s]]></MsgType>
                            <Image>
                            <MediaId><![CDATA[%s]]></MediaId>
                            </Image>
                            </xml>";
			$voiceTpl = "<xml>
                            <ToUserName><![CDATA[toUser]]></ToUserName>
                            <FromUserName><![CDATA[fromUser]]></FromUserName>
                            <CreateTime>12345678</CreateTime>
                            <MsgType><![CDATA[voice]]></MsgType>
                            <Voice>
                            <MediaId><![CDATA[media_id]]></MediaId>
                            </Voice>
                            </xml>";
						
			$time = time();
			$lang = array();
			$setp = $db -> getOne("SELECT `setp` FROM `wxch_user` WHERE `wxid` = '$fromUsername'");
			if($setp == 3)
			{
				$uname = $db -> getOne("SELECT `uname` FROM `wxch_user` WHERE `wxid` = '$fromUsername'");
			}
			if (empty($uname)) 
			{
				$postfix = '&wxid=' . $fromUsername;
			} 
			else 
			{
				$ret['wxid'] = $db -> getOne("SELECT `wxid` FROM `wxch_user` WHERE `wxid` = '$fromUsername'");
				$postfix = '&wxid=' . $ret['wxid'];
			}
			$base_ret = $db -> getRow("SELECT * FROM  `wxch_cfg` WHERE `cfg_name` = 'baseurl'");
			$imgpath_ret = $db -> getRow("SELECT * FROM  `wxch_cfg` WHERE `cfg_name` = 'imgpath'");
			$plustj_ret = $db -> getRow("SELECT * FROM  `wxch_cfg` WHERE `cfg_name` = 'plustj'");
			$cxbd = $db -> getOne("SELECT `cfg_value` FROM `wxch_cfg` WHERE `cfg_name` = 'cxbd'");
			$oauth_state = $db -> getOne("SELECT `cfg_value` FROM `wxch_cfg` WHERE `cfg_name` = 'oauth'");
			$goods_is_ret = $db -> getOne("SELECT `cfg_value` FROM `wxch_cfg` WHERE `cfg_name` = 'goods'");
			$article_url = $db -> getOne("SELECT `cfg_value` FROM `wxch_cfg` WHERE `cfg_name` = 'article'");
			$q_name = $db -> getOne("SELECT `autoreg_name` FROM `wxch_autoreg` WHERE `autoreg_id` = 1");
			if(empty($q_name))
			{
				$q_name="weixin";
			}
			$affiliate_id = $db -> getOne("SELECT `affiliate` FROM `wxch_user` WHERE `wxid` = '$fromUsername'");
			if ($affiliate_id >= 1) 
			{
				$affiliate = '&u=' . $affiliate_id;
			}
			if ($goods_is_ret == 'false')
			{
				$goods_is = ' AND is_delete = 0 AND is_on_sale = 1';
			}
			else
			{
				$goods_is = '';
			}
			$plustj = $plustj_ret['cfg_value'];
			$wxch_bd = $db -> getOne("SELECT `cfg_value` FROM `wxch_cfg` WHERE `cfg_name` = 'bd'");
			if (empty($base_ret['cfg_value'])) 
			{
				$m_url = $base_url ;
			}
			else 
			{
				$m_url = $base_ret['cfg_value'];
				$base_url = $base_ret['cfg_value'];
			}
			if ($de_base) 
			{
				echo $base_url;
			}
			$img_path = $imgpath_ret['cfg_value'];
			$base_img_path = $base_url;
			$oauth_location = $base_url . 'wechat/oauth/wxch_oauths.php?uri=';
			$thistable = $db -> prefix . 'users';
			//甜 心 新增显示会员账号密码
			$user_name=$db -> getOne("SELECT `user_name` FROM `$thistable` WHERE `wxid` ='$fromUsername'");
			$user_password=$db -> getOne("SELECT `password_tianxin` FROM `$thistable` WHERE `wxid` ='$fromUsername'");
			//甜心新增绑定多用户ID
            $ret = $db -> getRow("SELECT `user_id` FROM `$thistable` WHERE `user_name` ='$uname'");
			if (!empty($ret['user_id'])) 
			{
				$user_id = $ret['user_id'];
			}
			//是否已插入微信用户表
			$ret = $db -> getRow("SELECT `wxid` FROM `wxch_user` WHERE `wxid` = '$fromUsername'");
			if (empty($ret)) 
			{
				if (!empty($fromUsername)) 
				{	//插入微信用户
					$db -> query("INSERT INTO `wxch_user` (`subscribe`, `wxid` , `dateline`) VALUES ('1','$fromUsername','$time')");
				} 
			}
			else 
			{
				$db -> query("UPDATE  `wxch_user` SET  `subscribe` =  '1',`dateline` = '$time' WHERE  `wxch_user`.`wxid` = '$fromUsername';");
			}
			if ($msgType == 'text') 
			{
				$db -> query("INSERT INTO `wxch_message` (`wxid`, `message`, `dateline`) VALUES
( '$fromUsername', '$keyword', $time);");
			}
			$belong = $db -> insert_id();
			$thistable = $db -> prefix . 'users';
			//密码前缀
			$ec_pwd = $db -> getOne("SELECT `cfg_value` FROM `wxch_cfg` WHERE `cfg_name` = 'userpwd'");
			//随机位数
			$autoreg_rand = $db -> getOne("SELECT `autoreg_rand` FROM `wxch_autoreg` WHERE `autoreg_id` = 1");
			//按随机位数生成后几位密码
			$s_mima=$this->randomkeys($autoreg_rand);
			//前缀加后缀组合
			$ec_pwd=$ec_pwd.$s_mima;
            //新会员密码MD5格式
            $ec_pwd = md5($ec_pwd);
			//新会员密码
			$ec_pwd_no=$ec_pwd;
			$zhanghaoinfo="";
			//注册账号
			if (empty($uname)) 
			{
				$wxch_user_name_sql = "SELECT `user_name` FROM `$thistable` WHERE `wxch_bd`='ok' AND `wxid` = '$fromUsername'";
				$wxch_user_name = $db -> getOne($wxch_user_name_sql);
				/*甜心100修复开发*/
				$db->query("UPDATE `wxch_user` SET `setp`= 3,`uname`='$wxch_user_name' WHERE `wxid`= '$fromUsername';");
                if (empty($wxch_user_name))
                {
                    $autoreg_state = $db -> getOne("SELECT `state` FROM `wxch_autoreg` WHERE `autoreg_id` = 1");
                    if($autoreg_state)
                    {
                        $wxch_user_sql = "INSERT INTO `$thistable` ( `user_name`,`password`,`wxid`,`user_rank`,`wxch_bd`,`password_tianxin`,`reg_time`) VALUES ('$fromUsername','$ec_pwd','$fromUsername','0','ok','$ec_pwd_no','".time()."')";
                        $db -> query($wxch_user_sql);
                        //取得最新插入的id
                        $ecs_user_id = $db -> insert_id();
                        //前缀加ID组合成用户名
                        $ecs_user_name = $q_name . $ecs_user_id;
                        $ecs_update = " UPDATE `$thistable` SET `user_name` = '$ecs_user_name'  WHERE `user_id` = '$ecs_user_id'";
                        $db -> query($ecs_update);

                        //注册后默认绑定微信
                        $db->query("UPDATE `wxch_user` SET `setp`= 3,`uname`='$ecs_user_name' WHERE `wxid`= '$fromUsername';");
                        $zhanghaoinfo="您的账号：".$ecs_user_name."密码：".$ec_pwd_no;
                    }
                    else
                    {
                        $zhanghaoinfo="自动注册功能未开启！";
                    }
                }

			}

			//关注推送事件（包括搜索关注，扫二维码关注）
			if ($postObj -> Event == 'subscribe') 
			{
				//扫不带参数二维码关注或者直接搜索关注
				if (strlen($postObj -> EventKey) == 0) 
				{
				    $postObj -> EventKey = 'subscribe';
				}
				//扫带参数二维码关注 
				else 
				{
					$qrscene = $postObj -> EventKey;
					$scene_id_arr = explode("qrscene_", $qrscene);
					$scene_id = $scene_id_arr[1];
					$db -> query("UPDATE `wxch_qr` SET `subscribe`=`subscribe` + 1 WHERE `scene_id`= '$scene_id';");
					$scan_ret = $db -> getRow("SELECT * FROM `wxch_qr` WHERE scene_id =$scene_id");
					
					if ($scene_id >= 1) 
					{
						$postObj -> EventKey = 'affiliate_推荐成功_' . $scene_id;
					} 
					else 
					{
						$postObj -> EventKey = $scan_ret['function'];
					} 
					
				} 
			} 
			//取消关注推送事件
			elseif ($postObj -> Event == 'unsubscribe') 
			{
				$db -> query("UPDATE  `wxch_user` SET  `subscribe` =  '0' WHERE  `wxch_user`.`wxid` = '$fromUsername';");
			} 
			//已关注，扫带参数二维码推送事件
			elseif ($postObj -> Event == 'SCAN') 
			{
				$qrscene = $postObj -> EventKey;
				$scene_id = $qrscene;
				$update_sql = "UPDATE `wxch_qr` SET `scan`=`scan` + 1 WHERE `scene_id`= '$scene_id';";
				$db -> query("$update_sql");				
				$scan_ret = $db -> getRow("SELECT * FROM `wxch_qr` WHERE scene_id =$scene_id");
				if ($scan_ret['affiliate'] >= 1) 
				{
					$postObj -> EventKey = 'affiliate_推荐成功_' . $scan_ret['affiliate'];
				} 
				else 
				{
					$postObj -> EventKey = $scan_ret['function'];
				} 
			}

			if ($postObj -> MsgType == 'event') 
			{	
				//如果是事件给$keyword赋值
				$keyword = $postObj -> EventKey;
				$menu_message = 'menu:' . $keyword;
				$db -> query("INSERT INTO `wxch_message` (`wxid`, `message`, `dateline`) VALUES
( '$fromUsername', '$menu_message', $time);");
			}
			if ($postObj -> MsgType == 'voice') 
			{
				$keyword = $postObj -> Recognition;
				$menu_message = 'voice:' . $keyword;
				$db -> query("INSERT INTO `wxch_message` (`wxid`, `message`, `dateline`) VALUES
( '$fromUsername', '$menu_message', $time);");
			}

            /*************************************************************/
			$wxch_table = 'wxch_msg';
			$wxch_msg = $db -> getAll("SELECT * FROM  `$wxch_table`");
			foreach($wxch_msg as $k => $v) 
			{
				$commands[$k] = $v;
			}
			foreach($commands as $kk => $vv) 
			{
				$temp_msg = explode(" ", $vv['command']);
				if (in_array($keyword, $temp_msg)) 
				{
					$keyword = $vv['function'];
				} 
			}


			$this -> getauto($db, $keyword, $textTpl, $newsTpl, $base_url, $m_url, $fromUsername, $toUsername, $time, $article_url);

			//绑定上下级关系
			$aff_arr = explode('_', $keyword);
			if ($aff_arr[0] == 'affiliate') 
			{
			    //推荐人id
				if (!empty($aff_arr[2])) 
				{
					//选出推荐人
					$aff_query = "SELECT * FROM `ecs_users` WHERE `user_id` = $aff_arr[2]";
					$aff_db = $db -> getRow($aff_query);
					$flagetianxin100=true;
					//如果上级的信息是自己
					if($aff_db['wxid']==$fromUsername)
					{
					    $flagetianxin100=false;
					}
					else
					{
					    $flagetianxin100=true;
					}

					$aff_query = "SELECT user_id FROM `ecs_users` WHERE `wxid` = '$fromUsername'";
					$userid = $db -> getOne($aff_query);
                    $childinfo = $this->shengji($userid);
					$flag=true;
                    //验证关系：自己的下级不能是自己的上级
                    foreach($childinfo as $k=>$v)
                    {
                        if($v==$aff_arr[2])
                        {
                            $flag=false;
                        }
                    }
                    //判断我目前是否已绑定
                    $aff_query = "SELECT parent_id FROM `ecs_users` WHERE `wxid` = '$fromUsername'";
                    $parent_id = $db -> getOne($aff_query);
					//如果我没有上级，继续绑定
					if(empty($parent_id))
					{	
						//如果存在将要绑定的上级和我没有下级并且绑定的不是自己
						if (!empty($aff_db)&&$flag&&$flagetianxin100) 
						{
							//绑定会员账号
							$ecs_usertable = $db -> prefix . 'users';
							$aff_update = "UPDATE `$ecs_usertable` SET `parent_id` = '$aff_arr[2]' WHERE `wxid` = '$fromUsername';";
							$db -> query($aff_update);
							//绑定微信账号
							$wxch_usertable = 'wxch_user';
							$aff_update = "UPDATE `$wxch_usertable` SET `affiliate` = '$aff_arr[2]' WHERE `wxid` = '$fromUsername';";
							$db -> query($aff_update);

							$msgType = "text";
							$contentStr="欢迎您加入我们!";
							$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
							$this -> insert_wmessage($db, $fromUsername, $contentStr, $time, $belong);
							$this -> universal($fromUsername, $base_url);
							echo $resultStr;
							exit;						
						}
						else
						{
							$msgType = "text";
							$contentStr="推荐关系不合法可能情况1：推荐人不能是自己2：自己的下级（含伞下）不能成为自己的上级";
							$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
							$this -> insert_wmessage($db, $fromUsername, $contentStr, $time, $belong);
							$this -> universal($fromUsername, $base_url);
							echo $resultStr;
							exit;	
						}
					}
					else
					{
                        //查询上级昵称
                        $qu_wxid = "SELECT wxid FROM `ecs_users` WHERE `user_id` = '$parent_id'";
                        $parent_wxid = $db -> getOne($qu_wxid);
                        $qu_name = "SELECT nickname FROM `wxch_user` WHERE `wxid` = '$parent_wxid'";
                        $parent_name = $db -> getOne($qu_name);
                        $msgType = "text";
                        $contentStr="你已经有上级了哦，您的上级是".$parent_name;
                        //组XML
                        $resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
                        //插入会话记录
                        $this -> insert_wmessage($db, $fromUsername, $contentStr, $time, $belong);
                        $this -> universal($fromUsername, $base_url);
                        echo $resultStr;
                        exit;
					}					
				}  
			}

			if ($keyword == 'news')
			{
				$thistable = $db -> prefix . 'goods';
				$query_sql = "SELECT * FROM  `$thistable` WHERE `is_new` = 1 $goods_is ORDER BY sort_order, last_update DESC  LIMIT 0 , 1 ";
				$ret = $db -> getAll($query_sql);
				$ArticleCount = count($ret);
				$items = '';
				if ($ArticleCount >= 1) 
				{
					foreach($ret as $v) 
					{
						if ($img_path == 'local') 
						{
							$v['thumbnail_pic'] = $base_img_path . $v['goods_img'];
						} 
						elseif ($img_path == 'server') 
						{
							$v['thumbnail_pic'] = $v['goods_img'];
						} 
						if ($oauth_state == 'true') 
						{
							$goods_url = $oauth_location . $m_url . 'goods.php?id=' . $v['goods_id'] . $affiliate;
						} 
						elseif ($oauth_state == 'false') 
						{
							$goods_url = $m_url . 'goods.php?id=' . $v['goods_id'] . $postfix . $affiliate;
						}
						 
						$items.="<item><Title><![CDATA[".$v['goods_name']."]]></Title><PicUrl><![CDATA[".$v['thumbnail_pic']."]]></PicUrl><Url><![CDATA[".$goods_url."]]></Url></item>";
					}
					 
					$msgType = "news";
				}
				else
				{
						$titles="暂无最新商品";
						$site_url="http://".$_SERVER['HTTP_HOST']."/"; 
						$s_url=$site_url;
					
						$s_picurl=$site_url."/images/huanyin.jpg";
						$ArticleCount=1;
						$items .= "<item>
                 		<Title><![CDATA[" . $titles . "]]></Title>
                		<PicUrl><![CDATA[" . $s_picurl . "]]></PicUrl>
                		<Url><![CDATA[" . $s_url . "]]></Url>
                 		</item>";
						$msgType = "news";
				 }
				$resultStr = sprintf($newsTpl, $fromUsername, $toUsername, $time, $msgType, $ArticleCount, $items);
				$w_message = '图文消息';
				$this -> insert_wmessage($db, $fromUsername, $w_message, $time, $belong);
				$this -> plusPoint($db, $uname, $keyword, $fromUsername);
				$this -> universal($fromUsername, $base_url);
				echo $resultStr;
				exit;
			}
			 
			elseif ($keyword == 'best') 
			{
				$thistable = $db -> prefix . 'goods';
				$query_sql = "SELECT * FROM  `$thistable` WHERE `is_best` = 1 $goods_is ORDER BY sort_order, last_update DESC  LIMIT 0 , 1 ";
				$ret = $db -> getAll($query_sql);
				$ArticleCount = count($ret);
				$items = '';
				if ($ArticleCount >= 1) 
				{
					foreach($ret as $v) 
					{
						if ($img_path == 'local') 
						{
							$v['thumbnail_pic'] = $base_img_path . $v['goods_img'];
						} 
						elseif ($img_path == 'server') 
						{
							$v['thumbnail_pic'] = $v['goods_img'];
						} 
						if ($oauth_state == 'true') 
						{
							$goods_url = $oauth_location . $m_url . 'goods.php?id=' . $v['goods_id'] . $affiliate;
						} elseif ($oauth_state == 'false') 
						{
							$goods_url = $m_url . 'goods.php?id=' . $v['goods_id'] . $postfix . $affiliate;
						} 
						
						$items .= "<item>
                 		<Title><![CDATA[" . $v['goods_name'] . "]]></Title>
                 		<PicUrl><![CDATA[" . $v['thumbnail_pic'] . "]]></PicUrl>
                 		<Url><![CDATA[" . $goods_url . "]]></Url>
                 		</item>";
					}
					 
					$msgType = "news";
				} 
				else
				{
						$titles="暂无热销商品";
						$site_url="http://".$_SERVER['HTTP_HOST']."/"; 
						$s_url=$site_url;
						
						$s_picurl=$site_url."/images/huanyin.jpg";
						$ArticleCount=1;
						$items .= "<item>
                 		<Title><![CDATA[" . $titles . "]]></Title>
                		<PicUrl><![CDATA[" . $s_picurl . "]]></PicUrl>
                		<Url><![CDATA[" . $s_url . "]]></Url>
                 		</item>";
						$msgType = "news";
				 }
				$resultStr = sprintf($newsTpl, $fromUsername, $toUsername, $time, $msgType, $ArticleCount, $items);
				$w_message = '图文消息';
				$this -> insert_wmessage($db, $fromUsername, $w_message, $time, $belong);
				$this -> plusPoint($db, $uname, $keyword, $fromUsername);
				$this -> universal($fromUsername, $base_url);
				echo $resultStr;
				exit;
			}
			elseif ($keyword == 'hot') 
			{
				$thistable = $db -> prefix . 'goods';
				$ret = $db -> getAll("SELECT * FROM  `$thistable` WHERE `is_hot` = 1 $goods_is ORDER BY sort_order, last_update DESC  LIMIT 0 , 1");
				$ArticleCount = count($ret);
				$items = '';
				if ($ArticleCount >= 1) 
				{
					foreach($ret as $v) 
					{
						if ($img_path == 'local') 
						{
							$v['thumbnail_pic'] = $base_img_path . $v['goods_img'];
						} 
						elseif ($img_path == 'server') 
						{
							$v['thumbnail_pic'] = $v['goods_img'];
						} 
						if ($oauth_state == 'true') 
						{
							$goods_url = $oauth_location . $m_url . 'goods.php?id=' . $v['goods_id'] . $affiliate;
						} 
						elseif ($oauth_state == 'false') 
						{
							$goods_url = $m_url . 'goods.php?id=' . $v['goods_id'] . $postfix . $affiliate;
						} 
						$items .= "<item>
                 		<Title><![CDATA[" . $v['goods_name'] . "]]></Title>
                 		<PicUrl><![CDATA[" . $v['thumbnail_pic'] . "]]></PicUrl>
                 		<Url><![CDATA[" . $goods_url . "]]></Url>
                 		</item>";
					}
					 
					$msgType = "news";
				} 
				else
				{
						$titles="暂无精品推荐";
						$site_url="http://".$_SERVER['HTTP_HOST']."/"; 
						$s_url=$site_url;
						$s_picurl=$site_url."/images/huanyin.jpg";
						$ArticleCount=1;
						$items .= "<item>
                 		<Title><![CDATA[" . $titles . "]]></Title>
                		<PicUrl><![CDATA[" . $s_picurl . "]]></PicUrl>
                		<Url><![CDATA[" . $s_url . "]]></Url>
                 		</item>";

						$msgType = "news";
				 }
				$resultStr = sprintf($newsTpl, $fromUsername, $toUsername, $time, $msgType, $ArticleCount, $items);
				$w_message = '图文消息';
				$this -> insert_wmessage($db, $fromUsername, $w_message, $time, $belong);
				$this -> plusPoint($db, $uname, $keyword, $fromUsername);
				$this -> universal($fromUsername, $base_url);
				echo $resultStr;
				exit;
			}


			elseif ($keyword == 'help' or $keyword == 'HELP')
			{
				$msgType = "text";
				$lang['help'] = $db -> getOne("SELECT `lang_value` FROM `wxch_lang` WHERE `lang_name` = 'help'");
				$contentStr = $lang['help'];
				$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
				$this -> insert_wmessage($db, $fromUsername, $contentStr, $time, $belong);
				$this -> plusPoint($db, $uname, $keyword, $fromUsername);
				$this -> universal($fromUsername, $base_url);
				echo $resultStr;
				exit;
			}
			
			elseif ($keyword == 'dzp') 
			{
				$data = $this -> dzp($db, $base_url, $fromUsername);
				$msgType = "news";
				$resultStr = sprintf($newsTpl, $fromUsername, $toUsername, $time, $msgType, $data['ArticleCount'], $data['items']);
				$w_message = '图文消息';
				$this -> insert_wmessage($db, $fromUsername, $w_message, $time, $belong);
				$this -> plusPoint($db, $uname, $keyword, $fromUsername);
				$this -> universal($fromUsername, $base_url);
				echo $resultStr;
				exit;
			} 

			
			elseif ($keyword == 'zjd') 
			{
				$data = $this -> egg($db, $base_url, $fromUsername);
				$msgType = "news";
				$resultStr = sprintf($newsTpl, $fromUsername, $toUsername, $time, $msgType, $data['ArticleCount'], $data['items']);
				$w_message = '图文消息';
				$this -> insert_wmessage($db, $fromUsername, $w_message, $time, $belong);
				$this -> plusPoint($db, $uname, $keyword, $fromUsername);
				$this -> universal($fromUsername, $base_url);
				echo $resultStr;
				exit;
			}
			
			elseif ($keyword == 'qrcode') 
			{
				$ArticleCount = 1;
				$scene_id = $user_id;
				$affiliate= $user_id;
				$gourl = $base_url . 'wechat/egg/index1.php?scene_id=' . $scene_id;
				$type = 'tj';
				$qr_path = $db->getOne("SELECT `qr_path` FROM `wxch_qr` WHERE `scene_id`='$scene_id'");
				$user_name = $db->getOne("SELECT `user_name` FROM `ecs_users` WHERE `user_id`='$scene_id'");
				$scene=$user_name;
				if(!empty($qr_path))
				{
					 $surl=$qr_path;
				}
				else
				{
					$action_name="QR_LIMIT_SCENE";
					$json_arr = array('action_name'=>$action_name,'action_info'=>array('scene'=>array('scene_id'=>$scene_id)));
					$data = json_encode($json_arr);
					$this -> access_token($db);
					$ret = $db->getRow("SELECT `access_token` FROM `wxch_config`");
					$access_token = $ret['access_token'];
					if(strlen($access_token) >= 64) 
					{
						$url = 'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token='.$access_token;
						$res_json =$this -> curl_grab_page($url, $data);
						$json = json_decode($res_json);
					}
					$ticket = $json->ticket;
					if($ticket)
					{
						$ticket_url = urlencode($ticket);
						$ticket_url = 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='.$ticket_url;
						$imageinfo=$this -> downloadimageformweixin($ticket_url);
						$time = time();	
						$url=$_SERVER['HTTP_HOST'];			
						$path = '../images/qrcode/'.$time.'.jpg';
						$surl="http://".$url.'/images/qrcode/'.$time.'.jpg';
						$local_file=fopen($path,'a');
		
						if(false !==$local_file)
						{
							if(false !==fwrite($local_file,$imageinfo))
							{
								fclose($local_file);
								//将生成的二维码图片的地址放到数据库中
								$insert_sql = "INSERT INTO `wxch_qr` (`type`,`action_name`,`ticket`, `scene_id`, `scene` ,`qr_path`,`function`,`affiliate`,`endtime`,`dateline`) VALUES
					('$type','$action_name', '$ticket','$scene_id', '$scene' ,'$surl',0,'$affiliate','$endtime','$dateline')";
								$db->query($insert_sql);
							}
						}
					}
				}
				$des="扫描二维码可以获得推荐关系！";
				$items = "<item>
				<Title><![CDATA[推荐二维码]]></Title>
				<Description><![CDATA[" . $des . "]]></Description>
				<PicUrl><![CDATA[" . $surl . "]]></PicUrl>
				<Url><![CDATA[" . $gourl . "]]></Url>
				</item>";
				$msgType = "news";
				$resultStr = sprintf($newsTpl, $fromUsername, $toUsername, $time, $msgType, $ArticleCount, $items);
				$w_message = '图文消息';
				$this -> insert_wmessage($db, $fromUsername, $w_message, $time, $belong);
				$this -> plusPoint($db, $uname, $keyword, $fromUsername);
				$this -> universal($fromUsername, $base_url);
				echo $resultStr;
				exit;

			}
			

			elseif ($keyword == 'subscribe') 
			{
				
				/* 甜  心 10 0  新 增修 复  如果是直接关注的则默认分配上级账号避免推荐关系混乱*/
				$affiliate = unserialize($GLOBALS['_CFG']['affiliate']);
				$parent_id=(float)$affiliate['config']['parent_id'];
				$db->query("UPDATE  ecs_users SET `parent_id`='$parent_id' WHERE `user_name`='$ecs_user_name'");
			
				$type1 = $db -> getOne("SELECT `type` FROM `wxch_keywords1` WHERE `is_start` = 1");
				if($type1==3)
				{
					$keyword="关注回复文本";
				}
				else
				{
					$keyword="关注回复图文";
				}
				
				$this ->getauto_reg($db, $keyword, $textTpl, $newsTpl, $base_url, $m_url, $fromUsername, $toUsername, $time, $article_url,$user_name,$ec_pwd_no,$zhanghaoinfo,$user_password);
			} 


			if (!empty($keyword)) 
			{
				$this -> getauto($db, $keyword, $textTpl, $newsTpl, $base_url, $m_url, $fromUsername, $toUsername, $time, $article_url);
				$thistable = $db -> prefix . 'goods';
				$goods_name = $keyword;
				$search_sql = "SELECT * FROM  `$thistable` WHERE  `goods_name` LIKE '%$goods_name%' $goods_is ORDER BY sort_order, last_update DESC LIMIT 0,1";
				$ret = $db -> getAll($search_sql);
				$ArticleCount = count($ret);
				$items = '';
				if ($ArticleCount >= 1) 
				{
					foreach($ret as $v) 
					{
						if ($img_path == 'local') 
						{
							$v['thumbnail_pic'] = $base_img_path . $v['goods_img'];
						} 
						elseif ($img_path == 'server') 
						{
							$v['thumbnail_pic'] = $v['goods_img'];
						} 
						if ($oauth_state == 'true') 
						{
							$goods_url = $oauth_location . $m_url . 'goods.php?id=' . $v['goods_id'] . $affiliate;
						} 
						elseif ($oauth_state == 'false') 
						{
							$goods_url = $m_url . 'goods.php?id=' . $v['goods_id'] . $postfix . $affiliate;
						} 
						$items .= "<item>
                 		<Title><![CDATA[" . $v['goods_name'] . "]]></Title>
                 		<PicUrl><![CDATA[" . $v['thumbnail_pic'] . "]]></PicUrl>
                 		<Url><![CDATA[" . $goods_url . "]]></Url>
                 		</item>";
					}
					 
					$msgType = "news";
				} 
				else 
				{
					$msgType = "text";
					$thistable = $db -> prefix . 'goods';
					if ($plustj == 'true') 
					{
						$tj_str = $this -> plusTj($db, $m_url, $postfix, $oauth_location, $oauth_state, $goods_is, $affiliate);
						$contentStr = '没有搜索到"' . $goods_name . '"的商品' . $tj_str;
					} 
					elseif ($plustj == 'false') 
					{
						exit;
					} 
					$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
					$this -> insert_wmessage($db, $fromUsername, $contentStr, $time, $belong);
					$this -> plusPoint($db, $uname, $keyword, $fromUsername);
					$this -> universal($fromUsername, $base_url);
					echo $resultStr;
					exit;
				}
				$resultStr = sprintf($newsTpl, $fromUsername, $toUsername, $time, $msgType, $ArticleCount, $items);
				$w_message = '图文消息';
				$this -> insert_wmessage($db, $fromUsername, $w_message, $time, $belong);
				$this -> plusPoint($db, $uname, $keyword, $fromUsername);
				$this -> universal($fromUsername, $base_url);
				echo $resultStr;
				exit;
			}
		} 
		else 
		{
			echo "";
			exit;
		} 
	} 
	

	protected function plusPoint($db, $uname, $keyword, $fromUsername) 
	{
		$res_arr = array();
		$sql = "SELECT * FROM `wxch_point_record` WHERE `point_name` = '$keyword' AND `wxid` = '$fromUsername'";
		$record = $db -> getRow($sql);
		//甜心100修复积分赠送次数限制
		$num = $db -> getOne("SELECT `point_num` FROM `wxch_point` WHERE `point_name` = '$keyword'");
		$lasttime = time();
		if (empty($record)) 
		{
			$dateline = time();
			$insert_sql = "INSERT INTO `wxch_point_record` (`wxid`, `point_name`, `num`, `lasttime`, `datelinie`) VALUES
('$fromUsername', '$keyword' , 1, $lasttime, $dateline);";
			$potin_name = $db -> getOne("SELECT `point_name` FROM `wxch_point` WHERE `point_name` = '$keyword'");
			if (!empty($potin_name)) 
			{
				$db -> query($insert_sql);
			} 
		} 
		else 
		{
			$qdtoday = $db -> getOne("SELECT `lang_value` FROM `wxch_lang` WHERE `lang_name` = 'qdtoday'");
			if (empty($qdtoday)) 
			{
				$qdtoday = '今天您已经签到了，明天再来赚积分吧';
			} 
			$time = time();
			$lasttime_sql = "SELECT `lasttime` FROM `wxch_point_record` WHERE `point_name` = '$keyword' AND `wxid` = '$fromUsername'";
			$db_lasttime = $db -> getOne($lasttime_sql);
			if (($time - $db_lasttime) > (60 * 60 * 24)) 
			{
				$update_sql = "UPDATE `wxch_point_record` SET `num` = 0,`lasttime` = '$lasttime' WHERE `wxid` ='$fromUsername';";
				$db -> query($update_sql);
			} 
			$record_num = $db -> getOne("SELECT `num` FROM `wxch_point_record` WHERE `point_name` = '$keyword' AND `wxid` = '$fromUsername'");
			if ($record_num < $num) 
			{
				$update_sql = "UPDATE `wxch_point_record` SET `num` = `num`+1,`lasttime` = '$lasttime' WHERE `point_name` = '$keyword' AND `wxid` ='$fromUsername';";
				$db -> query($update_sql);
			} 
			else 
			{
				$qdno = $db -> getOne("SELECT `lang_value` FROM `wxch_lang` WHERE `lang_name` = 'qdno'");
				if (empty($qdno)) 
				{
					$qdno = '签到数次已用完';
				} 
				$res_arr['errmsg'] = 'no';
				$res_arr['contentStr'] = $qdno;
				return $res_arr;
			} 
		} 
		$wxch_table = 'wxch_point';
		$wxch_points = $db -> getAll("SELECT * FROM  `$wxch_table`");
		foreach($wxch_points as $k => $v) 
		{
			if ($v['point_name'] == $keyword) 
			{
				if ($v['autoload'] == 'yes') 
				{
					$points = $v['point_value'];
					$thistable = $db -> prefix . 'users';
					if (!empty($uname)) 
					{
						$sql = "UPDATE `$thistable` SET `pay_points` = `pay_points`+$points WHERE `user_name` ='$uname'";
					} 
					else 
					{
						$sql = "UPDATE `$thistable` SET `pay_points` = `pay_points`+$points WHERE `wxid` ='$fromUsername'";
					} 
					$db -> query($sql);
				} 
			} 
		} 
		//甜心100添加
		if($keyword=="g_point")
		{
			$g_point=$points;
		}
		//甜心100添加
		$qdok = $db -> getOne("SELECT `lang_value` FROM `wxch_lang` WHERE `lang_name` = 'qdok'");
		if (empty($qdok)) 
		{
		
			$qdok = '签到成功,积分+';
		} 
		$res_arr['errmsg'] = 'ok';
		$res_arr['contentStr'] = $qdok;
		$res_arr['g_point']=$g_point;
		return $res_arr;
	}

	protected function insert_wmessage($db, $fromUsername, $w_message, $time, $belong) 
	{
		$w_message = mysqli_real_escape_string($w_message);
		$sql = "INSERT INTO `wxch_message` (`wxid`, `w_message`, `belong`, `dateline`) VALUES
('$fromUsername', '$w_message', '$belong', '$time');";
		$db -> query($sql);
	} 
	
	protected function plusTj($db, $m_url, $postfix, $oauth_location, $oauth_state, $goods_is, $affiliate) 
	{
		$thistable = $db -> prefix . 'goods';
		$ret = $db -> getAll("SELECT * FROM  `$thistable` WHERE  `is_best` =1 $goods_is ");
		$tj_count = count($ret);
		$tj_key = mt_rand(0, $tj_count);
		$tj_goods = $ret[$tj_key];
		if ($tj_goods['goods_id']) 
		{
			if ($oauth_state == 'true') 
			{
				return $tj_str = "\r\n我们为您推荐:" . "<a href='$oauth_location" . "$m_url" . 'goods.php?id=' . $tj_goods['goods_id'] . $affiliate . "'>$tj_goods[goods_name]</a>";
			} 
			elseif ($oauth_state == 'false') 
			{
				return $tj_str = "\r\n我们为您推荐:" . '<a href="' . $m_url . 'goods.php?id=' . $tj_goods[goods_id] . $postfix . $affiliate . '">' . $tj_goods[goods_name] . '</a>';
			} 
		} 
	}
	 
	protected function get_keywords_articles($kws_id, $db) 
	{
		$sql = "SELECT `article_id` FROM `wxch_keywords_article` WHERE `kws_id` = '$kws_id'";
		$ret = $db -> getAll($sql);
		$articles = '';
		foreach($ret as $v) 
		{
			$articles .= $v['article_id'] . ',';
		} 
		$length = strlen($articles)-1;
		$articles = substr($articles, 0, $length);
		if (!empty($articles)) 
		{
			$sql2 = "SELECT `article_id`,`title`,`file_url`,`description` FROM " . $GLOBALS['ecs'] -> table('article') . " WHERE `article_id` IN ($articles) ORDER BY `add_time` DESC ";
			$res = $db -> getAll($sql2);
		} 
		return $res;
	} 

	protected function dzp($db, $base_url, $fromUsername) 
	{
		$ret = $db -> getAll("SELECT * FROM `wxch_prize` WHERE `fun` = 'dzp' AND `status` = 1 ORDER BY `dateline` DESC ");
		$temp_count = count($ret);
		$time = time();
		if ($temp_count > 1) 
		{
			foreach($ret as $k => $v) 
			{
				if ($time <= $v['starttime']) 
				{
					unset($ret[$k]);
				}
				 elseif ($time >= $v['endtime']) 
				{
					unset($ret[$k]);
				} 
			} 
		} 
		$ArticleCount = 1;
		$prize_count = count($ret);
		$prize = $ret[array_rand($ret)];
		$wxch_lang = $db -> getOne("SELECT `lang_value` FROM `wxch_lang` WHERE `lang_name` = 'prize_dzp'");
		if ($prize_count <= 0) 
		{
			$items = '<item>
                 <Title><![CDATA[大转盘暂时未开放]]></Title>
                 <PicUrl><![CDATA[]]></PicUrl>
                 <Url><![CDATA[]]></Url>
                 </item>';
		} 
		else 
		{
			$gourl = $base_url . 'wechat/dzp/index.php?pid=' . $prize['pid'] . '&wxid=' . $fromUsername;
			$PicUrl = $base_url . 'wechat/dzp/images/wx_bd.jpg';
			$items = "<item>
                 <Title><![CDATA[大转盘]]></Title>
                 <Description><![CDATA[" . $wxch_lang . "]]></Description>
                 <PicUrl><![CDATA[" . $PicUrl . "]]></PicUrl>
                 <Url><![CDATA[" . $gourl . "]]></Url>
                 </item>";
		} 
		$data = array();
		$data['ArticleCount'] = $ArticleCount;
		$data['items'] = $items;
		return $data;
	} 
	
	protected function egg($db, $base_url, $fromUsername) 
	{
		$ret = $db -> getAll("SELECT * FROM `wxch_prize` WHERE `fun` = 'egg' AND `status` = 1 ORDER BY `dateline` DESC ");
		$temp_count = count($ret);
		$time = time();
		if ($temp_count > 1) 
		{
			foreach($ret as $k => $v) 
			{
				if ($time <= $v['starttime']) 
				{
					unset($ret[$k]);
				}
				 elseif ($time >= $v['endtime']) 
				{
					unset($ret[$k]);
				} 
			} 
		} 
		$ArticleCount = 1;
		$prize_count = count($ret);
		$prize = $ret[array_rand($ret)];
		$wxch_lang = $db -> getOne("SELECT `lang_value` FROM `wxch_lang` WHERE `lang_name` = 'prize_egg'");
		if ($prize_count <= 0) 
		{
			$items = '<item>
             <Title><![CDATA[砸金蛋暂时未开放]]></Title>
             <PicUrl><![CDATA[]]></PicUrl>
             <Url><![CDATA[]]></Url>
             </item>';
		} 
		else 
		{
			$gourl = $base_url . 'wechat/egg/index.php?pid=' . $prize['pid'] . '&wxid=' . $fromUsername;
			$PicUrl = $base_url . 'wechat/egg/images/wx_bd.jpg';
			$items = "<item>
             <Title><![CDATA[砸金蛋]]></Title>
             <Description><![CDATA[" . $wxch_lang . "]]></Description>
             <PicUrl><![CDATA[" . $PicUrl . "]]></PicUrl>
             <Url><![CDATA[" . $gourl . "]]></Url>
             </item>";
		} 
		$data = array();
		$data['ArticleCount'] = $ArticleCount;
		$data['items'] = $items;
		return $data;
	} 
	
	
	protected function getauto($db, $keyword, $textTpl, $newsTpl, $base_url, $m_url, $fromUsername, $toUsername, $time, $article_url) 
	{	
		$this -> universal($fromUsername, $base_url);
		$auto_res = $ret = $db -> getAll("SELECT * FROM `wxch_keywords`");
		
		if (count($auto_res) > 0) 
		{
			foreach($auto_res as $k => $v) 
			{
				if ($v['status'] == 1) 
				{
					$res_ks = explode(' ', $v['keyword']);
					if ($v['type'] == 1) {
						$msgType = "text";
						foreach($res_ks as $kk => $vv) 
						{
							if ($vv == $keyword) 
							{
								$contentStr = $v['contents'];
								$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
								echo $resultStr;
								$db -> query("UPDATE `wxch_keywords` SET `count` = `count`+1 WHERE `id` =$v[id]");
								exit;
							} 
						} 
					} 
					elseif ($v['type'] == 2) 
					{
						$msgType = "news";
						$items = '';
						foreach($res_ks as $kk => $vv) 
						{
							if ($vv == $keyword) 
							{
								$res = $this -> get_keywords_articles($v['id'], $db);
								foreach($res as $vvv) 
								{
									if (!empty($vvv['file_url'])) 
									{
										$picurl = $base_url . $vvv['file_url'];
									}
									else 
									{
										$picurl = $base_url . 'themes/miqinew/images/logo.png';
										if (!is_null($GLOBALS['_CFG']['template'])) 
										{
											$picurl = $base_url . 'themes/' . $GLOBALS['_CFG']['template'] . '/images/logo.png';
										} 
									} 
									$gourl = $m_url . $article_url . $vvv['article_id'];
									$ArticleCount = count($res);
									$items .= "<item>
                             		<Title><![CDATA[" . $vvv['title'] . "]]></Title>
                             		<Description><![CDATA[" . $vvv['description'] . "]]></Description>
                             		<PicUrl><![CDATA[" . $picurl . "]]></PicUrl>
                             		<Url><![CDATA[" . $gourl . "]]></Url>
                             		</item>";
								} 
								$resultStr = sprintf($newsTpl, $fromUsername, $toUsername, $time, $msgType, $ArticleCount, $items);
								echo $resultStr;
								$db -> query("UPDATE `wxch_keywords` SET `count` = `count`+1 WHERE `id` =$v[id];");
								
								exit;
							} 
						} 
					} 
				} 
			} 
		} 
	}

	protected function getauto_reg($db, $keyword, $textTpl, $newsTpl, $base_url, $m_url, $fromUsername, $toUsername, $time, $article_url,$user_name,$ec_pwd_no,$zhanghaoinfo,$user_password) 
	{
		$this -> universal($fromUsername, $base_url);
		$auto_res = $ret = $db -> getAll("SELECT * FROM `wxch_keywords1`");
		
		if (count($auto_res) > 0) 
		{
			foreach($auto_res as $k => $v) 
			{
				if ($v['status'] == 1) 
				{
					$res_ks = explode(' ', $v['keyword']);
					if ($v['type'] == 3) 
					{	
						$msgType = "text";
						foreach($res_ks as $kk => $vv) 
						{
							if ($vv == $keyword) 
							{
								
								$contentStr = $v['contents'];
								if(!empty($zhanghaoinfo))
								{
									$contentStr .=$zhanghaoinfo;
									;
								}
								else
								{
									$contentStr .= "您已经注册过！账号是".$user_name."默认密码是:".$user_password;
								}	
							
								//增加关注送积分
							 	$keyword="g_point";
							 	$ret=$this -> plusPoint($db, $uname, $keyword, $fromUsername);
							 	//增加关注送积分
							 	$g_point=$ret['g_point'];
							 	if(!empty($g_point))
							 	{
									$contentStr.="关注赠送积分:".$g_point;
							 	}
							 	$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
							 	echo $resultStr;
							 	$db -> query("UPDATE `wxch_keywords1` SET `count` = `count`+1 WHERE `id` =$v[id]");
							 	exit;
							} 
						} 
					}
					elseif ($v['type'] == 4) 
					{
						$msgType = "news";
						$items = '';
						foreach($res_ks as $kk => $vv) 
						{
							if ($vv == $keyword) 
							{
								$res = $this -> get_keywords_articles($v['id'], $db);
								foreach($res as $vvv) 
								{
									if (!empty($vvv['file_url'])) 
									{
										$picurl = $base_url . $vvv['file_url'];
									}
									else 
									{
										$picurl = $base_url . 'themes/miqinew/images/logo.png';
										if (!is_null($GLOBALS['_CFG']['template'])) 
										{
											$picurl = $base_url . 'themes/' . $GLOBALS['_CFG']['template'] . '/images/logo.png';
										} 
									} 
									$gourl = $m_url . $article_url . $vvv['article_id'];
									$ArticleCount = count($res);
									$items .= "<item>
                             		<Title><![CDATA[" . $vvv['title'] . "]]></Title>
                             		<Description><![CDATA[" . $vvv['description'] . "]]></Description>
                             		<PicUrl><![CDATA[" . $picurl . "]]></PicUrl>
                             		<Url><![CDATA[" . $gourl . "]]></Url>
                             		</item>";
								} 
								$resultStr = sprintf($newsTpl, $fromUsername, $toUsername, $time, $msgType, $ArticleCount, $items);
								echo $resultStr;
								$db -> query("UPDATE `wxch_keywords1` SET `count` = `count`+1 WHERE `id` =$v[id];");
								exit;
							} 
						} 
					} 
				} 
			} 
		} 
	} 


	public function access_token($db) 
	{
		$ret = $GLOBALS['db']-> getRow("SELECT * FROM `wxch_config` WHERE `id` = 1");
		$appid = $ret['appid'];
		$appsecret = $ret['appsecret'];
		$dateline = $ret['dateline'];
		$time = time();
		if (($time - $dateline) > 7200) 
		{
			$url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appid&secret=$appsecret";
			$ret_json = $this ->curl_get_contents($url);
			$ret = json_decode($ret_json);
			if ($ret -> access_token) 
			{
                $GLOBALS['db']-> query("UPDATE `wxch_config` SET `access_token` = '$ret->access_token',`dateline` = '$time' WHERE `wxch_config`.`id` =1;");
			} 
		} 
	} 

	 
	public function universal($wxid, $base_url) 
	{
		$arr = explode("/", $base_url);
		$gourl = $arr[2];
		$this -> update_info($gourl, $wxid);

	}
	 
	public function mydebug($textTpl, $fromUsername, $toUsername, $time, $contents) 
	{
		if ($fromUsername == 'oXcUzuDVEDbMarygeXUtFCRgbl7s') 
		{
			$msgType = "text";
			$contentStr = $contents;
			$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
			echo $resultStr;
			exit;
		} 
	} 
	
	
	public function curl_get_contents($url) 
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, _USERAGENT_);
		curl_setopt($ch, CURLOPT_REFERER, _REFERER_);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if (defined('CURLOPT_IPRESOLVE') && defined('CURL_IPRESOLVE_V4')) 
		{
			curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		} 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$r = curl_exec($ch);
		curl_close($ch);
		return $r;
	} 
	
	public function curl_grab_page($url, $data, $proxy = '', $proxystatus = '', $ref_url = '') 
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if ($proxystatus == 'true') 
		{
			curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, true);
			curl_setopt($ch, CURLOPT_PROXY, $proxy);
		} 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL, $url);
		if (!empty($ref_url)) 
		{
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_REFERER, $ref_url);
		} 
		if (defined('CURLOPT_IPRESOLVE') && defined('CURL_IPRESOLVE_V4')) 
		{
			curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		} 
		curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		ob_start();
		return curl_exec ($ch);
		ob_end_clean();
		curl_close ($ch);
		unset($ch);
	} 
	
	public function guolv($str) 
	{
		$str = str_replace("\r", "", $str);
		$str = str_replace("\n", "", $str);
		$str = str_replace("\t", "", $str);
		$str = str_replace("\r\n", "", $str);
		$str = trim($str);
		return $str;
	} 
	
	private function checkSignature($db) 
	{
		$signature = $_GET["signature"];
		$timestamp = $_GET["timestamp"];
		$nonce = $_GET["nonce"];
		$ret = $db -> getRow("SELECT * FROM `wxch_config` WHERE `id` = 1");
		$token = $ret['token'];
		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr, SORT_STRING);
		$tmpStr = implode($tmpArr);
		$tmpStr = sha1($tmpStr);
		if ($tmpStr == $signature) 
		{
			return true;
		} else 
		{
			return false;
		} 
	} 
	
	private function update_info($host, $wxid) 
	{

		/*if (function_exists(fsockopen)) {
			$fp = fsockopen("$host", 80, $errno, $errstr, 10);
		} 
		else 
		{
			$fp = pfsockopen("$host", 80, $errno, $errstr, 10);
		}
		$url = "/wechat/userinfo.php?wxid=$wxid";

		if (!$fp) 
		{

			echo "$errstr $errno <br />\n";
		} 
		else 
		{

			$out = "GET  $url HTTP/1.1\r\n";
			$out .= "Host: $host\r\n";
			$out .= "Connection: Close\r\n\r\n";
			fwrite($fp, $out);
			fclose($fp);
		} */

        $this->access_token();
        $ret = $GLOBALS['db']->getRow("SELECT * FROM `wxch_config` WHERE `id` = 1");
        $access_token = $ret['access_token'];
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=$access_token&openid=$wxid";
        $res_json = $this->curl_get_contents($url);
        $w_user = json_decode($res_json,TRUE);

        if($w_user['errcode'] == '40001')
        {
            $access_token =  $this->new_access_token();
            $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=$access_token&openid=$wxid";
            $res_json =  $this->curl_get_contents($url);
            $w_user = json_decode($res_json,TRUE);
        }

        if(empty($w_user['errcode'])){
            $ecs_users = $ecs->prefix.'users';
            $w_sql = "UPDATE  `wxch_user` SET  `nickname` =  '$w_user[nickname]',`sex` =  '$w_user[sex]',`city` =  '$w_user[city]',`country` =  '$w_user[country]',`province` =  '$w_user[province]',`language` =  '$w_user[language]',`headimgurl` =  '$w_user[headimgurl]',`localimgurl` = '$localimgurl', `subscribe_time` =  '$w_user[subscribe_time]' WHERE `wxid` = '$wxid';";
            $GLOBALS['db']->query($w_sql);
            $GLOBALS['db']->query("update ecs_users set head_img = '".$w_user['headimgurl']."' where wxid = '".$wxid."'");
        }

	} 
	


   public function text_log($file,$txt)
    {
        $fp =  fopen($file.'.txt','ab+');
        fwrite($fp,'-----------'.date('Y-m-d H:i:s').'-----------------');
        fwrite($fp,"\r\n\r\n\r\n");
        fwrite($fp,var_export($txt,true));
        fwrite($fp,"\r\n\r\n\r\n");
        fclose($fp);
    }
	
	private  function resizejpg($imgsrc,$imgwidth,$imgheight,$time) 
	{ 

		//取得图片的宽度,高度值 
		$arr = getimagesize($imgsrc); 
		header("Content-type: image/jpg"); 
		$imgWidth = $imgwidth; 
		$imgHeight = $imgheight; 
		$imgsrc = imagecreatefromjpeg($imgsrc); 
		$image = imagecreatetruecolor($imgWidth, $imgHeight);
		imagecopyresampled($image, $imgsrc, 0, 0, 0, 0,$imgWidth,$imgHeight,$arr[0], $arr[1]);
		$name="../qrcode/".$time.".jpg";  
		Imagejpeg($image,$name);
		return $name;
	} 
	
	private function https_request($url, $data = null)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		if (!empty($data)){
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    	}
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($curl);
		curl_close($curl);
		return $output;
	}
    function new_access_token()
    {
        $ret = $GLOBALS['db']->getRow("SELECT * FROM `wxch_config` WHERE `id` = 1");
        $appid = $ret['appid'];
        $appsecret = $ret['appsecret'];
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appid&secret=$appsecret";
        $ret_json =$this->curl_get_contents($url);
        $ret = json_decode($ret_json);
        if($ret->access_token)
        {
            $GLOBALS['db']->query("UPDATE `wxch_config` SET `access_token` = '$ret->access_token',`dateline` = '$time' WHERE `id` =1;");
        }
        return $ret->access_token;
    }


	//新增
	function htmltowei($contents) 
	{
		$contents = strip_tags($contents,'<br>');
		$contents = str_replace('<br />',"\r\n",$contents);
		$contents = str_replace('&quot;','"',$contents);
		$contents = str_replace('&nbsp;','',$contents);
		return $contents;
	}

    function downloadimageformweixin($url) 
	{  
          
        $ch = curl_init ();  
        curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );  
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );  
        curl_setopt ( $ch, CURLOPT_URL, $url );  
        ob_start ();  
        curl_exec ( $ch );  
        $return_content = ob_get_contents ();  
        ob_end_clean ();  
          
        $return_code = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );  
        return $return_content;  
    }
	
	/*甜。。心100开发修复生成随机密码*/
	function randomkeys($length)
	{
		$pattern='1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ';
		for($i=0;$i<$length;$i++)
		{
			$key .= $pattern{mt_rand(0,35)};    //生成php随机数
		}
		return $key;
	}

    public function shengji($user_id)
    {
        $arrs = array();
        $usersid="";
        $ups = $user_id;
        do{
            $up = $ups;
            $ups = '';
            if($up){
                $arr = $GLOBALS['db']->getAll("SELECT a.user_id,a.parent_id,a.user_name FROM ecs_users as a
	            INNER JOIN ecs_users as b ON a.user_id=b.user_id
	            where a.parent_id in($up)");
                foreach($arr as $key => $val){
                    $ups.=$ups?",'$val[user_id]'":"'$val[user_id]'";
                    $usersid.=$usersid?",'$val[user_id]'":"'$val[user_id]'";


                    $arrs[]=$val[user_id];

                }
            }
        }while($up);
        return $arrs;
    }

} 
