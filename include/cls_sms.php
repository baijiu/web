<?php

/**
 * GAOXIN 短信模块 之 模型（类库）
 * ============================================================================
 * 版权所有 2014  ，并保留所有权利。
 * 网站地址: http://www.ecmoban.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: Jack $
 * $Id: cls_sms.php 17155 2014-02-06 06:29:05Z $
 */
if (!defined('IN_ECTOUCH')) {
    die('Hacking attempt');
}

/* 短信模块主类 */

class sms {

    var $sms_name = NULL; //用户名
    var $sms_password = NULL; //密码
    
    function __construct() {
        $this->sms();
    }

    function sms() {
        /* 由于要包含init.php，所以这两个对象一定是存在的，因此直接赋值 */
        $this->sms_name = $GLOBALS['_CFG']['sms_ecmoban_user'];
        $this->sms_password = $GLOBALS['_CFG']['sms_ecmoban_password'];
    }

    // 发送短消息
    function send($phones, $msg, &$sms_error = array()) {

		
        /* 检查发送信息的合法性 */
        $contents = $this->get_contents($phones, $msg);

		
        if (!$contents) {
            return false;
        }

        

        if (count($contents) > 1) {
            foreach ($contents as $key => $val) {
               

				$gets =$this->send_newsms($val['phones'],$val['content']);
                sleep(1);
            }
        } else {
            
		   $gets =$this->send_newsms($contents[0]['phones'],$contents[0]['content'],"","");
		  

        }
		
		$result_msg= array(
			'100'=>'发送成功',
			'101'=>'验证失败（帐号密码错误）',
			'102'=>'短信不足',
			'103'=>'操作失败',
			'104'=>'非法字符',
			'105'=>'内容过多',
			'106'=>'号码过多',
			'107'=>'频率过快',
			'108'=>'号码内容空',
			'109'=>'帐号冻结',
			'110'=>'禁止频繁单条发送',
			'111'=>'帐号暂停发送',
			'112'=>'号码错误',
			'113'=>'定时时间格式不对',
			'114'=>'帐号临时锁定，10分钟后自动解锁',
			'115'=>'连接失败',
			'116'=>'禁止接口发送',
			'117'=>'绑定IP错误',
			'120'=>'系统升级'
			
		);
        
        if ($gets == '100') {
            return true;
        }
		else 
		{
            $sms_error = $result_msg[$gets];
            return false;
        }
    }

   
    

    //检查手机号和发送的内容并生成生成短信队列
    function get_contents($phones, $msg) {
        if (empty($phones) || empty($msg)) {
            return false;
        }
        //$msg.='【'. $GLOBALS['_CFG']['shop_name'].'】'; //by wanganlin delete
        $phone_key = 0;
        $i = 0;
        $phones = explode(',', $phones);
        foreach ($phones as $key => $value) {
            if ($i < 200) {
                $i++;
            } else {
                $i = 0;
                $phone_key++;
            }
            if ($this->is_moblie($value)) {
                $phone[$phone_key][] = $value;
            } else {
                $i--;
            }
        }

		


        if (!empty($phone)) {
            foreach ($phone as $phone_key => $val) {
                if (EC_CHARSET != 'utf-8') {
                    $phone_array[$phone_key]['phones'] = implode(',', $val);
                    $phone_array[$phone_key]['content'] = $this->auto_charset($msg);
                } else {
                    $phone_array[$phone_key]['phones'] = implode(',', $val);
                    $phone_array[$phone_key]['content'] = $msg;
                }
            }
            return $phone_array;
        } else {
            return false;
        }
    }

    // 自动转换字符集 支持数组转换
    function auto_charset($fContents, $from = 'gbk', $to = 'utf-8') {
        $from = strtoupper($from) == 'UTF8' ? 'utf-8' : $from;
        $to = strtoupper($to) == 'UTF8' ? 'utf-8' : $to;
        if (strtoupper($from) === strtoupper($to) || empty($fContents) || (is_scalar($fContents) && !is_string($fContents))) {
            //如果编码相同或者非字符串标量则不转换
            return $fContents;
        }
        if (is_string($fContents)) {
            if (function_exists('mb_convert_encoding')) {
                return mb_convert_encoding($fContents, $to, $from);
            } elseif (function_exists('iconv')) {
                return iconv($from, $to, $fContents);
            } else {
                return $fContents;
            }
        } elseif (is_array($fContents)) {
            foreach ($fContents as $key => $val) {
                $_key = auto_charset($key, $from, $to);
                $fContents[$_key] = auto_charset($val, $from, $to);
                if ($key != $_key)
                    unset($fContents[$key]);
            }
            return $fContents;
        }
        else {
            return $fContents;
        }
    }

    // 检测手机号码是否正确
    function is_moblie($moblie) {
        return preg_match("/^1[3456789]\d{9}$/", $moblie);
    }



	function send_newsms($mobile,$content,$time='',$mid='')
{
	$http = 'http://http.yunsms.cn/tx/';
	
	$uid=$this->sms_name;
	$pwd=$this->sms_password;
	$data = array
		(
		'uid'=>$uid,					//数字用户名
		'pwd'=>strtolower(md5($pwd)),	//MD5位32密码
		'mobile'=>$mobile,				//号码
		//'content'=>$content,			//内容 如果对方是utf-8编码，则需转码iconv('gbk','utf-8',$content); 如果是gbk则无需转码
		'content'=>iconv('utf-8','gbk',$content),			//内容 如果对方是utf-8编码，则需转码iconv('gbk','utf-8',$content); 如果是gbk则无需转码
		'time'=>$time,		//定时发送
		'mid'=>$mid						//子扩展号
		);

		 
	$re=$this->postSMS($http,$data);			//POST方式提交
	
	
	
	return $re;
	
}

function postSMS($url,$data='')
{
	$post='';
	$row = parse_url($url);
	$host = $row['host'];
	//$port = $row['port'] ? $row['port']:80;
    $port =80;
	$file = $row['path'];
	while (list($k,$v) = each($data)) 
	{
		$post .= rawurlencode($k)."=".rawurlencode($v)."&";	//转URL标准码
	}
	$post = substr( $post , 0 , -1 );
	$len = strlen($post);
	
	//$fp = @fsockopen( $host ,$port, $errno, $errstr, 10);
	 
	$fp = stream_socket_client("tcp://".$host.":".$port, $errno, $errstr, 10);
	
	
	
	
	if (!$fp) {
		return "$errstr ($errno)\n";
	} else {
		$receive = '';
		$out = "POST $file HTTP/1.0\r\n";
		$out .= "Host: $host\r\n";
		$out .= "Content-type: application/x-www-form-urlencoded\r\n";
		$out .= "Connection: Close\r\n";
		$out .= "Content-Length: $len\r\n\r\n";
		$out .= $post;		
		fwrite($fp, $out);
		while (!feof($fp)) {
			$receive .= fgets($fp, 128);
		}
		fclose($fp);
		$receive = explode("\r\n\r\n",$receive);
		unset($receive[0]);
		return implode("",$receive);
	}
}


}










?>