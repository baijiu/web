<?php

/**
 * 高鑫 管理中心文章处理程序文件
 * ============================================================================
 * * 版权所有 2005-2012 ，并保留所有权利。
 * 网站地址: http://www.gxwlr.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: article.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECTOUCH', true);
require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'include/fckeditor/fckeditor.php');
require_once(ROOT_PATH . 'include/cls_image_tianxin.php');
$image = new cls_image($_CFG['bgcolor']);
/*初始化数据交换对象 */
$exc   = new exchange($ecs->table("productinfo"), $db, 'id', 'goods_name');
/* 允许上传的文件类型 */
$allow_file_types = '|GIF|JPG|PNG|BMP|SWF|DOC|XLS|PPT|MID|WAV|ZIP|RAR|PDF|CHM|RM|TXT|';
/*------------------------------------------------------ */
//-- 文章列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{

    /* 取得过滤条件 */
    $filter = array();

    $smarty->assign('ur_here',      "项目列表");
    $smarty->assign('action_link',  array('text' =>'添加项目', 'href' => 'product.php?act=add'));
    $smarty->assign('full_page',    1);
    $smarty->assign('filter',       $filter);

    $goods_list = get_goodslist();

    $smarty->assign('goods_list',    $goods_list['arr']);
    $smarty->assign('filter',          $goods_list['filter']);
    $smarty->assign('record_count',    $goods_list['record_count']);
    $smarty->assign('page_count',      $goods_list['page_count']);

    assign_query_info();
    $smarty->display('product_list.htm');
}

/*------------------------------------------------------ */
//-- 翻页，排序
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{

    $goods_list = get_goodslist();
    $smarty->assign('goods_list',    $goods_list['arr']);
    $smarty->assign('filter',          $goods_list['filter']);
    $smarty->assign('record_count',    $goods_list['record_count']);
    $smarty->assign('page_count',      $goods_list['page_count']);
    make_json_result($smarty->fetch('product_list.htm'), '',
        array('filter' => $goods_list['filter'], 'page_count' => $goods_list['page_count']));
}

/*------------------------------------------------------ */
//-- 添加文章
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'add')
{

    /* 创建 html editor */
    create_html_editor('FCKeditor1');


    //取得股票
    $sql = "SELECT cat_name, cat_id FROM ecs_product_type ORDER BY cat_id ASC ";
    $rs = $db->query($sql);

    $ranks = array();
    while($row = $db->FetchRow($rs))
    {
        $ranks[$row['cat_id']] = $row['cat_name'];
    }

    $smarty->assign('piao', $ranks);

    $smarty->assign('status_id', 0);

    /*初始化*/
    $stockgoods = array();
	$stockgoods['goods_price'] = "0.00";
    $stockgoods['total_amount'] = "0.00";
    $stockgoods['ketoucishu'] = "1";
    $smarty->assign('stockgoods',     $stockgoods);
    $smarty->assign('ur_here',     "添加产品");
    $smarty->assign('action_link', array('text' =>"产品列表", 'href' => 'product.php?act=list'));
    $smarty->assign('form_action', 'insert');

    assign_query_info();
    $smarty->display('product_info.htm');
}

/*------------------------------------------------------ */
//-- 添加文章
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'insert')
{
    if(empty( $_POST['goods_name'])){
        sys_msg("请输入项目名称", 1);
    }
    $is_only = $exc->is_only('goods_name', $_POST['goods_name'], $_POST['id']);
    if (!$is_only)
    {
        sys_msg("项目已存在", 1);
    }
    $goods_name = $_POST['goods_name'];
    $cycle = intval($_POST['cycle']);//周期
    if($cycle<=0){
        sys_msg("项目周期最小为1个自然日", 1);
    }
    $goods_price =  floatval($_POST['goods_price']);//起投金额
    if($goods_price<=0){
        sys_msg("起投金额不能为0", 1);
    }
    $total_amount =  floatval($_POST['total_amount']);//项目规模
    if($total_amount<=0){
        sys_msg("项目规模不能为0", 1);
    }
    if($goods_price>$total_amount){
        sys_msg("起投金额不能大于项目规模", 1);
    }
    $daily_rate = floatval($_POST['daily_rate']);//日化率
    if($daily_rate<=0){
        sys_msg("日化率不能小于0", 1);
    }
    $ketoucishu = intval($_POST['ketoucishu']);//可投次数
    if($ketoucishu<1){
        sys_msg("可投次数不能为0", 1);
    }
    $type = $_POST['type'];//还款方式
    if($type==''){
        sys_msg("请选择还款方式", 1);
    }
    $tuijianzhishu = $_POST['tuijianzhishu'];//推荐指数
    if($tuijianzhishu==''){
        sys_msg("请选择推荐指数", 1);
    }
    /*$jiesuanshijian = $_POST['jiesuanshijian'];//结算时间
    if($jiesuanshijian==''){
        sys_msg("结算时间不能为空", 1);
    }*/

    /*$zijinyongtu = $_POST['zijinyongtu'];//资金用途
    if($zijinyongtu==''){
        sys_msg("资金用途不能为空", 1);
    }*/


    $shangyingtime = $_POST['shangyingtime'];//上映时间
    if($shangyingtime==''){
        sys_msg("上映时间不能为空", 1);
    }

    $yitou_amount =  floatval($_POST['yitou_amount']);//已投
    if($yitou_amount<0){
        sys_msg("已投资金不能小于0", 1);
    }

    $content = $_POST['FCKeditor1'];//项目概述
   


    $cat_id = $_POST['cat_id'];//类型
    if($cat_id<=0){
        sys_msg("请选择类型", 1);
    }

    /* 上传图片 */
    $file_url = '';
    if ((isset($_FILES['file']['error']) && $_FILES['file']['error'] == 0) || (!isset($_FILES['file']['error']) && isset($_FILES['file']['tmp_name']) && $_FILES['file']['tmp_name'] != 'none'))
    {
        // 检查文件格式
        if (!check_file_type($_FILES['file']['tmp_name'], $_FILES['file']['name'], $allow_file_types))
        {
            sys_msg($_LANG['invalid_file']);
        }

        // 复制文件
        $res = upload_article_file($_FILES['file']);
        if ($res != false)
        {
            $file_url = $res;
        }
    }

    if($file_url =='')
	{
		sys_msg("请上传缩略图", 1);
	}

	 //生成所略图
    if (!empty($file_url))
    {
        // 如果设置缩略图大小不为0，生成缩略图
        if ($_CFG['thumb_width'] != 0 || $_CFG['thumb_height'] != 0)
        {
            $goods_thumb = $image->make_thumb(WEB_PATH . $file_url, $GLOBALS['_CFG']['thumb_width'],  $GLOBALS['_CFG']['thumb_height']);
            if ($goods_thumb === false)
            {
                sys_msg($image->error_msg(), 1, array(), false);
            }
        }
        else
        {
            $goods_thumb = $file_url;
        }
    }

    /*插入数据*/
    $add_time = gmtime();
    $sql = "INSERT INTO ".$ecs->table('productinfo')."(cat_id,goods_name,cycle,daily_rate, goods_price,total_amount,tuijianzhishu,shangyingtime,ketoucishu,yitou_amount,type, add_time,content, file_url,goods_thumb) ".
            "VALUES ('$cat_id', '$goods_name', '$cycle','$daily_rate', '$goods_price', '$total_amount','$tuijianzhishu','$shangyingtime', '$ketoucishu', '$yitou_amount', '$type', '$add_time', '$content', '$file_url', '$goods_thumb')";
    $db->query($sql);

    $link[0]['text'] = "继续添加";
    $link[0]['href'] = 'product.php?act=add';

    $link[1]['text'] = "返回项目列表";
    $link[1]['href'] = 'product.php?act=list';

    clear_cache_files(); // 清除相关的缓存文件

    sys_msg("添加成功！",0, $link);
}

/*------------------------------------------------------ */
//-- 编辑
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'edit')
{

    $sql = "SELECT * FROM " .$ecs->table('productinfo'). " WHERE id='$_REQUEST[id]'";
    $stockgoods = $db->GetRow($sql);

    /* 创建 html editor */
    create_html_editor('FCKeditor1',$stockgoods['content']);
    //取得股票
    $sql = "SELECT cat_name, cat_id FROM ecs_product_type ORDER BY cat_id ASC ";
    $rs = $db->query($sql);

    $ranks = array();
    while($row = $db->FetchRow($rs))
    {
        $ranks[$row['cat_id']] = $row['cat_name'];
    }

    $smarty->assign('piao', $ranks);

    $smarty->assign('status_id',  $stockgoods['cat_id']);


    $smarty->assign('stockgoods',     $stockgoods);
    $smarty->assign('ur_here',     "编辑项目");
    $smarty->assign('action_link', array('text' => "项目列表", 'href' => 'product.php?act=list&' . list_link_postfix()));
    $smarty->assign('form_action', 'update');

    assign_query_info();
    $smarty->display('product_info.htm');
}

if ($_REQUEST['act'] =='update')
{

    if(empty( $_POST['goods_name'])){
        sys_msg("请输入项目名称", 1);
    }

    /*检查是否相同*/
    $is_only = $exc->is_only('goods_name', $_POST['goods_name'], $_POST['id']);

    if (!$is_only)
    {
        sys_msg("项目名称已存在", 1);
    }



    $goods_name = $_POST['goods_name'];
    $cycle = intval($_POST['cycle']);//周期
    if($cycle<=0){
        sys_msg("项目周期最小为1个自然日", 1);
    }
    $goods_price =  floatval($_POST['goods_price']);//起投金额
    if($goods_price<=0){
        sys_msg("起投金额不能为0", 1);
    }
    $total_amount =  floatval($_POST['total_amount']);//项目规模
    if($total_amount<=0){
        sys_msg("项目规模不能为0", 1);
    }
    if($goods_price>$total_amount){
        sys_msg("起投金额不能大于项目规模", 1);
    }
    $daily_rate = floatval($_POST['daily_rate']);//日化率
    if($daily_rate<=0){
        sys_msg("日化率不能小于0", 1);
    }
    $ketoucishu = intval($_POST['ketoucishu']);//可投次数
    if($ketoucishu<1){
        sys_msg("可投次数不能为0", 1);
    }
    $type = $_POST['type'];//还款方式
    if($type==''){
        sys_msg("请选择还款方式", 1);
    }
    $tuijianzhishu = $_POST['tuijianzhishu'];//推荐指数
    if($tuijianzhishu==''){
        sys_msg("请选择推荐指数", 1);
    }
    /*$jiesuanshijian = $_POST['jiesuanshijian'];//结算时间
    if($jiesuanshijian==''){
        sys_msg("结算时间不能为空", 1);
    }

    $zijinyongtu = $_POST['zijinyongtu'];//资金用途
    if($zijinyongtu==''){
        sys_msg("资金用途不能为空", 1);
    }*/

    $shangyingtime = $_POST['shangyingtime'];//上映时间
    if($shangyingtime==''){
        sys_msg("上映时间不能为空", 1);
    }

    $yitou_amount =  floatval($_POST['yitou_amount']);//已投
    if($yitou_amount<0){
        sys_msg("已投资金不能小于0", 1);
    }

    if($yitou_amount>=$total_amount){
        $state = 1;
    }else{
        $state = 0;
    }


     $content = $_POST['FCKeditor1'];//项目概述

    $cat_id = $_POST['cat_id'];//类型
    if($cat_id<=0){
        sys_msg("请选择类型", 1);
    }





    $file_url = '';
    $goods_thumb = '';
    if (empty($_FILES['file']['error']) || (!isset($_FILES['file']['error']) && isset($_FILES['file']['tmp_name']) && $_FILES['file']['tmp_name'] != 'none'))
    {
        // 检查文件格式
        if (!check_file_type($_FILES['file']['tmp_name'], $_FILES['file']['name'], $allow_file_types))
        {
            sys_msg($_LANG['invalid_file']);
        }
        // 上传文件
        $res = upload_article_file($_FILES['file']);

        if ($res != false)
        {
            $file_url = $res;
        }
    }
	
	
	 // 生成所略图
    if (!empty($file_url))
    {
        // 如果设置缩略图大小不为0，生成缩略图
        if ($_CFG['thumb_width'] != 0 || $_CFG['thumb_height'] != 0)
        {
            $goods_thumb = $image->make_thumb(WEB_PATH . $file_url, $GLOBALS['_CFG']['thumb_width'],  $GLOBALS['_CFG']['thumb_height']);
            if ($goods_thumb === false)
            {
                sys_msg($image->error_msg(), 1, array(), false);
            }
        }
        else
        {
            $goods_thumb = $file_url;
        }
    }

    $sql = "SELECT file_url, goods_thumb FROM " . $ecs->table('productinfo') . " WHERE id = '$_POST[id]'";
    $row = $db->getRow($sql);

	if($file_url=='')
	{
		$file_url=$row['file_url'];
	}
	if($goods_thumb=='')
	{
		$goods_thumb=$row['goods_thumb'];
	}


    if ($exc->edit("cat_id='$cat_id' , goods_name='$goods_name' , cycle='$cycle' , daily_rate='$daily_rate',goods_price='$goods_price', total_amount='$total_amount',tuijianzhishu='$tuijianzhishu',  shangyingtime='$shangyingtime',  ketoucishu='$ketoucishu',yitou_amount='$yitou_amount',type='$type',content='$content',
file_url ='$file_url', goods_thumb = '$goods_thumb',state = '$state'", $_POST['id']))
    {
        $link[0]['text'] = "返回项目列表";
        $link[0]['href'] = 'product.php?act=list';

        clear_cache_files();
        sys_msg("修改成功！",0, $link);
    }
    else
    {
        die($db->error());
    }
}


elseif ($_REQUEST['act'] == 'toggle_on_sale')
{
    //check_authz_json('goods_manage');

    $goods_id       = intval($_POST['id']);
    $on_sale        = intval($_POST['val']);

    if ($exc->edit("is_on_sale = '$on_sale'", $goods_id))
    {
        clear_cache_files();
        make_json_result($on_sale);
    }
}

elseif ($_REQUEST['act'] == 'remove')
{


    $id = intval($_GET['id']);

    $count = $db->getone("select count(*) from ecs_product_order where goods_id = '".$id."' and is_pay = 1 and  state = 0");

    if($count>0){
        sys_msg("该项目已有用户投资且未完成，无法删除！");
    }else{
        if ($exc->drop($id))
        {
            sys_msg("操作成功！");
        }else{
            sys_msg("操作失败！");
        }

    }


}


function get_goodslist()
{
    $result = get_filter();
    if ($result === false)
    {
        $filter = array();
        $filter['keyword']    = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
        if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
        {
            $filter['keyword'] = json_str_iconv($filter['keyword']);
        }

        $filter['sort_by']    = empty($_REQUEST['sort_by']) ? 'id' : trim($_REQUEST['sort_by']);
        $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $where = '';
        if (!empty($filter['keyword']))
        {
            $where = " AND goods_name LIKE '%" . mysqli_like_quote($filter['keyword']) . "%'";
        }


        $sql = 'SELECT COUNT(*) FROM ' .$GLOBALS['ecs']->table('productinfo').'WHERE 1 ' .$where;
        $filter['record_count'] = $GLOBALS['db']->getOne($sql);

        $filter = page_and_size($filter);

        $sql = 'SELECT * FROM ' .$GLOBALS['ecs']->table('productinfo').
               'WHERE 1 ' .$where. ' ORDER by '.$filter['sort_by'].' '.$filter['sort_order'];

        $filter['keyword'] = stripslashes($filter['keyword']);
        set_filter($filter, $sql);
    }
    else
    {
        $sql    = $result['sql'];
        $filter = $result['filter'];
    }
    $arr = array();
    $res = $GLOBALS['db']->selectLimit($sql, $filter['page_size'], $filter['start']);

    while ($rows = $GLOBALS['db']->fetchRow($res))
    {
        $rows['date'] = local_date($GLOBALS['_CFG']['time_format'], $rows['add_time']);
        $arr[] = $rows;
    }
    return array('arr' => $arr, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
}

/* 上传文件 */
function upload_article_file($upload)
{
    if (!make_dir("../" . DATA_DIR . "/product"))
    {
        /* 创建目录失败 */
        return false;
    }
    $filename = cls_image::random_filename() . substr($upload['name'], strpos($upload['name'], '.'));
    $path     = WEB_PATH. DATA_DIR . "/product/" . $filename;

    if (move_upload_file($upload['tmp_name'], $path))
    {
        return DATA_DIR . "/product/" . $filename;
    }
    else
    {
        return false;
    }
}

?>