<?php

/**
 * 高鑫 管理中心菜单数组
 * ============================================================================
 * * 版权所有 2005-2012 ，并保留所有权利。
 * 网站地址: http://www.gxwlr.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: inc_menu.php 17217 2011-01-19 06:29:08Z liubo $
*/

if (!defined('IN_ECTOUCH'))
{
    die('Hacking attempt');
}
/*甜   心 100 添加  */
$modules['02_cat_and_goods']['01_goods_list']       = 'goods.php?act=list';         // 商品列表
$modules['02_cat_and_goods']['02_goods_add']        = 'goods.php?act=add';          // 添加商品
$modules['02_cat_and_goods']['03_category_list']    = 'category.php?act=list';
$modules['02_cat_and_goods']['04_comment_manage']   = 'comment_manage.php?act=list';
$modules['02_cat_and_goods']['06_goods_brand_list'] = 'brand.php?act=list';
$modules['02_cat_and_goods']['08_goods_type']       = 'goods_type.php?act=manage';
$modules['02_cat_and_goods']['11_goods_trash']      = 'goods.php?act=trash';        // 商品回收站
$modules['02_cat_and_goods']['15_exchange_goods']       = 'exchange_goods.php?act=list';//
//$modules['02_cat_and_goods']['50_virtual_card_list']   = 'goods.php?act=list&extension_code=virtual_card';
//$modules['02_cat_and_goods']['51_virtual_card_add']    = 'goods.php?act=add&extension_code=virtual_card';

$modules['04_order']['02_order_list']               = 'order.php?act=list';
$modules['04_order']['03_order_query']              = 'order.php?act=order_query';
$modules['04_order']['09_delivery_order']           = 'order.php?act=delivery_list';



$modules['07_content']['03_article_list']           = 'article.php?act=list';
$modules['07_content']['02_articlecat_list']        = 'articlecat.php?act=list';

$modules['08_members']['03_users_list']             = 'users.php?act=list';//
$modules['08_members']['04_users_add']              = 'users.php?act=add';//
$modules['08_members']['08_unreply_msg']            = 'user_msg.php?act=list_all';
$modules['08_members']['11_user_recharge']              = 'user_recharge.php?act=list';//
$modules['08_members']['12_user_withdraw']            = 'user_withdraw.php?act=list';

$modules['10_priv_admin']['admin_logs']             = 'admin_logs.php?act=list';
$modules['10_priv_admin']['admin_list']             = 'privilege.php?act=list';
$modules['10_priv_admin']['admin_role']             = 'role.php?act=list';

$modules['11_system']['01_shop_config']             = 'shop_config.php?act=list_edit';
$modules['11_system']['02_payment_list']            = 'payment.php?act=list';
$modules['11_system']['03_shipping_list']           = 'shipping.php?act=list';
$modules['11_system']['04_mail_settings']           = 'shop_config.php?act=mail_settings';
$modules['11_system']['05_area_list']               = 'area_manage.php?act=list';
$modules['11_system']['navigator']                  = 'navigator.php?act=list';
$modules['11_system']['03_template_setup']        = 'template.php?act=setup';
$modules['11_system']['ad_position']                = 'ad_position.php?act=list';
$modules['11_system']['ad_list']                    = 'ads.php?act=list';
$modules['11_system']['09_set']                    = 'set.php?act=list';
//微信通管理
$modules['17_wechat']['wx_1api']           = 'wxch-ent.php?act=wxconfig';
$modules['17_wechat']['wx_3menu']        = 'wxch-ent.php?act=menu';
$modules['17_wechat']['wx_2config']       = 'wxch-ent.php?act=config';
$modules['17_wechat']['wx_4autoreg']       = 'wxch-ent.php?act=autoreg';
$modules['17_wechat']['wx_regmsg']        = 'wxch-ent.php?act=regmsg';
$modules['17_wechat']['wx_keywords']        = 'wxch-ent.php?act=keywords';
$modules['17_wechat']['wx_prize']        = 'wxch-ent.php?act=prize';
$modules['17_wechat']['wx_zjd']        = 'wxch-ent.php?act=zjd';
$modules['17_wechat']['wx_dzp']        = 'wxch-ent.php?act=dzp';
$modules['17_wechat']['wx_order']        = 'wxch-ent.php?act=order';
$modules['17_wechat']['wx_pay']        = 'wxch-ent.php?act=pay';
$modules['17_wechat']['wx_reorder']        = 'wxch-ent.php?act=reorder';
$modules['17_wechat']['wx_fans']        = 'wxch_users.php?act=list';
$modules['17_wechat']['wx_oauth']        = 'wxch-ent.php?act=oauth';


?>
