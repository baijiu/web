<?php
define('IN_ECTOUCH', true);
require(dirname(__FILE__) . '/includes/init.php');
if($_REQUEST['act'] == 'list'){
    /* 检查权限 */
	admin_priv('stock_order');
    $smarty->assign('ur_here','投资列表');

	

	$goodsNo = $_REQUEST['goodsNo'];
    $goods = goods($goodsNo);
    $smarty->assign('goods',   $goods['goods']);
    $smarty->assign('filter',       $goods['filter']);
    $smarty->assign('record_count', $goods['record_count']);
    $smarty->assign('page_count',   $goods['page_count']);
	$smarty->assign('allamout',   $goods['allamout']);

	$smarty->assign('all_amount',   $goods['all_amount']);
    $smarty->assign('full_page',    1);
    $smarty->assign('sort_user_id', '<img src="images/sort_desc.gif">');
    assign_query_info();
    $smarty->display('sys_stockorder.htm');
}

/*------------------------------------------------------ */
//-- ajax返回用户列表
/*------------------------------------------------------ */

elseif($_REQUEST['act'] == 'query'){
    $goodsNo = $_REQUEST['goodsNo'];
    $goods = goods($goodsNo);
    $smarty->assign('goods',    $goods['goods']);
    $smarty->assign('filter',       $goods['filter']);
    $smarty->assign('record_count', $goods['record_count']);
    $smarty->assign('page_count',   $goods['page_count']);
	$smarty->assign('allamout',   $goods['allamout']);
	$smarty->assign('all_amount',   $goods['all_amount']);
    $sort_flag  = sort_flag($goods['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('sys_stockorder.htm'), '', array('filter' => $goods['filter'], 'page_count' => $goods['page_count']));
}

function goods($goodsNo=''){
        /* 过滤条件 */
		
	if($goodsNo)$filter['goodsNo'] = $goodsNo;
	$filter['user_id'] = empty($_REQUEST['user_id']) ? '' : trim($_REQUEST['user_id']);
	$filter['mobile_phone'] = empty($_REQUEST['mobile_phone']) ? '' : trim($_REQUEST['mobile_phone']);


	$filter['order_sn'] = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
	$filter['starttime'] = empty($_REQUEST['starttime']) ? '' : trim($_REQUEST['starttime']);	
	$filter['endtime'] = empty($_REQUEST['endtime']) ? '' : trim($_REQUEST['endtime']);	

	$filter['keywords'] = empty($_REQUEST['keywords']) ? '' : trim($_REQUEST['keywords']);
    if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1){
        $filter['keywords'] = json_str_iconv($filter['keywords']);
    }
    $filter['goods_name'] = empty($_REQUEST['goods_name']) ? '' : trim($_REQUEST['goods_name']);

	$filter['sort_by'] = empty($_REQUEST['sort_by'])? 'id' : trim($_REQUEST['sort_by']);
	$filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC': trim($_REQUEST['sort_order']);
	$ex_where = ' WHERE is_pay = 1 ';
	if ($filter['mobile_phone']){
		$user_id = $GLOBALS['db']->getOne("select user_id from ecs_users where mobile_phone = '".$filter['mobile_phone']."'");
		$ex_where .= " AND user_id = '".$user_id."'";
	}
	
	if ($filter['keywords']){
		$userid = $GLOBALS['db']->getOne("select user_id from ecs_users where user_name = '".$filter['keywords']."'");
		$ex_where .= " AND user_id = '".$userid."'";
	}

    if ($filter['goods_name']){
        $goods_id = $GLOBALS['db']->getOne("select id from ecs_productinfo where goods_name = '".$filter['goods_name']."'");
        $ex_where .= " AND goods_id = '".$goods_id."'";
    }

	if ($filter['user_id']){
		$ex_where .= " AND user_id ='".$filter['user_id']."'";
	}
	
	if ($filter['order_sn']){
		$ex_where .= " AND order_sn ='".$filter['order_sn']."'";
	}
	


	if ($filter['starttime']){
		
		$filter['starttime'] = strtotime($filter['starttime']);
	
		$ex_where .= " AND add_time >='".$filter['starttime']."'";
	}
	
	if ($filter['endtime']){
		
		$filter['endtime'] = strtotime($filter['endtime'])+86400-1;
	
		$ex_where .= " AND add_time <='".$filter['endtime']."'";
	}

	$filter['record_count'] = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM ecs_product_order". $ex_where."");
        /* 分页大小 */
	$filter = page_and_size($filter);
	$sql = "SELECT * FROM ecs_product_order".$ex_where.
			"ORDER by ". $filter['sort_by'] . ' ' . $filter['sort_order'] .
			" LIMIT " . $filter['start'] . ',' . $filter['page_size'];
	$filter['keywords'] = stripslashes($filter['keywords']);
	set_filter($filter, $sql);
    $goods = $GLOBALS['db']->getAll($sql);



	foreach($goods as $key => $val){
	
		$goods[$key]['time'] = date("Y-m-d H:i:s",$val['add_time']);
		if($val['end_time']>0){
            $goods[$key]['endtime'] = date("Y-m-d H:i:s",$val['end_time']);
        }

		$row = $GLOBALS['db']->getRow("select user_name,mobile_phone from ecs_users where user_id = '".$val['user_id']."'");
		$goods[$key]['user_name'] = $row['user_name'];
		$goods[$key]['mobile_phone'] = $row['mobile_phone'];

        if($val['state']==1){
            $goods[$key]['status'] ='已完成';
        }else{
            $goods[$key]['status'] ='进行中';
        }

        $goods[$key]['goods_name'] = $GLOBALS['db']->getone("select goods_name from ecs_productinfo where id = '".$val['goods_id']."'");
		
	}
    $arr = array(  'goods' => $goods, 'filter' => $filter,  'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    return $arr;
}





























?>