<?php
define('IN_ECTOUCH', true);
require(dirname(__FILE__) . '/includes/init.php');

if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}
else
{
    $_REQUEST['act'] = trim($_REQUEST['act']);
}
if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here','提现列表');
    $transaction_log = transaction_log();
    $smarty->assign('transaction_log',   $transaction_log['transaction_log']);
    $smarty->assign('filter',       $transaction_log['filter']);
    $smarty->assign('record_count', $transaction_log['record_count']);
    $smarty->assign('page_count',   $transaction_log['page_count']);
    $smarty->assign('full_page',    1);
    $smarty->assign('sort_user_id', '<img src="images/sort_desc.gif">');
    assign_query_info();
    $smarty->display('user_withdraw_list.htm');
}

/*------------------------------------------------------ */
//-- ajax返回列表
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $transaction_log = transaction_log();
    $smarty->assign('transaction_log',    $transaction_log['transaction_log']);
    $smarty->assign('filter',       $transaction_log['filter']);
    $smarty->assign('record_count', $transaction_log['record_count']);
    $smarty->assign('page_count',   $transaction_log['page_count']);
    $sort_flag  = sort_flag($transaction_log['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    make_json_result($smarty->fetch('user_withdraw_list.htm'), '', array('filter' => $transaction_log['filter'], 'page_count' => $transaction_log['page_count']));
}
elseif ($_REQUEST['act'] == 'check_ok'){
    $id = $_GET['id'];
    $truntime = time();
    $account = $db->getRow("SELECT * FROM ecs_withdraw WHERE id = '$id' and state = 0");
    if($account){
        $chack=	$GLOBALS['db']->query("update ecs_withdraw set truntime='".$truntime."',state=1 where id='".$id."'");
        if($chack){
            $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'user_withdraw.php?act=list');
            sys_msg("操作成功", 0, $lnk);
        }
        else
        {
            sys_msg("操作失败");
        }

    }else{
        sys_msg("非法操作");
    }

}

elseif ($_REQUEST['act'] == 'check_no'){
    $id = $_GET['id'];
    if($id){
        $row = $db->getrow("select * from ecs_withdraw where  id='".$id."'  and state ='0'");
        if($row['state']=='0'){
            $chack = $db->query("update ecs_withdraw set state=2 ,truntime='".time()."' where  id='".$id."'");
            if ($chack){
                if($row['ti_type']==0){
                    log_account_change($row['user_id'],$row['amount'],0,0,0,"取消提现",99);
                }elseif($row['ti_type']==1){
                    log_account_change_new($row['user_id'],0,0,0,0,$row['amount'],"取消提现",99);
                }

                $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'user_withdraw.php?act=list');
                sys_msg("取消成功", 0, $lnk);
            }
        }else{
            sys_msg("非法操作", 0, $lnk);
        }

    }else{
        sys_msg("非法操作", 0, $lnk);
    }
}


elseif ($_REQUEST['act'] == 'daochu'){
	require(dirname(__FILE__) . '/includes/Excel/Classes/PHPExcel.php');
	require(dirname(__FILE__) . '/includes/Excel/Classes/PHPExcel/Writer/Excel2007.php');
	$info = $db->getAll("select * from ecs_withdraw where  order by id desc");
	$res = push($info,date("YmdHis"));
}

function transaction_log(){
        /* 过滤条件 */
	$filter['keywords'] = empty($_REQUEST['keywords']) ? '' : trim($_REQUEST['keywords']);
    if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1){
        $filter['keywords'] = json_str_iconv($filter['keywords']);
    }
    $filter['state'] = $_REQUEST['state'];
	$filter['createtime1'] = empty($_REQUEST['createtime1'])? '' : trim($_REQUEST['createtime1']);
	$filter['createtime2'] = empty($_REQUEST['createtime2'])? '' : trim($_REQUEST['createtime2']);
	$filter['sort_by']    = empty($_REQUEST['sort_by'])    ? 'id' : trim($_REQUEST['sort_by']);
	$filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
	$ex_where = ' WHERE 1 ';
	if ($filter['keywords']){
		$user_id = $GLOBALS['db']->getOne("select user_id from ecs_users where user_name = '".$filter['keywords']."'");
		$ex_where .= " AND user_id ='".$user_id."'";
	}
    if($filter['state']!=''){
        $ex_where .= " and state='".$filter['state']."'";
    }

	if($filter['createtime1']){
	    $start_time = strtotime($filter['createtime1']);
		$ex_where .= " and createtime>='".$start_time."'";
	}
	if($filter['createtime2']){

        $end_time= strtotime($filter['createtime2'])+86400-1;

        $ex_where .= " and createtime<='".$end_time."'";
	}

	$filter['record_count'] = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM ecs_withdraw ". $ex_where);
        /* 分页大小 */
	$filter = page_and_size($filter);
	$sql = "SELECT * FROM ecs_withdraw".$ex_where.
			"  ORDER by ". $filter['sort_by'] . ' ' . $filter['sort_order'] .
			" LIMIT " . $filter['start'] . ',' . $filter['page_size'];
	$filter['keywords'] = stripslashes($filter['keywords']);
	set_filter($filter, $sql);
    $transaction_log = $GLOBALS['db']->getAll($sql);

	foreach($transaction_log as $key => $val){
        $transaction_log[$key]['createtime'] = date('Y-m-d H:i:s',$val['createtime']);
        if($val['truntime']){
            $transaction_log[$key]['truntime'] =date('Y-m-d H:i:s',$val['truntime']);
        }

        $transaction_log[$key]['user_name'] =$GLOBALS['db']->getOne("select user_name from ecs_users where user_id = '".$val['user_id']."'");
	}
    $arr = array('transaction_log' => $transaction_log, 'filter' => $filter,'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    return $arr;
}


/* 导出excel函数*/
function push($info,$name){
    $objPHPExcel = new PHPExcel();
    //保存excel—2007格式
    $objWriter = new PHPExcel_Writer_Excel2007( $objPHPExcel );
    //创建人
    $objPHPExcel->getProperties()->setCreator( "ZYB" );
    //最后修改人
    $objPHPExcel->getProperties()->setLastModifiedBy( "ZYB" );
    //标题
    $objPHPExcel->getProperties()->setTitle( "Office 2007 XLSX Test Document" );
    //题目
    $objPHPExcel->getProperties()->setSubject( "Office 2007 XLSX Test Document" );
    //描述
    $objPHPExcel->getProperties()->setDescription( "Test document for Office 2007 XLSX, generated using PHP classes." );
    //关键字
    $objPHPExcel->getProperties()->setKeywords( "office 2007 openxml php" );
    //种类
    $objPHPExcel->getProperties()->setCategory( "Test result file" );
    //
    //设置当前的sheet
    $objPHPExcel->setActiveSheetIndex( 0 );
    //设置sheet的name
    $objPHPExcel->getActiveSheet()->setTitle( '导出表测试' );
    //设置单元格的值
    $subTitle = array( '编号','账号', 'ID', '金额', '实际到账','申请时间','审核时间');
    $datas = array();
    $j=0;
    foreach($info as $key => $val){
        $datas[$j] = array($val['id'], $val['userName'].' ', $val['userName2'], $val['amount'], $val['amountrun'], $val['createtime'], $val['truntime']);
        $j++;
    }
    $colspan = range( 'A', 'K' );

    $count = count( $subTitle );
    // 标题输出
    for ( $index = 0; $index < $count; $index++ ) {
        $col = $colspan[$index];
        $objPHPExcel->getActiveSheet()->setCellValue( $col . '1', $subTitle[$index] );
        //设置font
        $objPHPExcel->getActiveSheet()->getStyle( $col . '1' )->getFont()->setName( 'Candara' );
        $objPHPExcel->getActiveSheet()->getStyle( $col . '1' )->getFont()->setSize( 12 );
        $objPHPExcel->getActiveSheet()->getStyle( $col . '1' )->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle( $col . '1' )->getFont()->getColor()
                ->setARGB( PHPExcel_Style_Color::COLOR_WHITE );

        //设置填充色彩
        $objPHPExcel->getActiveSheet()->getStyle( $col . '1' )->getFill()
                ->setFillType( PHPExcel_Style_Fill::FILL_SOLID );
        $objPHPExcel->getActiveSheet()->getStyle( $col . '1' )->getFill()->getStartColor()->setARGB( 'FF808080' );
        // align 设置居中
        $objPHPExcel->getActiveSheet()->getStyle( $col . '1' )->getAlignment()
                ->setHorizontal( PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
       if ( $subTitle[$index] == '账号' || $subTitle[$index] == '申请时间' || $subTitle[$index] == '审核时间') {
            // 设置宽度
            $objPHPExcel->getActiveSheet()->getColumnDimension( $col )->setWidth( 20 );
       }

    }
    // 内容输出
    foreach ( $datas as $key => $value ) {
        $colNumber = $key + 2; //第二行开始才是内容
        foreach ( $colspan as $colKey => $col ) {
            $objPHPExcel->getActiveSheet()->setCellValue( $col . $colNumber, $value[$colKey] );
        }
    }
    //
    //在默认sheet后，创建一个worksheet
    $objPHPExcel->createSheet();
    $fileName = $name.".xlsx";
    $objWriter->save($fileName);
    download($fileName, true);
    exit;
}

function download( $fileName, $delDesFile = false, $isExit = true ) { 
    if ( file_exists( $fileName ) ) { 
        header( 'Content-Description: File Transfer' ); 
        header( 'Content-Type: application/octet-stream' ); 
        header( 'Content-Disposition: attachment;filename = ' . basename( $fileName ) ); 
        header( 'Content-Transfer-Encoding: binary' ); 
        header( 'Expires: 0' ); 
        header( 'Cache-Control: must-revalidate, post-check = 0, pre-check = 0' ); 
        header( 'Pragma: public' ); 
        header( 'Content-Length: ' . filesize( $fileName ) ); 
        ob_clean(); 
        flush(); 
        readfile( $fileName ); 
        if ( $delDesFile ) { 
            unlink( $fileName ); 
        } 
        if ( $isExit ) { 
            exit; 
        } 
    } 
}
?>