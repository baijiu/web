<?php

/**
 * GAOXIN 会员管理程序
 * ============================================================================
 * * 版权所有 2005-2012 ，并保留所有权利。
 * 网站地址: http://www.gxwlr.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: users.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'include/lib_weixintong.php');

/*------------------------------------------------------ */
//-- 用户帐号列表
/*------------------------------------------------------ */



if ($_REQUEST['act'] == 'list')
{
    /* 检查权限 */
    admin_priv('users_manage');

    $smarty->assign('ur_here',      $_LANG['03_users_list']);
    $smarty->assign('action_link',  array('text' => $_LANG['04_users_add'], 'href'=>'users.php?act=add'));
    $user_list = user_list();
	$user_list1=$user_list['user_list'];
	foreach($user_list1 as $aa){
		$user_id=$aa['user_id'];
		$sql = "SELECT count(user_id)  FROM ecs_users  where parent_id='$user_id' ";
		$aa['number']=$db->getone($sql);
		if($aa['parent_id']){
            $sql = "SELECT user_name  FROM ecs_users  where user_id='$aa[parent_id]' ";
            $aa['up_name']=$db->getone($sql);
        }
		$kk[]=$aa;
	}
    $smarty->assign('user_list',   $kk);
    $smarty->assign('filter',       $user_list['filter']);
    $smarty->assign('record_count', $user_list['record_count']);
    $smarty->assign('page_count',   $user_list['page_count']);
    $smarty->assign('full_page',    1);
    $smarty->assign('sort_user_id', '<img src="images/sort_desc.gif">');

    assign_query_info();
    $smarty->display('users_list.htm');
}

/*------------------------------------------------------ */
//-- ajax返回用户列表
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $user_list = user_list();
	$user_list1=$user_list['user_list'];
	foreach($user_list1 as $aa){
		$user_id=$aa['user_id'];

        $sql = "SELECT count(user_id)  FROM ecs_users  where parent_id='$user_id' ";
        $aa['number']=$db->getone($sql);
        if($aa['parent_id']){
            $sql = "SELECT user_name  FROM ecs_users  where user_id='$aa[parent_id]' ";
            $aa['up_name']=$db->getone($sql);
        }
		$kk[]=$aa;
	}
	
    $smarty->assign('user_list',    $kk);
    $smarty->assign('filter',       $user_list['filter']);
    $smarty->assign('record_count', $user_list['record_count']);
    $smarty->assign('page_count',   $user_list['page_count']);

    $sort_flag  = sort_flag($user_list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('users_list.htm'), '', array('filter' => $user_list['filter'], 'page_count' => $user_list['page_count']));
}

/*------------------------------------------------------ */
//-- 添加会员帐号
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add')
{
    /* 检查权限 */
    admin_priv('users_manage');

    $user = array(
                    'credit_line'   => 0
                    );


    $smarty->assign('ur_here',          $_LANG['04_users_add']);
    $smarty->assign('action_link',      array('text' => $_LANG['03_users_list'], 'href'=>'users.php?act=list'));
    $smarty->assign('form_action',      'insert');
    $smarty->assign('user',             $user);

    assign_query_info();
    $smarty->display('user_info.htm');
}

/*------------------------------------------------------ */
//-- 添加会员帐号
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'insert')
{
    /* 检查权限 */
    admin_priv('users_manage');
    $username = empty($_POST['username']) ? '' : trim($_POST['username']);
    $password = empty($_POST['password']) ? '' : trim($_POST['password']);

    $parent_id= empty($_POST['parent_id']) ? '0' : intval($_POST['parent_id']);
    $user_level = empty($_POST['user_level']) ? '0' : intval($_POST['user_level']);
    if(!empty($parent_id))
    {
        $sql = 'SELECT * FROM ' . $ecs->table('users') . "WHERE user_id ='$parent_id'";
        $tianxinarr=$db->getRow($sql);
        if(empty($tianxinarr)){
            sys_msg("上级ID不存在,请重新输入", 0);

        }
    }

    $mobile_phone = empty($_POST['mobile_phone']) ? '' : trim($_POST['mobile_phone']);
    if(empty($mobile_phone)){
        sys_msg("请输入手机号", 0);
    }else{
        $one = $db->getOne("select mobile_phone from ecs_users where mobile_phone = '$mobile_phone'");
        if($one)
        {
            sys_msg('该手机号已注册,请重新输入！', 1);
        }
    }


    $users =& init_users();

    if (!$users->add_user($username, $password))
    {
        /* 插入会员数据失败 */
        if ($users->error == ERR_INVALID_USERNAME)
        {
            $msg = $_LANG['username_invalid'];
        }
        elseif ($users->error == ERR_USERNAME_NOT_ALLOW)
        {
            $msg = $_LANG['username_not_allow'];
        }
        elseif ($users->error == ERR_USERNAME_EXISTS)
        {
            $msg = $_LANG['username_exists'];
        }
        elseif ($users->error == ERR_INVALID_EMAIL)
        {
            $msg = $_LANG['email_invalid'];
        }
        elseif ($users->error == ERR_EMAIL_NOT_ALLOW)
        {
            $msg = $_LANG['email_not_allow'];
        }
        elseif ($users->error == ERR_EMAIL_EXISTS)
        {
            $msg = $_LANG['email_exists'];
        }
        else
        {
        }
        sys_msg($msg, 1);
    }



    /* 更新会员的其它信息 */
    $other =  array();
    $other['parent_id'] = $parent_id;
    $other['mobile_phone'] = $mobile_phone;
    $other['reg_time'] = time();
    $other['user_level'] = $user_level;
    $db->autoExecute($ecs->table('users'), $other, 'UPDATE', "user_name = '$username'");

    /* 记录管理员操作 */
    admin_log($_POST['username'], 'add', 'users');

    /* 提示信息 */
    $link[] = array('text' => $_LANG['go_back'], 'href'=>'users.php?act=list');
    sys_msg(sprintf($_LANG['add_success'], htmlspecialchars(stripslashes($_POST['username']))), 0, $link);

}

/*------------------------------------------------------ */
//-- 编辑用户帐号
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit')
{
    /* 检查权限 */
    admin_priv('users_manage');


    $sql = "SELECT u.user_level,u.user_id,u.user_name,  u.sex, u.birthday, u.pay_points, u.rank_points, u.user_rank , u.user_money, u.frozen_money, u.credit_line, u.parent_id, u2.user_name as parent_username, u.qq, u.msn,
    u.office_phone, u.home_phone, u.mobile_phone,u.accountname, u.bankname, u.accountnumber, u.bank_address, u.id_card".
        " FROM " .$ecs->table('users'). " u LEFT JOIN " . $ecs->table('users') . " u2 ON u.parent_id = u2.user_id WHERE u.user_id='$_GET[id]'";

    $row = $db->GetRow($sql);

    if ($row)
    {
        $user['user_id']        = $row['user_id'];
        $user['user_name']        = $row['user_name'];
        $user['pay_points']     = $row['pay_points'];
        $user['formated_user_money'] = price_format($row['user_money']);
        $user['parent_id']      = $row['parent_id'];
        $user['parent_username']= $row['parent_username'];
        $user['mobile_phone']   = $row['mobile_phone'];
        $user['user_level']   = $row['user_level'];

        $user['accountname']= $row['accountname'];
        $user['bankname']   = $row['bankname'];
        $user['accountnumber']   = $row['accountnumber'];
        $user['bank_address']   = $row['bank_address'];
        $user['id_card']   = $row['id_card'];
    }
    else
    {
          $link[] = array('text' => $_LANG['go_back'], 'href'=>'users.php?act=list');
          sys_msg($_LANG['username_invalid'], 0, $links);

     }

    assign_query_info();
    $smarty->assign('ur_here',          $_LANG['users_edit']);
    $smarty->assign('action_link',      array('text' => $_LANG['03_users_list'], 'href'=>'users.php?act=list&' . list_link_postfix()));
    $smarty->assign('user',             $user);
    $smarty->assign('form_action',      'update');

    $smarty->display('user_info.htm');
}

/*------------------------------------------------------ */
//-- 更新用户帐号
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'update')
{
    /* 检查权限 */
    admin_priv('users_manage');
    $username = empty($_POST['username']) ? '' : trim($_POST['username']);
    $password = empty($_POST['password']) ? '' : trim($_POST['password']);
    $email = empty($_POST['email']) ? '' : trim($_POST['email']);
    $sex = empty($_POST['sex']) ? 0 : intval($_POST['sex']);
    $sex = in_array($sex, array(0, 1, 2)) ? $sex : 0;
    $birthday = $_POST['birthdayYear'] . '-' .  $_POST['birthdayMonth'] . '-' . $_POST['birthdayDay'];

    $parent_id= empty($_POST['parent_id']) ? '0' : intval($_POST['parent_id']);

    $user_level = empty($_POST['user_level']) ? '0' : intval($_POST['user_level']);

    $accountname = empty($_POST['accountname']) ? '' : trim($_POST['accountname']);
    $bankname = empty($_POST['bankname']) ? '' : trim($_POST['bankname']);
    $accountnumber = empty($_POST['accountnumber']) ? '' : trim($_POST['accountnumber']);


    $bank_address= empty($_POST['bank_address']) ? '' : trim($_POST['bank_address']);
    $id_card = empty($_POST['id_card']) ? '' : trim($_POST['id_card']);

    if(!empty($parent_id))
    {
        $sql = 'SELECT * FROM ' . $ecs->table('users') . "WHERE user_id ='$parent_id' and user_name != '".$username."'";
        $tianxinarr=$db->getRow($sql);
        if(empty($tianxinarr)){
            sys_msg("上级ID不存在,请重新输入", 0);

        }
    }

    $mobile_phone = empty($_POST['mobile_phone']) ? '' : trim($_POST['mobile_phone']);
    if(empty($mobile_phone)){
        sys_msg("请输入手机号", 0);
    }else{
        $one = $db->getOne("select mobile_phone from ecs_users where mobile_phone = '$mobile_phone'  and user_name != '".$username."'");
        if($one)
        {
            sys_msg('该手机号已注册,请重新输入！', 1);
        }
    }


    $users  =& init_users();

    if (!$users->edit_user(array('username'=>$username, 'password'=>$password, 'email'=>$email, 'gender'=>$sex, 'bday'=>$birthday ), 1))
    {
        if ($users->error == ERR_EMAIL_EXISTS)
        {
            $msg = $_LANG['email_exists'];
        }
        else
        {
            $msg = $_LANG['edit_user_failed'];
        }
        sys_msg($msg, 1);
    }
    if(!empty($password))
    {
			$sql="UPDATE ".$ecs->table('users'). "SET `ec_salt`='0' WHERE user_name= '".$username."'";
			$db->query($sql);
	}


    /* 更新会员的其它信息 */
    $other =  array();
    $other['parent_id'] = $parent_id;
    $other['mobile_phone'] = $mobile_phone;
    $other['user_level'] = $user_level;
    $other['accountname'] = $accountname;
    $other['bankname'] = $bankname;
    $other['accountnumber'] = $accountnumber;
    $other['bank_address'] = $bank_address;
    $other['id_card'] = $id_card;
    $db->autoExecute($ecs->table('users'), $other, 'UPDATE', "user_name = '$username'");

    /* 记录管理员操作 */
    admin_log($username, 'edit', 'users');

    /* 提示信息 */
    $links[0]['text']    = $_LANG['goto_list'];
    $links[0]['href']    = 'users.php?act=list&' . list_link_postfix();
    $links[1]['text']    = $_LANG['go_back'];
    $links[1]['href']    = 'javascript:history.back()';

    sys_msg($_LANG['update_success'], 0, $links);

}

/*------------------------------------------------------ */
//-- 批量删除会员帐号
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'batch_remove')
{
    /* 检查权限 */
    admin_priv('users_drop');

    if (isset($_POST['checkboxes']))
    {
        $sql = "SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id " . db_create_in($_POST['checkboxes']);
        $col = $db->getCol($sql);
        $usernames = implode(',',addslashes_deep($col));
        $count = count($col);
        /* 通过插件来删除用户 */
        $users =& init_users();
        $users->remove_user($col);

        admin_log($usernames, 'batch_remove', 'users');

        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'users.php?act=list');
        sys_msg(sprintf($_LANG['batch_remove_success'], $count), 0, $lnk);
    }
    else
    {
        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'users.php?act=list');
        sys_msg($_LANG['no_select_user'], 0, $lnk);
    }
}



/*------------------------------------------------------ */
//-- 删除会员帐号
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'remove')
{
    /* 检查权限 */
    admin_priv('users_drop');

    $sql = "SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id = '" . $_GET['id'] . "'";
    $username = $db->getOne($sql);
    /* 通过插件来删除用户 */
    $users =& init_users();
    $users->remove_user($username); //已经删除用户所有数据

    /* 记录管理员操作 */
    admin_log(addslashes($username), 'remove', 'users');

    /* 提示信息 */
    $link[] = array('text' => $_LANG['go_back'], 'href'=>'users.php?act=list');
    sys_msg(sprintf($_LANG['remove_success'], $username), 0, $link);
}

/*------------------------------------------------------ */
//--  收货地址查看
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'address_list')
{
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    $sql = "SELECT a.*, c.region_name AS country_name, p.region_name AS province, ct.region_name AS city_name, d.region_name AS district_name ".
           " FROM " .$ecs->table('user_address'). " as a ".
           " LEFT JOIN " . $ecs->table('region') . " AS c ON c.region_id = a.country " .
           " LEFT JOIN " . $ecs->table('region') . " AS p ON p.region_id = a.province " .
           " LEFT JOIN " . $ecs->table('region') . " AS ct ON ct.region_id = a.city " .
           " LEFT JOIN " . $ecs->table('region') . " AS d ON d.region_id = a.district " .
           " WHERE user_id='$id'";
    $address = $db->getAll($sql);
    $smarty->assign('address',          $address);
    assign_query_info();
    $smarty->assign('ur_here',          $_LANG['address_list']);
    $smarty->assign('action_link',      array('text' => $_LANG['03_users_list'], 'href'=>'users.php?act=list&' . list_link_postfix()));
    $smarty->display('user_address_list.htm');
}

/*------------------------------------------------------ */
//-- 脱离推荐关系
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'remove_parent')
{
    /* 检查权限 */
    admin_priv('users_manage');

    $sql = "UPDATE " . $ecs->table('users') . " SET parent_id = 0 WHERE user_id = '" . $_GET['id'] . "'";
    $db->query($sql);

    /* 记录管理员操作 */
    $sql = "SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id = '" . $_GET['id'] . "'";
    $username = $db->getOne($sql);
    admin_log(addslashes($username), 'edit', 'users');

    /* 提示信息 */
    $link[] = array('text' => $_LANG['go_back'], 'href'=>'users.php?act=list');
    sys_msg(sprintf($_LANG['update_success'], $username), 0, $link);
}


/*------------------------------------------------------ */
//-- 查看用户分销下线列表
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'share_list')
{
    /* 检查权限 */
    admin_priv('users_manage');
    $smarty->assign('ur_here',      $_LANG['03_users_list']);
    $user_list = array();
	$auid = $_GET['id'];
    $up_uid = "'$auid'";
    $all_count = 0;
    $i = 1;
    do{
        $count = 0;
        $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE parent_id IN($up_uid)";
        $query = $db->query($sql);
        $up_uid = '';
        while ($rt = $db->fetch_array($query))
        {
            $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
            $count++;
        }
        $all_count += $count;
        if ($count)
        {
            $sql = "SELECT user_id, user_name, '$i' AS level,  user_money,   pay_points, reg_time,wxid ,parent_id".
                " FROM " . $GLOBALS['ecs']->table('users') . " WHERE user_id IN($up_uid)" .
                " ORDER by level, user_id";
            $user_info=$db->getAll($sql);
            foreach($user_info as $k=>$v){
                $user_info[$k]['up_name'] = $db->getone("select user_name from ecs_users where user_id='".$v['parent_id']."'");

                $sql = "SELECT count(user_id)  FROM ecs_users  where parent_id='$v[user_id]' ";
                $user_info[$k]['number']=$db->getone($sql);

                $user_info[$k]['reg_time']=local_date($GLOBALS['_CFG']['date_format'], $v['reg_time']);
            }
            $user_list[$i]['level'] = $i;
            $user_list[$i]['name'] = $i.'级会员';
            $user_list[$i]['user'] = $user_info;
        }
        $i++;
    }while($up_uid);

    $uname = $db->getone("select user_name from ecs_users where user_id = '$auid'");
    $user_list['record_count'] = $all_count;
    $smarty->assign('user_list', $user_list);
    $smarty->assign('uname', $uname);
    $smarty->assign('record_count', $user_list['record_count']);
    $smarty->assign('full_page',    1);
    assign_query_info();
    $smarty->display('share_list.htm');
}

/**
 *  返回用户列表数据
 *
 * @access  public
 * @param
 *
 * @return void
 */
function user_list()
{
    $result = get_filter();
    if ($result === false)
    {
        /* 过滤条件 */
        $filter['keywords'] = empty($_REQUEST['keywords']) ? '' : trim($_REQUEST['keywords']);
        if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
        {
            $filter['keywords'] = json_str_iconv($filter['keywords']);
        }
        $filter['sort_by']    = empty($_REQUEST['sort_by'])    ? 'user_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC'     : trim($_REQUEST['sort_order']);

        $ex_where = ' WHERE 1 ';
        if ($filter['keywords'])
        {
            $ex_where .= " AND user_name LIKE '%" . mysqli_like_quote($filter['keywords']) ."%'";
        }
        $filter['record_count'] = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('users') . $ex_where);

        /* 分页大小 */
        $filter = page_and_size($filter);
        $sql = "SELECT * ".
                " FROM " . $GLOBALS['ecs']->table('users') . $ex_where .
                " ORDER by " . $filter['sort_by'] . ' ' . $filter['sort_order'] .
                " LIMIT " . $filter['start'] . ',' . $filter['page_size'];

        $filter['keywords'] = stripslashes($filter['keywords']);
        set_filter($filter, $sql);
    }
    else
    {
        $sql    = $result['sql'];
        $filter = $result['filter'];
    }

    $user_list = $GLOBALS['db']->getAll($sql);

    $count = count($user_list);
    $team_rank =  array('普通团长','一级团长','二级团长','三级团长','四级团长','五级团长');
    $user_levels = array('普通会员','白银会员','黄金会员','钻石会员','皇冠会员','名誉股东');
    for ($i=0; $i<$count; $i++)
    {
        $user_list[$i]['reg_time'] = local_date($GLOBALS['_CFG']['date_format'], $user_list[$i]['reg_time']);

        $user_list[$i]['rank_name'] = $user_levels[$user_list[$i]['user_level']];

        $user_list[$i]['zongchongzhi'] = $GLOBALS['db']->getone("select sum(amount) from ecs_recharge where user_id = '".$user_list[$i]['user_id']."' and paystate = 1");

        $user_list[$i]['all_touzi'] = $GLOBALS['db']->getone("select sum(amount) from ecs_product_order where user_id = '".$user_list[$i]['user_id']."' and is_pay = 1");

        $all_uid = all_uid($user_list[$i]['user_id']);
        $user_list[$i]['allyeji'] = $GLOBALS['db']->getone("select sum(amount) from ecs_product_order where user_id in($all_uid) and is_pay = 1");

        $user_list[$i]['tuanduichongzhi'] = $GLOBALS['db']->getone("select sum(amount) from ecs_recharge where user_id in($all_uid) and paystate = 1");


        $user_list[$i]['team_name'] = $team_rank[$user_list[$i]['team_level']];

    }

    $arr = array('user_list' => $user_list, 'filter' => $filter,
        'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}
/*function team_count($auid){
    $up_uid = "'$auid'";
    $all_count = 0;
    do{
        $count = 0;
        $sql = "SELECT user_id FROM ecs_users WHERE parent_id IN($up_uid)";
        $query = $GLOBALS['db']->query($sql);
        $up_uid = '';
        while ($rt = $GLOBALS['db']->fetch_array($query))
        {
            $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
            $count++;
        }
        $all_count += $count;
    }while($up_uid);
    return $all_count;
}


function team_rank($user_id,$yeji,$next_num){
    $db =  $GLOBALS['db'];
    $set = $db->getOne("select yejijiang from ecs_set where id = 1");
    $set = explode("|",$set);
    foreach ($set as $val=>$key){
        $set[$val] = explode(",",$key);
    }
    $teamname = '普通团长';
    foreach($set as $k=>$v){
        if($next_num>=$v[0]&&$yeji>=$v[1]){
            $level = $k+1;
            $teamname = $level.'级团长';

        }
    }
    return $teamname;
}*/

function all_uid($auid){
    $db =  $GLOBALS['db'];

    $up_uid = "'$auid'";
    $all_uid = "'$auid'";

    do{

        $sql = "SELECT user_id FROM ecs_users WHERE parent_id IN($up_uid)";
        $query = $db->query($sql);
        $up_uid = '';
        while ($rt = $db->fetch_array($query))
        {
            $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
            $all_uid .= $all_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
        }



    }while($up_uid);

    return $all_uid;
}

?>