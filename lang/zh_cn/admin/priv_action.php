<?php

/**
 * GAOXIN 权限名称语言文件
 * ============================================================================
 * * 版权所有 2005-2012 ，并保留所有权利。
 * 网站地址: http://www.gxwlr.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: priv_action.php 17217 2011-01-19 06:29:08Z liubo $
*/
/* 权限管理的一级分组 */
$_LANG['goods'] = '商品管理';
$_LANG['cms_manage'] = '文章管理';
$_LANG['users_manage'] = '会员管理';
$_LANG['priv_manage'] = '权限管理';
$_LANG['sys_manage'] = '系统设置';
$_LANG['order_manage'] = '订单管理';
$_LANG['wechat'] = '微信通';

//商品管理部分的权限
$_LANG['goods_manage'] = '商品添加/编辑';
$_LANG['remove_back'] = '商品删除/恢复';
$_LANG['cat_manage'] = '分类添加/编辑';
$_LANG['cat_drop'] = '分类转移/删除';
$_LANG['attr_manage'] = '商品属性管理';
$_LANG['brand_manage'] = '商品品牌管理';
$_LANG['comment_priv'] = '用户评论管理';//
$_LANG['goods_type'] = '商品类型';
$_LANG['exchange_goods'] = '积分商城';

//文章管理部分的权限
$_LANG['article_cat'] = '文章分类管理';
$_LANG['article_manage'] = '文章内容管理';

//会员信息管理
$_LANG['feedback_priv'] = '会员留言管理';
$_LANG['users_drop'] = '会员删除';
$_LANG['user_rank'] = '会员等级管理';
$_LANG['users_manages'] = '会员添加/编辑';
$_LANG['surplus_manage'] = '会员余额管理';
$_LANG['account_manage'] = '会员账户管理';
$_LANG['user_recharge'] = '充值管理';
$_LANG['user_withdraw'] = '提现管理';

//权限管理部分的权限
$_LANG['admin_manage'] = '管理员添加/编辑';
$_LANG['admin_drop'] = '删除管理员';
$_LANG['allot_priv'] = '分派权限';
$_LANG['logs_manage'] = '管理日志列表';
$_LANG['logs_drop'] = '删除管理日志';
$_LANG['role_manage'] = '角色管理';

//系统设置部分权限
$_LANG['mail_settings'] = '邮件服务器设置';
$_LANG['shop_config'] = '基本设置';
$_LANG['ship_manage'] = '配送方式管理';
$_LANG['payment'] = '支付方式管理';
$_LANG['shiparea_manage'] = '配送区域管理';
$_LANG['area_manage'] = '地区列表管理';
$_LANG['ad_manage'] = '广告管理';
$_LANG['navigator'] = '自定义导航栏';
$_LANG['template_setup']  = '模板设置';
$_LANG['parameter_set'] = '参数设置';


//订单管理部分权限
$_LANG['order_os_edit'] = '编辑订单状态';
$_LANG['order_ps_edit'] = '编辑付款状态';
$_LANG['order_ss_edit'] = '编辑发货状态';
$_LANG['order_edit'] = '添加编辑订单';
$_LANG['order_view'] = '查看未完成订单';
$_LANG['order_view_finished'] = '查看已完成订单';
$_LANG['repay_manage'] = '退款申请管理';
$_LANG['sale_order_stats'] = '订单销售统计';
$_LANG['client_flow_stats'] = '客户流量统计';
$_LANG['delivery_view'] = '查看发货单';
$_LANG['back_view'] = '查看退货单';


//商创微信通
$_LANG['wx_api']         = '微信接口';
$_LANG['wx_menu']        = '微信菜单设置';
$_LANG['wx_config']         = '微信通设置';
$_LANG['wx_autoreg']         = '自动注册设置';
$_LANG['wx_regmsg']         = '关注回复内容';
$_LANG['wx_keywords']         = '关键词自动回复';
$_LANG['wx_fun']         = '功能变量';
$_LANG['wx_prize']         = '抽奖规则';
$_LANG['wx_zjd']         = '砸金蛋';
$_LANG['wx_dzp']         = '大转盘';
$_LANG['wx_order']         = '发货提醒';
$_LANG['wx_pay']         = '付款提醒';
$_LANG['wx_reorder']         = '订单提醒';
$_LANG['wx_fans']         = '粉丝管理';
$_LANG['wx_oauth']         = '微信OAuth';




?>