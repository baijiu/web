<?php

/**
 * 高鑫 管理中心公用文件
 * ============================================================================
 * * 版权所有 2005-2012 ，并保留所有权利。
 * 网站地址: http://www.gxwlr.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: init.php 17217 2011-01-19 06:29:08Z liubo $
*/

if (!defined('IN_ECTOUCH'))
{
    die('Hacking attempt');
}

define('ECS_ADMIN', true);

error_reporting(0);

if (__FILE__ == '')
{
    die('Fatal error code: 0');
}

/* 初始化设置 */
@ini_set('memory_limit',          '64M');
@ini_set('session.cache_expire',  180);
@ini_set('session.use_trans_sid', 0);
@ini_set('session.use_cookies',   1);
@ini_set('session.auto_start',    0);
@ini_set('display_errors',        0);

if (DIRECTORY_SEPARATOR == '\\')
{
    @ini_set('include_path',      '.;' . ROOT_PATH);
}
else
{
    @ini_set('include_path',      '.:' . ROOT_PATH);
}
ob_start();
if (file_exists('../data/config.php'))
{
    include('../data/config.php');
}
else
{
    include('../include/config.php');
}

/* 取得当前ecshop所在的根目录 */
if(!defined('ADMIN_PATH'))
{
    define('ADMIN_PATH','channel');
}
define('ROOT_PATH', str_replace(ADMIN_PATH . '/includes/init.php', '', str_replace('\\', '/', __FILE__)));

if (defined('DEBUG_MODE') == false)
{
    define('DEBUG_MODE', 0);
}

if (PHP_VERSION >= '5.1' && !empty($timezone))
{
    date_default_timezone_set($timezone);
}

if (isset($_SERVER['PHP_SELF']))
{
    define('PHP_SELF', $_SERVER['PHP_SELF']);
}
else
{
    define('PHP_SELF', $_SERVER['SCRIPT_NAME']);
}

define('WEB_PATH',ROOT_PATH);
require(ROOT_PATH . 'include/inc_constant.php');//加载常量
require(ROOT_PATH . 'include/cls_ecshop.php');
require(ROOT_PATH . 'include/cls_error.php');
require(ROOT_PATH . 'include/lib_time.php');//加载时间函数
require(ROOT_PATH . 'include/lib_base.php');//基础函数库
require(ROOT_PATH . 'include/lib_common.php');//公用函数库
require(ROOT_PATH . 'include/lib_main.php');//前台公用函数库
require(ROOT_PATH . ADMIN_PATH . '/includes/lib_main.php');
require(ROOT_PATH . ADMIN_PATH . '/includes/cls_exchange.php');


/* 对用户传入的变量进行转义操作。*/
if (!get_magic_quotes_gpc())
{
    if (!empty($_GET))
    {
        $_GET  = addslashes_deep($_GET);
    }
    if (!empty($_POST))
    {
        $_POST = addslashes_deep($_POST);
    }

    $_COOKIE   = addslashes_deep($_COOKIE);
    $_REQUEST  = addslashes_deep($_REQUEST);
}

/* 对路径进行安全处理 */
if (strpos(PHP_SELF, '.php/') !== false)
{
    ecs_header("Location:" . substr(PHP_SELF, 0, strpos(PHP_SELF, '.php/') + 4) . "\n");
    exit();
}

/* 创建 ECSHOP 对象 */
$ecs = new ECS($db_name, $prefix);
define('DATA_DIR', $ecs->data_dir());
define('IMAGE_DIR', $ecs->image_dir());

/* 初始化数据库类 */
require(ROOT_PATH . 'include/cls_mysql.php');
$db = new cls_mysql($db_host, $db_user, $db_pass, $db_name);
$db_host = $db_user = $db_pass = $db_name = NULL;

/* 创建错误处理对象 */
$err = new ecs_error('message.htm');

/* 初始化session */
require(ROOT_PATH . 'include/cls_session.php');
$sess = new cls_session($db, $ecs->table('sessions'), $ecs->table('sessions_data'), 'ECSCP_ID');

/* 初始化 action */
if (!isset($_REQUEST['act']))
{	
    $_REQUEST['act'] = '';
}
elseif (($_REQUEST['act'] == 'login' || $_REQUEST['act'] == 'logout' || $_REQUEST['act'] == 'signin') &&
    strpos(PHP_SELF, '/privilege.php') === false)
{
    $_REQUEST['act'] = '';
}
elseif (($_REQUEST['act'] == 'forget_pwd' || $_REQUEST['act'] == 'reset_pwd' || $_REQUEST['act'] == 'get_pwd') &&
    strpos(PHP_SELF, '/get_password.php') === false)
{
    $_REQUEST['act'] = '';
}
/* 载入系统参数 */
$_CFG = load_config();

require(ROOT_PATH . 'lang/zh_cn/admin/common.php');

if (file_exists(ROOT_PATH . 'lang/zh_cn/admin/' . basename(PHP_SELF)))
{
    include(ROOT_PATH . 'lang/zh_cn/admin/' . basename(PHP_SELF));
}

if (!file_exists('../data/caches'))
{
    @mkdir('../data/caches', 0777);
    @chmod('../data/caches', 0777);
}

if (!file_exists('../data/compiled/channel'))
{
    @mkdir('../data/compiled/channel', 0777);
    @chmod('../data/compiled/channel', 0777);
}

clearstatcache();


/* 创建 Smarty 对象。*/
require(ROOT_PATH . 'include/cls_template.php');
$smarty = new cls_template;

$smarty->template_dir  = ROOT_PATH . ADMIN_PATH . '/templates';
$smarty->compile_dir   = ROOT_PATH . 'data/compiled/channel';
if ((DEBUG_MODE & 2) == 2)
{
    $smarty->force_compile = true;
}

$smarty->assign('lang', $_LANG);

/* 验证管理员身份 */
if ((!isset($_SESSION['channel_id']) || intval($_SESSION['channel_id']) <= 0) &&
    $_REQUEST['act'] != 'login' && $_REQUEST['act'] != 'signin' &&
    $_REQUEST['act'] != 'forget_pwd' && $_REQUEST['act'] != 'reset_pwd')
{
    /* session 不存在，检查cookie */
    if (!empty($_COOKIE['ECSCP']['channel_id']) && !empty($_COOKIE['ECSCP']['channel_pass']))
    {
        // 找到了cookie, 验证cookie信息
        $sql = 'SELECT user_id, user_name, password ' .
                ' FROM ' .$ecs->table('users') .
                " WHERE is_channel=1 and user_id = '" . intval($_COOKIE['ECSCP']['channel_id']) . "'";
        $row = $db->GetRow($sql);

        if (!$row)
        {
            // 没有找到这个记录
            setcookie($_COOKIE['ECSCP']['channel_id'],   '', 1);
            setcookie($_COOKIE['ECSCP']['channel_pass'], '', 1);

            if (!empty($_REQUEST['is_ajax']))
            {
                make_json_error('对不起,您没有执行此项操作的权限!');
            }
            else
            {
                ecs_header("Location: privilege.php?act=login\n");
            }

            exit;
        }
        else
        {
            // 检查密码是否正确
            if (md5($row['password'] . $_CFG['hash_code']) == $_COOKIE['ECSCP']['channel_pass'])
            {
                set_admin_session($row['user_id'], $row['user_name']);
                // 更新最后登录时间和IP
                $db->query('UPDATE ' . $ecs->table('users') .
                            " SET last_login = '" . gmtime() . "', last_ip = '" . real_ip() . "'" .
                            " WHERE user_id = '" . $_SESSION['channel_id'] . "'");
            }
            else
            {
                setcookie($_COOKIE['ECSCP']['channel_id'],   '', 1);
                setcookie($_COOKIE['ECSCP']['channel_pass'], '', 1);

                if (!empty($_REQUEST['is_ajax']))
                {
                    make_json_error('对不起,您没有执行此项操作的权限!');
                }
                else
                {
                    ecs_header("Location: privilege.php?act=login\n");
                }

                exit;
            }
        }
    }
    else
    {
        if (!empty($_REQUEST['is_ajax']))
        {
            make_json_error('对不起,您没有执行此项操作的权限!');
        }
        else
        {
            ecs_header("Location: privilege.php?act=login\n");
        }

        exit;
    }
}


if ($_REQUEST['act'] != 'login' && $_REQUEST['act'] != 'signin' &&
    $_REQUEST['act'] != 'forget_pwd' && $_REQUEST['act'] != 'reset_pwd')
{
	
	
    $admin_path = preg_replace('/:\d+/', '', $ecs->url()) . ADMIN_PATH;

    if (!empty($_SERVER['HTTP_REFERER']) &&
        strpos(preg_replace('/:\d+/', '', $_SERVER['HTTP_REFERER']), $admin_path) === false)
    {
        if (!empty($_REQUEST['is_ajax']))
        {
            make_json_error('对不起,您没有执行此项操作的权限!');
        }
        else
        {
            ecs_header("Location: privilege.php?act=login\n");
        }

        exit;
    }
	
}

/* 管理员登录后可在任何页面使用 act=phpinfo 显示 phpinfo() 信息 */
if ($_REQUEST['act'] == 'phpinfo' && function_exists('phpinfo'))
{
    phpinfo();

    exit;
}


header('content-type: text/html; charset=' . EC_CHARSET);
header('Expires: Fri, 14 Mar 1980 20:53:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-cache, must-revalidate');
header('Pragma: no-cache');

if ((DEBUG_MODE & 1) == 1)
{
    error_reporting(E_ALL);
}
else
{
    error_reporting(E_ALL ^ E_NOTICE);
}
if ((DEBUG_MODE & 4) == 4)
{
    include(ROOT_PATH . 'include/lib.debug.php');
}


ob_start();

?>
