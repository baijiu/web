<?php

/**
 * GAOXIN 首页文件
 * ============================================================================
 * * 版权所有 2005-2012 ，并保留所有权利。
 * 网站地址: http://www.gxwlr.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: index.php 17217 2011-01-19 06:29:08Z liubo $
*/
//
define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/include/init.php');

require(ROOT_PATH . 'include/lib_weixintong.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

$cat_id = isset($_REQUEST['cat_id'])  ? intval($_REQUEST['cat_id']) : 0;

$floor = $db-> getall("select * from ecs_product_type where enabled = 1 order by cat_id desc");

if($cat_id == 0){

    $row = $db->getrow("select * from ecs_product_type where enabled = 1 order by cat_id desc");
    $title = $row['cat_name'];
    $cat_id = $row['cat_id'];
    $vip_goods = $db->getall("select * from ecs_productinfo where cat_id = '$cat_id' and  is_on_sale = 1  order by id desc");
}else{

    $title =$db->getone("select cat_name from ecs_product_type where cat_id = '$cat_id'");
    $vip_goods = $db->getall("select * from ecs_productinfo where cat_id = '$cat_id' and  is_on_sale = 1 order by id desc");
}



foreach ($vip_goods as $k=>$v){
    if($v['state']==0){
        $vip_goods[$k]['jingdu'] = '进行中';
    }elseif($v['state']==1){
        $vip_goods[$k]['jingdu'] = '已结束';
    }
    //$jd=sprintf("%.2f",$v['yitou_amount']/$v['total_amount']);
	
	$jd=$v['yitou_amount']/$v['total_amount'];
		 $jd=floor($jd*10000)/10000;
    if($jd>1){
        $jd=1;
    }
    $vip_goods[$k]['jd'] =$jd * 100;

}
$smarty->assign('title', $title);
$smarty->assign('vip_goods', $vip_goods);
$smarty->assign('floor', $floor);
$smarty->assign('cat_id', $cat_id);
$smarty->display('touzi.dwt');



?>