<?php
/**
 * GAOXIN 品牌列表
 * ============================================================================
 * * 版权所有 2005-2012 ，并保留所有权利。
 * 网站地址: http://www.gxwlr.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: brand.php 17217 2011-01-19 06:29:08Z liubo $
 */
define('IN_ECTOUCH', true);
require(dirname(__FILE__) . '/include/init.php');

if ((DEBUG_MODE & 2) != 2) {
    $smarty->caching = true;
}

$goods_id = isset($_REQUEST['id'])  ? intval($_REQUEST['id']) : 0;
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'view';
}


if ($_REQUEST['act'] == 'view')
{

    /* 获得商品的信息 */
    $goods = $db->getrow("select * from ecs_productinfo where id = '".$goods_id."'");
    if ($goods === false)
    {
        /* 如果没有找到任何记录则跳回到首页 */
        ecs_header("Location: ./\n");
        exit;
    }
    else
    {
        //$jd=sprintf("%.2f",$goods['yitou_amount']/$goods['total_amount']);
		
		 $jd=$goods['yitou_amount']/$goods['total_amount'];
		 $jd=floor($jd*10000)/10000;
		
        if($jd>1){
            $jd=1;
        }
        $goods['jd'] =$jd * 100;

        $zongshouyi = ($goods['goods_price']*$goods['daily_rate']/100)*$goods['cycle'];
        $goods['zongshouyi'] = $zongshouyi;
        $goods['all'] = $zongshouyi+$goods['goods_price'];
        $smarty->assign('goods',              $goods);
        $smarty->assign('goods_id',           $goods['id']);
        assign_template();
        $smarty->assign('page_title',          $position['title']);                    // 页面标题
        $smarty->assign('ur_here',             $position['ur_here']);                  // 当前位置
        $smarty->display('product.dwt');

    }



}
elseif ($_REQUEST['act'] == 'buy')
{
    /* 查询：判断是否登录 */
    if ($_SESSION['user_id'] <= 0)
    {
        show_message("请先登录再购买！", array("立即登录"), array("user.php"), 'error');
    }
    /* 查询：取得参数：商品id */
    $goods_id = isset($_POST['goods_id']) ? intval($_POST['goods_id']) : 0;
    if ($goods_id <= 0)
    {
        ecs_header("Location: ./\n");
        exit;
    }
    /* 查询商品信息 */
    $goods =$db->getrow("select * from ecs_productinfo where id = '".$goods_id."'");
    if (empty($goods))
    {
        ecs_header("Location: ./\n");
        exit;
    }
    $user_id = $_SESSION['user_id'];
    $userinfo = $db->getrow("select * from ecs_users where user_id = '".$user_id."'");


    //检查项目状态
    if($goods['state']==1){
        show_message("项目已结束！");
    }
	
	if($goods['yitou_amount']>=$goods['total_amount']){
		
		$db->query("update ecs_productinfo set state=1  where id = '".$goods_id."' ");
		
        show_message("项目已结束！");
    }

    if($goods['is_on_sale']==0){
        show_message("项目已下架！");
    }

    $fenshu = intval($_POST['number']);
    if($fenshu<=0){
        show_message("最少投资1份！");
    }


    //检查投资次数限制
    $yigoucishu = $db->getone("select sum(fenshu) from ecs_product_order where user_id = '".$user_id."' and goods_id = '".$goods_id."' and is_pay = 1 ");


    if($yigoucishu+$fenshu>$goods['ketoucishu']){

        $kt = $goods['ketoucishu'] - $yigoucishu;
        show_message("每人限投".$goods['ketoucishu']."份,您可投资".$kt."份");
    }
    //投资金额


    $benjin = $fenshu*$goods['goods_price'];

   //订单号
    do{
        $order_sn = get_order_sn($user_id);
        $oid = $db->getone("select id from ecs_product_order where order_sn = '".$order_sn."'");
    }while($oid);

    $amount = $goods['goods_price'];//本金
    $add_time = time();

    $set = $db->getOne("select jiacheng from ecs_set where id = 1");
    $rate = explode("|",$set);

    if($goods['type']==3){
        $daily_rate =  $goods['daily_rate'];
    }else{
        if($userinfo['user_level']==0){
            $daily_rate =  $goods['daily_rate'];
        }else {
            $daily_rate = $goods['daily_rate'] + (float)$rate[$userinfo['user_level']-1];
        }
    }


    //每日收益
    $earnings = $benjin*$daily_rate/100;
    //总收益
    $gain = ($benjin*$daily_rate/100)*$goods['cycle'];

    //先息后本,每日返回
    if($goods['type']==0){
        $next_time = 24*3600+time();
        $end_time = $goods['cycle']*24*3600+time();
        $sql = "INSERT INTO ecs_product_order (order_sn,validity,limit_num,earnings,gain,is_gain,remaining,user_id, goods_id,fenshu, amount,daifan_amount,type, add_time,is_pay,state,daily_rate,next_time,end_time)".
            "VALUES ('$order_sn','".$goods['cycle']."','".$goods['cycle']."','$earnings','$gain', 0, '$gain','$user_id','$goods_id','$fenshu','$benjin','$benjin','".$goods['type']."','".time()."',0,0,'$daily_rate','$next_time','$end_time') ";

        $ok =$db->query($sql);

        if($ok)
        {
            $smarty->assign('order_sn',  $order_sn);
            $smarty->display('product_flow.dwt');
        }
        else
        {
            show_message("插入订单失败，请重新购买！");
        }
    }
    //等额本息
    if($goods['type']==1){

        $next_time = 24*3600+time();
        $meiqibenjin = sprintf("%.2f",$benjin/$goods['cycle']);
        $end_time = $goods['cycle']*24*3600+time();

        $sql = "INSERT INTO ecs_product_order (order_sn,validity,limit_num,earnings,gain,is_gain,remaining,user_id, goods_id,fenshu,  amount,daifan_amount,meiqibenjin,type, add_time,is_pay,state,daily_rate,next_time,end_time)".
            "VALUES ('$order_sn','".$goods['cycle']."','".$goods['cycle']."','$earnings','$gain', 0, '$gain','$user_id','$goods_id','$fenshu','$benjin','$benjin','$meiqibenjin','".$goods['type']."','".time()."',0,0,'$daily_rate','$next_time','$end_time') ";

        $ok =$db->query($sql);

        if($ok)
        {
            $smarty->assign('order_sn',  $order_sn);
            $smarty->display('product_flow.dwt');
        }
        else
        {
            show_message("插入订单失败，请重新购买！");
        }
    }

    //到期返本反息
    if($goods['type']==2){

        $next_time = $goods['cycle']*24*3600+time();
        $end_time = $next_time;
        $sql = "INSERT INTO ecs_product_order (order_sn,validity,limit_num,earnings,gain,is_gain,remaining,user_id, goods_id,fenshu,  amount,daifan_amount,type, add_time,is_pay,state,daily_rate,next_time,end_time)".
            "VALUES ('$order_sn','".$goods['cycle']."','1','$earnings','$gain', 0, '$gain','$user_id','$goods_id','$fenshu','$benjin','$benjin','".$goods['type']."','".time()."',0,0,'$daily_rate','$next_time','$end_time') ";

        $ok =$db->query($sql);

        if($ok)
        {
            $smarty->assign('order_sn',  $order_sn);
            $smarty->display('product_flow.dwt');
        }
        else
        {
            show_message("插入订单失败，请重新购买！");
        }
    }

    //先息后本,每小时返回
    if($goods['type']==3){
        $next_time = 3600+time();
        $end_time = $goods['cycle']*3600+time();
        $sql = "INSERT INTO ecs_product_order (order_sn,validity,limit_num,earnings,gain,is_gain,remaining,user_id, goods_id,fenshu,  amount,daifan_amount,type, add_time,is_pay,state,daily_rate,next_time,end_time)".
            "VALUES ('$order_sn','".$goods['cycle']."','".$goods['cycle']."','$earnings','$gain', 0, '$gain','$user_id','$goods_id','$fenshu','$benjin','$benjin','".$goods['type']."','".time()."',0,0,'$daily_rate','$next_time','$end_time') ";

        $ok =$db->query($sql);

        if($ok)
        {
            $smarty->assign('order_sn',  $order_sn);
            $smarty->display('product_flow.dwt');
        }
        else
        {
            show_message("插入订单失败，请重新购买！");
        }
    }
}

elseif ($_REQUEST['act'] == 'pay_balance')
{

    /* 查询：判断是否登录 */
    if ($_SESSION['user_id'] <= 0)
    {
        show_message("请先登录再购买！", array("立即登录"), array("user.php"), 'error');
    }
    $order_sn = $_GET['order_sn'];

    $order = $db->getrow("select * from ecs_product_order where user_id = '".$_SESSION['user_id']."' and order_sn = '".$order_sn."' and is_pay = 0");
    if (empty($order))
    {
        show_message("订单不存在或已支付！");
    }

    $goods =$db->getrow("select * from ecs_productinfo where id = '".$order['goods_id']."'");
    //检查项目状态
    if($goods['state']==1){
        show_message("项目已结束！");
    }
	
	if($goods['yitou_amount']>=$goods['total_amount']){
		
		$db->query("update ecs_productinfo set state=1  where id = '".$order['goods_id']."' ");
		
        show_message("项目已结束！");
    }



    //检查投资次数限制
    $yigoucishu = $db->getone("select sum(fenshu) from ecs_product_order where user_id = '".$_SESSION['user_id']."' and goods_id = '".$order['goods_id']."' and is_pay = 1 ");


    if($yigoucishu+$order['fenshu']>$goods['ketoucishu']){

        $kt = $goods['ketoucishu'] - $yigoucishu;
        show_message("每人限投".$goods['ketoucishu']."份,您可投资".$kt."份!");
    }





    $user_info = $db->getrow("select * from ecs_users where user_id = '".$_SESSION['user_id']."'");
    if($user_info['user_money']<$order['amount']){
        show_message("余额不足,请充值！");
    }
    $goods_name = $db->getone("select goods_name from ecs_productinfo where id = '".$order['goods_id']."'");





    $chack = $db->query("update ecs_product_order set is_pay=1 , pay_type=1 ,pay_time ='".time()."'   where order_sn = '".$order_sn."' and user_id = '".$_SESSION['user_id']."'");
    if($chack){

        $db->query("update ecs_productinfo set yitou_amount=yitou_amount + '$order[amount]'  where id = '".$order['goods_id']."' ");

        if($order['amount']+$goods['yitou_amount']>=$goods['total_amount']){
            $db->query("update ecs_productinfo set state=1  where id = '".$order['goods_id']."' ");
        }

        //有效用户
        if($user_info['is_youxiao']==0){
            $db->query("update ecs_users set is_youxiao = 1 where user_id = '".$_SESSION['user_id']."'");
        }

        log_account_change($_SESSION['user_id'], -1*$order['amount'], 0, 0, 0, "投资【".$goods_name."】",54);

        //赠送积分
        $points_rate = $db->getOne("select points_rate from ecs_set where id = 1");
        $give_points = $order['amount']*((float)$points_rate/100);
        log_account_change($_SESSION['user_id'], 0, 0, 0, $give_points, "投资【".$goods_name."】赠送",54);
        //升级
        //shengji($_SESSION['user_id'],$order['amount']);
        //分销
        sanji($order['id'],$_SESSION['user_id']);//动态收益
        show_message("购买成功", "会员中心", 'user.php',
            'info');
    }else{
        show_message("购买失败！");
    }
}


function get_order_sn($user_id)
{
    /* 选择一个随机的方案 */
    mt_srand((double) microtime() * 1000000);

    return date('Ymd') .$user_id. str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
}

function sanji($oid,$uid){
    $db = $GLOBALS['db'];
    $set = $db->getOne("select fenxiao_money from ecs_set where id = 1");
    $rate = explode("|",$set);

    $num = count($rate);
    //订单信息表 商品价格
    $order = $db->getRow("select * from ecs_product_order where id = $oid");

    $buy = $db->getone("select user_name from ecs_users where user_id = '".$uid."'");
    for($i=0;$i<$num;$i++){
        $pid = $db->getOne("select parent_id from ecs_users where user_id = '".$uid."'");
        if($pid){
            $uid = $pid;
            $money = $order['amount']*((float)$rate[$i]/100);
            if($money>0)
            {
               // $is_youxiao = $db->getOne("select is_youxiao from ecs_users where user_id = '".$pid."'");
               
                    log_account_change_new($pid, 0, 0, 0, 0,$money, "下级用户【".$buy."】投资".$order['amount']."元推荐奖（ 订单 ".$order['order_sn']."）",55);
              

            }
        }
    }
}
/*function shengji($user_id,$money){
    $db = $GLOBALS['db'];
    $set = $set = $db->getOne("select shengji from ecs_set where id = 1");
    $shengji = explode("|",$set);
    $zunxiang = explode(",",$shengji[0]);
    $zhizun = explode(",",$shengji[1]);
    $user_info = $db->getrow("select * from ecs_users where user_id = '".$user_id."'");
    $leiji = $db->getone("select sum(amount) from ecs_product_order where user_id = '".$user_id."'  and is_pay = 1  ");
    $leiji = $leiji + $money;
    if($user_info['user_level'] == 0){
        if(($money>=$zunxiang[0]&&$money<$zhizun[0])||($leiji>=$zunxiang[1]&&$leiji<$zhizun[1])){
            $db->query("update ecs_users set user_level = 1 where user_id = '".$user_id."'");
        }

        if(($money>=$zhizun[0])||($leiji>=$zhizun[1])){
            $db->query("update ecs_users set user_level = 2 where user_id = '".$user_id."'");
        }
    }elseif($user_info['user_level'] == 1){
        if($money>=$zhizun[0]||$leiji>=$zhizun[1]){
            $db->query("update ecs_users set user_level = 2 where user_id = '".$user_id."'");
        }
    }
}
*/



?>