<?php
/* 
*甜心  100分销微分销分销中心
*/
define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/include/init.php');

$user_id = $_SESSION['user_id'];
if(empty($_SESSION['user_id'])){
    ecs_header("Location: user.php\n");
    exit;
}
$team_all = team_all($user_id);
$smarty->assign('team_all',     $team_all);
$smarty->display('teamlist.dwt');



function team_all($auid){
    $db =  $GLOBALS['db'];
    $user_list = array();
    $auid = $auid;
    $up_uid = "'$auid'";
    $all_count = 0;
    $i = 1;
    $user_levels = array('普通会员','白银会员','黄金会员','钻石会员','皇冠会员','名誉股东');
    $team_rank =  array('普通团长','一级团长','二级团长','三级团长','四级团长','五级团长');
    do{
        $count = 0;
        $sql = "SELECT user_id FROM ecs_users WHERE parent_id IN($up_uid)";
        $query = $db->query($sql);
        $up_uid = '';
        while ($rt = $db->fetch_array($query))
        {

            $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
            $count++;
        }
        $all_count += $count;
        if ($count)
        {
            $sql = "SELECT user_id, user_name, '$i' AS level,  user_money, user_level,team_level,  pay_points,head_img, reg_time,wxid ,parent_id".
                " FROM " . $GLOBALS['ecs']->table('users') . " WHERE user_id IN($up_uid) " .
                " ORDER by level, user_id";
            $user_info=$db->getAll($sql);
            foreach($user_info as $k=>$v){


                $v['rank_name']= $user_levels[$v['user_level']];
                $v['reg_time']=local_date($GLOBALS['_CFG']['date_format'], $v['reg_time']);
                $v['all_touzi']= $db->getone("select sum(amount) from ecs_product_order where user_id = '".$v['user_id']."' and is_pay = 1");
                if(empty( $v['all_touzi'])){
                    $v['all_touzi'] = 0.00;
                }

                $v['zongchongzhi']= $db->getone("select sum(amount) from ecs_recharge where user_id = '".$v['user_id']."' and paystate = 1");
                if(empty( $v['zongchongzhi'])){
                    $v['zongchongzhi'] = 0.00;
                }
                $all_uid = all_uid($v['user_id']);
                $v['zongyeji']= $db->getone("select sum(amount) from ecs_product_order where user_id in($all_uid) and is_pay = 1");
                if(empty( $v['zongyeji'])){
                    $v['zongyeji'] = 0.00;
                }

                $v['teamname'] = $team_rank[$v['team_level']];
                $user_list[] = $v;
            }
        }
        $i++;
    }while($up_uid);

    return $user_list;
}

function all_uid($auid){
    $db =  $GLOBALS['db'];

    $up_uid = "'$auid'";
    $all_uid = "'$auid'";

    do{

        $sql = "SELECT user_id FROM ecs_users WHERE parent_id IN($up_uid)";
        $query = $db->query($sql);
        $up_uid = '';
        while ($rt = $db->fetch_array($query))
        {
            $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
            $all_uid .= $all_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
        }



    }while($up_uid);

    return $all_uid;
}



?>