<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta charset="utf-8" />
<title><?php echo $this->_var['page_title']; ?> </title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>

<body>
<style>

#content img {
    width: 100% !important;
    height: auto;
    display: block;
}
.gray{
color:#808A87;
font-size: 11px;
}
.red{
color:red;
}
.title{
font-family:微软雅黑;
height:40px;
width:100%;
padding:5px 0;
}
.good_tap{
line-height:24px;
min-width:24%;
height:36px;
float:left;
padding:10px 10px;
font-family:微软雅黑;
font-size:14px;
text-align:center;
}
.goods_div{
background-color: #fff;padding: 0 0.7rem;margin:10px 0px;
}
.goods_li{
width:100%;min-height:20px;border-bottom:1px #cccccc solid;padding:6px 0;font-size:12px;font-family:微软雅黑;overflow:hidden;
}
.li_left{
float:left;
width:auto;
margin-right:20px;
}
.li_right{
float:left;
width:75%;
height:auto;
}
</style>
<header id="header">
  <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1);"> 返回 </a> </div>
  <h1> 项目详情 </h1>
 
</header>

 
<script src="<?php echo $this->_var['ectouch_themes']; ?>/js/TouchSlide.js"></script>
<section class="goods_slider">
  <div class="blank2"></div>
  <div id="slideBox" class="slideBox">
    <div class="scroller" style="    max-width: 100%;"> 
      <div   style="    position: relative;"><a href="javascript:showPic()">
      <?php if ($this->_var['goods']['state'] == 1): ?>
                <img src="/images/succes.png" style="width: 100px; top:50px;left: 50px;position: absolute;z-index: 99;" class="succes">
                <?php endif; ?>
        <img src="<?php echo $this->_var['site_url']; ?><?php echo $this->_var['goods']['file_url']; ?>"  alt="<?php echo $this->_var['goods']['goods_name']; ?>" /></a></div>
    </div>
  </div>
  <div class="blank2"></div>
</section>


<section class="goodsInfo">
  <div class="title">
       <h2 style="float:left;"> <?php echo $this->_var['goods']['goods_name']; ?> </h2>
     
  </div>
  <div class="title" style="height:6px;">
            <div class="layui-progress layui-progress">
                       <div class="layui-progress-bar layui-bg-red" lay-percent="<?php echo $this->_var['goods']['jd']; ?>%"></div>
             </div>
  </div>
  <div class="title"  style="padding-top:0;padding-bottom:10px;">
     <span class="gray">
        项目进度：<span class="red"><?php echo $this->_var['goods']['jd']; ?></span> %
      </span>
      
        <span class="gray"  style="  float:right;">
        <?php if ($this->_var['goods']['type'] == 3): ?>时<?php else: ?>日<?php endif; ?>利率：<span class="red"><?php echo $this->_var['goods']['daily_rate']; ?></span> %
      </span>
  </div>
</section>

<section class="goods_div" style="height:66px;">

    <div class="good_tap">

        <p class="gray">期限</p>
        <p class="red"><?php echo $this->_var['goods']['cycle']; ?>个<?php if ($this->_var['goods']['type'] == 3): ?>小时 <?php else: ?>自然日<?php endif; ?></p>


    </div>
    <div style="width:1px;height:20px;float:left;border-right:1px #cccccc solid;margin-top:23px;"></div>
    <div class="good_tap">
            <p class="gray">起购</p>
            <p class="red"><?php echo $this->_var['goods']['goods_price']; ?></p>
    </div>
    <div style="width:1px;height:20px;float:left;border-right:1px #cccccc solid;margin-top:23px;"></div>
    <div class="good_tap">
            <p class="gray">项目规模</p>
            <p class="red"><?php echo $this->_var['goods']['total_amount']; ?></p>
    </div>
</section>

    <div class="goods_div">
        <div class="goods_li">
              <div class="li_left">还款方式</div>

              <div class="li_right"><?php if ($this->_var['goods']['type'] == 0): ?>
                      每日返息，到期返本
                  <?php elseif ($this->_var['goods']['type'] == 1): ?>

                      等额本息，每日返还
                  <?php elseif ($this->_var['goods']['type'] == 2): ?>
                      到期返本返息

                  <?php elseif ($this->_var['goods']['type'] == 3): ?>
                      时时还息，到期还本
                  <?php endif; ?></div>
        </div>
        <div class="goods_li">
                      <div class="li_left">担保机构</div>
                      <div class="li_right">香港环球国际投资担保有限公司</div>
         </div>
        <div class="goods_li" style="border:none;">
              <div class="li_left">安全保障</div>
              <div class="li_right"><img src="<?php echo $this->_var['ectouch_themes']; ?>/images/bao.png" width="24px;"></div>
        </div>
    </div>

     <div class="goods_div">
            <h1 style="color:#6A5ACD;text-align:center;font-weight:600;padding:10px 0;font-size:18px;">产品详情</h1>
            <div class="goods_li">
                  <div class="li_left"><span class="gray">投资项目</span></div>
                  <div class="li_right"><?php echo $this->_var['goods']['goods_name']; ?></div>
            </div>
            <div class="goods_li">
                          <div class="li_left"><span class="gray">项目金额</span></div>
                          <div class="li_right"><span class="red"><?php echo $this->_var['goods']['total_amount']; ?>元人民币</span></div>
             </div>
            <div class="goods_li">
                  <div class="li_left"><span class="gray"> <?php if ($this->_var['goods']['type'] == 3): ?>时<?php else: ?>日<?php endif; ?>收益率</span></div>
                  <div class="li_right"><span class="red"><?php echo $this->_var['goods']['daily_rate']; ?>%</span> <?php if ($this->_var['goods']['type'] == 3): ?>时<?php else: ?>日<?php endif; ?>化收益</div>
            </div>
            <div class="goods_li" >
                  <div class="li_left"><span class="gray">起投金额</span></div>
                  <div class="li_right"><span class="red"><?php echo $this->_var['goods']['goods_price']; ?>元起投</span></div>
            </div>
                        <div class="goods_li">
                              <div class="li_left"><span class="gray">项目期限</span></div>
                              <div class="li_right"><span class="red"><?php echo $this->_var['goods']['cycle']; ?></span>个<?php if ($this->_var['goods']['type'] == 3): ?>小时 <?php else: ?>自然日<?php endif; ?></div>
                        </div>
            <div class="goods_li" >
                  <div class="li_left"><span class="gray">收益计算</span></div>
                  <div class="li_right"><span class="red"><?php echo $this->_var['goods']['goods_price']; ?>元*<?php echo $this->_var['goods']['daily_rate']; ?>%*<?php echo $this->_var['goods']['cycle']; ?></span>=总收益<span class="red"><?php echo $this->_var['goods']['zongshouyi']; ?>元+本金<span class="red"><?php echo $this->_var['goods']['goods_price']; ?>元</span>=总计本息<span class="red"><?php echo $this->_var['goods']['all']; ?>元；</span></div>
            </div>

            <div class="goods_li">
                  <div class="li_left"><span class="gray">还款方式</span></div>
                  <div class="li_right">
                      <?php if ($this->_var['goods']['type'] == 0): ?>
                          每日返息，到期返本
                      <?php elseif ($this->_var['goods']['type'] == 1): ?>

                          等额本息，每日返还
                      <?php elseif ($this->_var['goods']['type'] == 2): ?>
                          到期返本返息
                      <?php elseif ($this->_var['goods']['type'] == 3): ?>
                          时时还息，到期还本
                      <?php endif; ?>

                  </div>
            </div>

                        <div class="goods_li">
                              <div class="li_left"><span class="gray">可投金额</span></div>
                              <div class="li_right">投资期间只要产品未投满，投资者限投<?php echo $this->_var['goods']['ketoucishu']; ?>份.</div>
                        </div>

                                                <div class="goods_li">
                                                      <div class="li_left"><span class="gray">安全保障</span></div>
                                                      <div class="li_right">担保机构对平台上的每-笔投资提供100%本息保障,平台设立风险备用金，对本息承诺全额垫付;</div>
                                                </div>
         <div class="goods_li">
             <div class="li_left"><span class="gray">上市时间</span></div>
             <div class="li_right"><?php echo $this->_var['goods']['shangyingtime']; ?></div>
         </div>

                        <div class="goods_li" style="border:none;">
                              <div class="li_left"><span class="gray">产品简介</span></div>
                              <div class="li_right" id="content">
                                  <?php echo $this->_var['goods']['content']; ?>
                                </div>
                        </div>
        </div>




<div class="wrap" style="margin-bottom: 70px">
  <section class="goodsBuy radius5">
    <form action="product.php?act=buy" method="post" name="ECS_FORMBUY" id="ECS_FORMBUY" >
      <div class="fields" style=" display:block">
        <ul class="ul1">
          <li style="width: 100%;">请输入投资份数</li>
        </ul>

          <ul class="quantity" style="padding:0 0">
              <span style="color:#999">投资份数：</span>
              <div class="items" style="display:inline-block">
                  <span class="ui-number radius5">


            <input style="width:6.8rem;" class="num" name="number" id="goods_number" autocomplete="off" value="1"   type="number" />
                  </span>
              </div>
          </ul>


      </div>
      <div class="option" >
       <input type="hidden" name="goods_id" value="<?php echo $this->_var['goods']['id']; ?>" />

      </div>
    </form>
  </section>
</div>


<section  onclick="buy();" id="s-action" class="s-action float" style="padding-right: 0px;Z-index:9999">
    <button class="buy" type="button">立刻购买</button>
</section>
<script>

    function buy(){
        document.getElementById("ECS_FORMBUY").submit();
    }
</script>

<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/layui/layui.js"></script>
<script>


layui.use('element', function () {
    var element = layui.element;
});

</script>
</body>
</html>