<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8" />
<title><?php echo $this->_var['title']; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css?v=2014" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/TouchSlide.js"></script>
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/layui/layui.js"></script>

    <script>
        //注意进度条依赖 element 模块，否则无法进行正常渲染和功能性操作
        layui.use('element', function(){
            var element = layui.element;
        });
    </script>
    
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
<style>
.user_top_goods {
height: 5rem;
overflow: hidden; 
background:#ffbf6b;
position:relative

}
.user_top_goods dt {
float: left;
margin: 0.8rem 0.8rem 0;
text-align: center;
position: relative;
width: 3.7rem;
height: 3.7rem;
border-radius: 3.7rem;
padding:0.15rem; background:#FFFFFF
}
.user_top_goods dt img {
width: 3.7rem;
height:3.7rem;
border-radius: 3.7rem;
}
.guanzhu {
background-color: #ffbf6b;
}

.guanzhu {
color: #fff;
border: 0;
height: 2.5rem;
line-height: 2.5rem;
width: 100%;
-webkit-box-flex: 1;
display: block;
-webkit-user-select: none;
font-size: 0.9rem;
}
#cover2 {
    background-color: #333333;
    display: none;
    left: 0;
    opacity: 0.8;
    position: absolute;
    top: 0;
    z-index: 1000;
}
#share_weixin, #share_qq {
    right: 10px;
    top: 2px;
    width: 260px;
}
#share_weixin, #share_qq, #share_qr {
    display: none;
    position: fixed;
    z-index: 3000;
}
#share_weixin img, #share_qq img {
    height: 165px;
    width: 260px;
}

		.button_3 {
    background-color: #EEEEEE;
    border: 1px solid #666666;
    color: #666666;
    font-size: 16px;
    line-height: 20px;
    padding: 10px 0;
    text-align: center;
}
#share_weixin button, #share_qq button {
    margin-top: 25px;
    width: 100%;
}

.bd li {
    text-align: center;
}

 .bd li img {
	 width: 100%;
     
 }
 .content{
	margin: 0px auto;
	padding-top: 5px;
    width: 95%;
    height: 35px;
	}

.focus .bd li img {
    width: 97%;

    border-radius: 2%;
}
.cur span{border-bottom: 2px solid #FF7B16;}

</style>

</head>
<body style=" background: #f5f3f3;">

<header id="header">
    <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1);"> 返回 </a> </div>
    <h1> <?php echo $this->_var['title']; ?> </h1>

</header>



<div style=" width:auto; background:#FFF;height:40px; font-size:15px; overflow-x:auto; white-space:nowrap; text-align:center; line-height:40px;">

    <!--<a href="touzi.php"  style=" font-size:15px; padding-left:25px;" <?php if ($this->_var['cat_id'] == 0): ?>class="cur"<?php endif; ?> > <span>全部项目</span></a>-->

    <?php $_from = $this->_var['floor']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'cat');$this->_foreach['name'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['name']['total'] > 0):
    foreach ($_from AS $this->_var['cat']):
        $this->_foreach['name']['iteration']++;
?>
    <a href="touzi.php?cat_id=<?php echo $this->_var['cat']['cat_id']; ?>"   style=" font-size:15px; padding-left:25px;<?php if (($this->_foreach['name']['iteration'] == $this->_foreach['name']['total'])): ?>padding-right:25px; <?php endif; ?>" <?php if ($this->_var['cat']['cat_id'] == $this->_var['cat_id']): ?>class="cur"<?php endif; ?> ><span> <?php echo htmlspecialchars($this->_var['cat']['cat_name']); ?></span></a>
    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?></div>


<style>
    .best_cont {padding:0 10px; background: #fff;margin-top:10px;}
    .best_cont img {width:100%;}
    .best_cont header a {height:48px;line-height:48px; display:block; position:relative;padding-left:15px; font-weight:700;font-size:14px;     color: #ed6a85;}
    .best_cont header a i {background:url('<?php echo $this->_var['ectouch_themes']; ?>/images/sjsg.png') no-repeat; background-size:100%;width:8px;height:12px; display:inline-table;position:absolute;top:20px;left:0;}
    .best_ul li {margin-bottom:15px;    padding-bottom: 15px;}
    .price_box {color:#fc6655;font-size:1.4rem !important;}
    .best_ul li .title_box {line-height:36px;height:26px;font-size:14px;font-weight:400;font-family:微软雅黑 arial}
    .price_box del {color:#666;font-size:12px;padding-left:8px;}
    .best_ul li .price_box {
        float: right;
    }
    .yellow{
        color:#FF9912;

    }
    .title_box span{
        font-size:0.80rem !important;
    }
</style>
<div class="best_cont">


    <div class="best_ul">
        <ul>
            <?php $_from = $this->_var['vip_goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'goods');$this->_foreach['vip_goods'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['vip_goods']['total'] > 0):
    foreach ($_from AS $this->_var['goods']):
        $this->_foreach['vip_goods']['iteration']++;
?>
            <li   style="    position: relative;">
                <a href="product.php?id=<?php echo $this->_var['goods']['id']; ?>">
                 <?php if ($this->_var['goods']['state'] == 1): ?>
                <img src="/images/succes.png" style="width: 100px; top:50px;left: 50px;position: absolute;z-index: 99;" class="succes">
                <?php endif; ?>
                    <img draggable="false" src="<?php echo $this->_var['site_url']; ?><?php echo $this->_var['goods']['file_url']; ?>">
                    <h3 style="font-family:微软雅黑;text-align:center;font-size:1.5em"><?php echo $this->_var['goods']['goods_name']; ?></h3>

                    <div class="title_box" style=""><span style="";>项目进展：<?php echo $this->_var['goods']['jingdu']; ?></span>  <span  class="price_box" style="width:40%;text-algin:right;font-size:0.80rem !important;"> <span style="color:black;">推荐指数:</span>
                            <?php if ($this->_var['goods']['tuijianzhishu'] == 5): ?>
                                ★★★★★
                            <?php elseif ($this->_var['goods']['tuijianzhishu'] == 4): ?>
                                ★★★★
                            <?php elseif ($this->_var['goods']['tuijianzhishu'] == 3): ?>
                                ★★★
                            <?php elseif ($this->_var['goods']['tuijianzhishu'] == 2): ?>
                                ★★
                            <?php elseif ($this->_var['goods']['tuijianzhishu'] == 1): ?>
                                ★
                            <?php endif; ?>
                        </span> </div>
                    <div class="title_box">
                              <span style="width:100%">还款方式：<span class="yellow">

                                      <?php if ($this->_var['goods']['type'] == 0): ?>
                                          每日返息，到期返本
                                      <?php elseif ($this->_var['goods']['type'] == 1): ?>

                                          等额本息，每日返还
                                      <?php elseif ($this->_var['goods']['type'] == 2): ?>
                                          到期返本返息
                                      <?php elseif ($this->_var['goods']['type'] == 3): ?>
                                          时时还息，到期还本

                                      <?php endif; ?>

                                  </span></span>
                    </div>
                    <div class="title_box">
                        <span style="">起投金额：<span class="yellow"><?php echo $this->_var['goods']['goods_price']; ?></span></span> <span style="margin-left:20px;"><?php if ($this->_var['goods']['type'] == 3): ?>时<?php else: ?>日<?php endif; ?>化收益：<span class="yellow"><?php echo $this->_var['goods']['daily_rate']; ?>%</span></span>
                    </div>
                    <div class="title_box">
                        <span style="">项目规模：<span class="yellow"><?php echo $this->_var['goods']['total_amount']; ?></span></span> <span style="margin-left:20px;">项目周期：<span class="yellow"><?php echo $this->_var['goods']['cycle']; ?>个<?php if ($this->_var['goods']['type'] == 3): ?>小时 <?php else: ?>自然日<?php endif; ?></span></span>
                    </div>
                    <div class="title_box">
                            <span style="width:23%;float:left">
                            项目进度：
                            </span>
                        <span style="width:77%;float:left;padding-top: 9px;">
                                 <div class="layui-progress layui-progress-big" lay-showpercent="true">
                                     <div class="layui-progress-bar layui-bg-red" lay-percent="<?php echo $this->_var['goods']['jd']; ?>%"></div>
                                  </div>
                           </span>
                    </div>

                </a>

            </li>

            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
        </ul>
    </div>
</div>





<?php echo $this->fetch('library/page_footer.lbi'); ?> 


<style>
 .contact-public{
    position:fixed;
    left:0px;
    width:30px;
    padding:0;
    line-height:15px;
    border-radius:5px;
    background:rgba(0,0,0,0.9);
    bottom:70px;
    text-align:center;
    color:#fff;
    z-index:2;
 }
.contact-public li{
   display:block;
   border-bottom:1px solid #7D7D7D;
   text-align:center;
               -webkit-box-sizing:border-box;
                  -moz-box-sizing:border-box;
                     -o-box-sizing:border-box;   
                        box-sizing:border-box;
}
.contact-public li:last-child{
   border-bottom:0;
   border-top:1px solid #242424;


}
.contact-public a{
   display:block;
   color:#fff;
   font-size:16px;
   text-align:center;
   padding:6px;
}
.contact-public .icon-tel{
   display:block;
   margin:0px auto 0 auto;
   width:25px;
   height:20px;
   background:url("http://wfx.91dcw.com/weixin/images/distribution2.png") -200px -820px;
   background-size:300px 1000px;
}

</style>



<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.mmenu.js"></script>
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/ectouch.js"></script>
</body>
</html>