<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8" />
<title><?php if ($this->_var['name']): ?><?php echo $this->_var['name']; ?>的<?php endif; ?><?php echo $this->_var['shop_name']; ?> </title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css?v=2014" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/TouchSlide.js"></script>
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/layui/layui.js"></script>

    <script>
        //注意进度条依赖 element 模块，否则无法进行正常渲染和功能性操作
        layui.use('element', function(){
            var element = layui.element;
        });
    </script>
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
    
<style>
.user_top_goods {
height: 5rem;
overflow: hidden; 
background:#ffbf6b;
position:relative

}
.user_top_goods dt {
float: left;
margin: 0.8rem 0.8rem 0;
text-align: center;
position: relative;
width: 3.7rem;
height: 3.7rem;
border-radius: 3.7rem;
padding:0.15rem; background:#FFFFFF
}
.user_top_goods dt img {
width: 3.7rem;
height:3.7rem;
border-radius: 3.7rem;
}
.guanzhu {
background-color: #ffbf6b;
}

.guanzhu {
color: #fff;
border: 0;
height: 2.5rem;
line-height: 2.5rem;
width: 100%;
-webkit-box-flex: 1;
display: block;
-webkit-user-select: none;
font-size: 0.9rem;
}
#cover2 {
    background-color: #333333;
    display: none;
    left: 0;
    opacity: 0.8;
    position: absolute;
    top: 0;
    z-index: 1000;
}
#share_weixin, #share_qq {
    right: 10px;
    top: 2px;
    width: 260px;
}
#share_weixin, #share_qq, #share_qr {
    display: none;
    position: fixed;
    z-index: 3000;
}
#share_weixin img, #share_qq img {
    height: 165px;
    width: 260px;
}

		.button_3 {
    background-color: #EEEEEE;
    border: 1px solid #666666;
    color: #666666;
    font-size: 16px;
    line-height: 20px;
    padding: 10px 0;
    text-align: center;
}
#share_weixin button, #share_qq button {
    margin-top: 25px;
    width: 100%;
}

.bd li {
    text-align: center;
}

 .bd li img {
	 width: 100%;
     
 }
 .content{
	margin: 0px auto;
	padding-top: 5px;
    width: 95%;
    height: 35px;
	}

.focus .bd li img {
    width: 97%;

    border-radius: 2%;
}

</style>

</head>
<body style="    background: #f5f3f3;">

  
	<header class="region" >

            <div class="logo" style="    padding: 0 5px;">
                <a href="index.php" title=""><img src="<?php echo $this->_var['ectouch_themes']; ?>/images/logo.png"  style="    margin: 5px 5px;
    height: 30px;" ></a>
                <?php if ($this->_var['userid'] > 0): ?>
                <a  title="" style="float: right; margin-top: 10px; margin-right: 8px; color: blue" id="sign_in">+签到</a>
                <?php else: ?>
                    <a  href="user.php" title="" style="float: right; margin-top: 10px; margin-right: 8px; color: blue">+签到</a>
                <?php endif; ?>

            </div>

  	</header>



  <script>
      //签到
      layui.use(['layer'], function(){
          var btn;
          var layer = layui.layer;
          btn = document.querySelector('#sign_in');
          btn.addEventListener('click', function(){

              if(<?php echo $this->_var['is_sign']; ?>==1){
                  layer.msg('您今天已签到过了！');
                  return false;
              }
          else{
                  $.ajax({
                      url: 'user.php?act=sign_in',
                      type: 'post',
                      data: {
                          qq: 'qq',
                      },
                      success: function(res){
                          res = eval("("+res+")");
                          layer.msg(res.msg);
                      }
                  });
              }
          });
      });

      </script>
    

<div id="focus" class="focus region">
  <div class="hd">
    <ul>
    </ul>
  </div>
  <div class="bd">
    
<?php $this->assign('ads_id','1'); ?><?php $this->assign('ads_num','10'); ?><?php echo $this->fetch('library/ad_position.lbi'); ?>

  </div>
</div>
<script type="text/javascript">
TouchSlide({ 
	slideCell:"#focus",
	titCell:".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
	mainCell:".bd ul", 
	effect:"leftLoop", 
	autoPlay:true,//自动播放
	autoPage:true //自动分页
});
</script>



<div class="blank2"> </div>


<div id=content class="wrap">
  
  <div class="region row row_category">
    <ul class="flex flex-f-row">
    
	  <?php $_from = $this->_var['navigator_list']['middle']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'nav');$this->_foreach['nav_middle_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['nav_middle_list']['total'] > 0):
    foreach ($_from AS $this->_var['nav']):
        $this->_foreach['nav_middle_list']['iteration']++;
?>
      <li class="flex_in"> <a href="<?php echo $this->_var['nav']['url']; ?>"<?php if ($this->_var['nav']['opennew'] == 1): ?> target="_blank"<?php endif; ?> title="<?php echo $this->_var['nav']['name']; ?>"> <img src="<?php echo $this->_var['nav']['pic']; ?>" /> </a>
        <p> <?php echo $this->_var['nav']['name']; ?> </p>
      </li>
      <?php if ($this->_foreach['nav_middle_list']['iteration'] % 4 == 0 && $this->_foreach['nav_middle_list']['iteration'] != $this->_foreach['nav_middle_list']['total']): ?>
      </ul><ul class="flex flex-f-row">
      <?php endif; ?>
      <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
    </ul>
  </div>
  





<?php echo $this->fetch('library/recommend_best.lbi'); ?>

</div>







<style>
	.best_cont {padding:0 10px; background: #fff;margin-top:10px;}
	.best_ul img {width:100%;}
	.best_cont header a {height:48px;line-height:48px; position:relative;padding-left:15px; font-weight:700;font-size:14px;     color: #ed6a85;}
	.best_cont header a i {background:url('<?php echo $this->_var['ectouch_themes']; ?>/images/sjsg.png') no-repeat; background-size:100%;width:8px;height:12px; display:inline-table;position:absolute;top:20px;left:0;}
	.best_ul li {margin-bottom:15px;    padding-bottom: 15px;}
	.price_box {color:#fc6655;font-size:1.4rem !important;}
	.best_ul li .title_box {line-height:36px;height:26px;font-size:14px;font-weight:400;font-family:微软雅黑 arial}
	.price_box del {color:#666;font-size:12px;padding-left:8px;}
    .best_ul li .price_box {
        float: right;
    }
    .yellow{
        color:#FF9912;

    }
    .title_box span{
        font-size:0.80rem !important;
    }
</style>



<div class="best_cont">

<header>
    	<a href="#" class="more">热门项目</a>

    </header>


        	<div id="J_ItemList">


        		<ul class="best_ul single_item infos">
        		</ul>

        		<a href="javascript:;" class="get_more"> </a>
        	</div>
    </div>








<?php echo $this->fetch('library/page_footer.lbi'); ?> 


<div id="main-search" class="main-search">
    <div class="hd"> <span class="ico_08 close"> 关闭 </span> </div>
    <div class="bd">
      <div class="search_box">
        <form action="search.php" method="post" id="searchForm" name="searchForm">
          <div class="content">
            <input class="text" type="search" name="keywords" id="keywordBox" autofocus />
            <button class="ico_07" type="submit" value="搜 索" onclick="return check('keywordBox')"></button>
          </div>
        </form>
      </div>
    </div>
</div>

<style>
 .contact-public{
    position:fixed;
    left:0px;
    width:30px;
    padding:0;
    line-height:15px;
    border-radius:5px;
    background:rgba(0,0,0,0.9);
    bottom:70px;
    text-align:center;
    color:#fff;
    z-index:2;
 }
.contact-public li{
   display:block;
   border-bottom:1px solid #7D7D7D;
   text-align:center;
               -webkit-box-sizing:border-box;
                  -moz-box-sizing:border-box;
                     -o-box-sizing:border-box;   
                        box-sizing:border-box;
}
.contact-public li:last-child{
   border-bottom:0;
   border-top:1px solid #242424;


}
.contact-public a{
   display:block;
   color:#fff;
   font-size:16px;
   text-align:center;
   padding:6px;
}
.contact-public .icon-tel{
   display:block;
   margin:0px auto 0 auto;
   width:25px;
   height:20px;
   background:url("http://wfx.91dcw.com/weixin/images/distribution2.png") -200px -820px;
   background-size:300px 1000px;
}

</style>



<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.mmenu.js"></script>
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/ectouch.js"></script>

<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.moress.js"></script>

  <script type="text/javascript">
      var url = 'index_bestgoods.php?act=ajax';
      $(function(){

          $('#J_ItemList').more({'address': url});
      });

  </script>



<script type="text/javascript">
window.onload = function(){
  $('#menu').css('display','');
}
$(function() {
	$('nav#menu').mmenu();
	$('#get_search_box').click(function(){
		$(".mm-page").children('div').hide();
		$("#main-search").css('position','fixed').css('top','0px').css('width','100%').css('z-index','999').show();
		//$('#keywordBox').focus();
	})
	$("#main-search .close").click(function(){
		$(".mm-page").children('div').show();
		$("#main-search").hide();
	})
});
</script>

<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
</body>
</html>