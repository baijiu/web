<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
    <meta charset="utf-8" />
    <title>财务中心 </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="format-detection" content="telephone=no" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/user_main.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>
<style>
    .nrxx{
        line-height: 25px;
        height: 25px;
        padding: 13px;
        border-bottom: rgb(243, 234, 234) 1px solid;
    }
</style>
<body>
<header id="header">
    <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1)"> 返回 </a> </div>
    <h1> 财务中心 </h1>
</header>
<div>
    <div class="nr_bj01">
        <div style="text-align: center"><p class="zt_bai zt_20px"><span class="zt_jc">￥</span>&nbsp;<?php echo $this->_var['user']['user_money']; ?></p></div>
        <div style="text-align: center"><p  class="zt_bai">可用余额</p></div>
        <div class="nr_bj01_a">
            <div style="float: left;    width:50%;">
                <a  href="user.php?act=account_deposit"><img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc37.png" style="width: 40px;padding: 10px;margin-bottom: 5px;"></a>
                <p><a  href="user.php?act=account_deposit">账户充值</a></p>
            </div>

               <!-- <div style="float: left;    width: 33%;">
                    <a  href="user.php?act=withdraw"><img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc36.png" style="width: 40px;padding: 10px;margin-bottom: 5px;"> </a>
                    <p><a  href="user.php?act=withdraw">余额提现</a></p>
                </div>-->

            <div style="float: left;    width: 50%;">
                <a  href="user.php?act=gain_withdraw"><img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc36.png" style="width: 40px;padding: 10px;margin-bottom: 5px;"> </a>
                <p><a  href="user.php?act=withdraw">提现</a></p>
            </div>


        </div>
    </div>
</div>
<div class="nr" style="width: 95%;margin: 70px auto 0; border-radius: 8px;">
    <div style="padding:0 15px;">

<!--
        <a  href="user.php?act=zhuanzhang">
            <div style="padding: 15px 0;border-bottom: 1px solid #E8E8E8;height:25px">
                <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc38.png" style="width: 25px;float: left">
                <p style="float: left;width: 50%;margin-top: 2px;margin-left: 8px">余额转账 <p/>
                <span class="zt_hui01" style="float: right;margin-top: 2px">余额互转<i class="layui-icon layui-icon-right"></i></span>
            </div>
        </a>-->


      <!--  <a  href="user.php?act=pointzhuanzhang">
            <div style="padding: 15px 0;border-bottom: 1px solid #E8E8E8;height:25px">
                <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc38.png" style="width: 25px;float: left">
                <p style="float: left;width: 50%;margin-top: 2px;margin-left: 8px">积分转账 <p/>
                <span class="zt_hui01" style="float: right;margin-top: 2px"><i class="layui-icon layui-icon-right"></i></span>
            </div>
        </a>-->

        <a  href="user.php?act=gaintomoney">
            <div style="padding: 15px 0;border-bottom: 1px solid #E8E8E8;height:25px">
                <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc38.png" style="width: 25px;float: left">
                <p style="float: left;width: 50%;margin-top: 2px;margin-left: 8px">收益转余额 <p/>
                <span class="zt_hui01" style="float: right;margin-top: 2px"><i class="layui-icon layui-icon-right"></i></span>
            </div>
        </a>

        <a  href="user.php?act=recharge_log">
        <div style="padding: 15px 0;border-bottom: 1px solid #E8E8E8;height:25px">
            <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc38.png" style="width: 25px;float: left">
            <p style="float: left;width: 50%;margin-top: 2px;margin-left: 8px">充值记录 <p/>
            <span class="zt_hui01" style="float: right;margin-top: 2px"><i class="layui-icon layui-icon-right"></i></span>
        </div>
        </a>
        <a  href="user.php?act=withdraw_log">
        <div style="padding: 15px 0;border-bottom: 1px solid #E8E8E8;height:25px">
            <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc39.png" style="width: 25px;float: left">
            <p style="float: left;width: 50%;margin-top: 2px;margin-left: 8px">提现记录 <p/>
            <span class="zt_hui01" style="float: right;margin-top: 2px"><i class="layui-icon layui-icon-right"></i></span>
        </div>
        </a>
        <a  href="money_log.php?type=user_money">
        <div style="padding: 15px 0;border-bottom: 1px solid #E8E8E8;height:25px">
            <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc40.png" style="width: 25px;float: left">
            <p style="float: left;width: 50%;margin-top: 2px;margin-left: 8px">余额明细 <p/>
            <span class="zt_hui01" style="float: right;margin-top: 2px">查看余额明细<i class="layui-icon layui-icon-right"></i></span>
        </div>
        </a>
        <a  href="money_log.php?type=pay_points">
        <div style="padding: 15px 0;height:25px">
            <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc41.png" style="width: 25px;float: left">
            <p style="float: left;width: 50%;margin-top: 2px;margin-left: 8px">积分明细 <p/>
            <span class="zt_hui01" style="float: right;margin-top: 2px">查看积分明细<i class="layui-icon layui-icon-right"></i></span>
        </div>
        </a>
        <div class="qcfd" style="height: 1px"></div>
    </div>
</div>

<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>
