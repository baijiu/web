<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8" />
<title><?php echo $this->_var['page_title']; ?> </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/iconfontcss/iconfont.css" rel="stylesheet" type="text/css" />

<?php echo $this->smarty_insert_scripts(array('files'=>'common.js,user.js')); ?>
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery-1.4.4.min.js"></script>
<?php echo $this->smarty_insert_scripts(array('files'=>'utils.js')); ?>

<script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>
<body style="background:#ffffff;">


<style>
.key{
	display:table-cell; 

}

.key i{
	
	font-size:20px
}

.val{
	display:table-cell;
	width:100%;
}

.val input{
	border:none;
}

</style>

 
<?php if ($this->_var['action'] == 'login' || $this->_var['action'] == 'register'): ?>
<div id="page">
  <header id="header">
    <div class="header_l"> <a class="ico_10" onClick="javascript:history.back();"> 返回 </a> </div>
    <h1> 
    <?php if ($this->_var['action'] == 'login'): ?>登录<?php else: ?>注册<?php endif; ?> </h1>
  </header>
</div>
<section class="wrap dlys" style="background:#ffffff;">
  <div id="leftTabBox" class="loginBox">
    
    <div class="bd"<?php if ($this->_var['action'] == 'register'): ?> style="display:none"<?php endif; ?>>
      <ul>
        <div class="table_box" style="padding:0 10px">
          <form name="formLogin" action="user.php" method="post" onSubmit="return userLogin()">
          
          <dl style="width: 72%; height:212px;margin: 1rem auto 0;text-align: center;display: none">

            </dl>
            
            <dl style="border-bottom: 1px solid #eee; ">
              <dd>
               <p  class=" key"><i class="iconfont icon-phone"></i></p><span class="val" > <input placeholder="请输入<?php echo $this->_var['lang']['username']; ?>/<?php echo $this->_var['lang']['mobile']; ?>" name="username" type="text"  class="inputBg" id="username" /></span>
              </dd>
            </dl>

            <dl  style="border-bottom: 1px solid #eee;">
              <dd>
               <p class="key"><i class="iconfont icon-password"></i></p><span class="val" > <input  placeholder="请输入登录<?php echo $this->_var['lang']['label_password']; ?>"  name="password" type="password" class="inputBg" /></span>
              </dd>
            </dl>
            
            
            <dl>
              <dd>
                <input type="checkbox" value="1" name="remember" id="remember" style="vertical-align:middle;" /><label for="remember"> 记住我</label>
              </dd>
            </dl>

            <dl>
              <dd>
                <input type="hidden" name="act" value="act_login" />
                <input type="hidden" name="back_act" value="<?php echo $this->_var['back_act']; ?>" />
                <input type="submit" name="submit"  value="立即登陆" class="c-btn3" />
              </dd>
            </dl>
          </form>
          <dl>
            <dd> <a href="user.php?act=get_password" class="f6">忘记密码？</a>  <a style="float:right" href="user.php?act=register" class="f6">新用户注册</a> </dd>
          </dl>

        </div>
      </ul>
    </div>

    <div class="bd"<?php if ($this->_var['action'] == 'login'): ?>style="display:none"<?php endif; ?>>
      <ul style="height:25rem">
        <form action="user.php" method="post" name="formUser" onsubmit="return register2();">
          <input type="hidden" name="flag" id="flag" value="register" />
          <div class="table_box"  style="padding:0 10px">

            <dl style="border-bottom: 1px solid #eee; ">
              <dd>
                <p  class=" key"><i class="iconfont icon-uname"></i></p><span class="val" ><input placeholder="请输入用户名" class="inputBg" name="user_name" id="user_name" type="text" /></span>
              </dd>
            </dl>

            <dl style="border-bottom: 1px solid #eee; ">
              <dd>
                <p  class=" key"><i class="iconfont icon-password"></i></p><span class="val" ><input placeholder="请输入登录密码" class="inputBg" name="password" id="mobile_pwd" type="password" /></span>
              </dd>
            </dl>


            <dl style="border-bottom: 1px solid #eee; ">
              <dd>
                <p  class=" key"><i class="iconfont icon-phone"></i></p><span class="val" ><input placeholder="请输入手机号码" class="inputBg" name="mobile" id="mobile_phone" type="text" /></span>
              </dd>
            </dl>

            
            <dl style="border-bottom: 1px solid #eee; ">
              <dd>
                <p  class=" key"><i class="iconfont icon-Code"></i></p><span class="val" ><input placeholder="请输入验证码" class="inputBg" name="mobile_code" id="mobile_code" type="text" /></span>
              </dd>
              <dd>
              <input type="hidden" name="uv_r" value="" id="uv_r">
              <input id="zphone" name="sendsms" type="button" value="获取验证码" onClick="sendSms();" class="c-btn3" style="background: #fff; color:#E5004F ;border: 1px solid #e5004f;border-radius: .6rem;    height: 2rem;
    line-height: 2rem;"  />
              </dd>
            </dl>
            
              <dl style="border-bottom: 1px solid #eee; ">
              <dd>
                <?php if ($this->_var['parent_id'] != ''): ?>
                <p  class=" key"><i class="iconfont icon-parent"></i></p><span class="val" ><input placeholder="请输入推荐人ID(必填)" class="inputBg" name="parent_id" id="parent_id" type="text" value="<?php echo $this->_var['parent_id']; ?>" readonly /></span>
                <?php else: ?>
                <p  class=" key"><i class="iconfont icon-parent"></i></p><span class="val" ><input placeholder="请输入推荐人ID(必填)" class="inputBg" name="parent_id" id="parent_id" type="text" /></span>
                <?php endif; ?>
              </dd>
            </dl>    
            
            <dl>
              <dd>
                <input id="agreement" name="agreement" type="checkbox" value="1" checked="checked" style="vertical-align:middle; " /><label for="agreement"> 我已看过并同意《<a href="article.php?id=47">用户协议</a>》</label>
              </dd>
            </dl>
            <dl>
              <dd>
                <input name="act" type="hidden" value="act_register" />
                <input name="enabled_sms" type="hidden" value="1" />
                <input type="hidden" name="back_act" value="<?php echo $this->_var['back_act']; ?>" />
                <input name="Submit" type="submit" value="下一步" class="c-btn3" />
              </dd>
            </dl>
             <dl>
            <dd> <a href="user.php?act=login" class="f6">已有账号？</a>  </dd>
          </dl>
            
          </div>
        </form>
        
        <?php if ($this->_var['need_rechoose_gift']): ?>
        <?php echo $this->_var['lang']['gift_remainder']; ?>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</section>
<script type="text/javascript">
jQuery(function($){
	$('.hd ul li').click(function(){
		var index = $('.hd ul li').index(this);
		$(this).addClass('on').siblings('li').removeClass('on');
		$('.loginBox .bd:eq('+index+')').show().siblings('.bd').hide();
	})
})
</script>

<script type="text/javascript">
	         var _uuid = getUUID();
	       if(getCookie("_UUID_UV")!=null && getCookie("_UUID_UV")!=undefined)
	        {
	            _uuid = getCookie("_UUID_UV");
	         }else{
	             setCookie("_UUID_UV",_uuid);
	         }
		
	         document.getElementById("uv_r").value = _uuid;//赋给hidden表单
	        //生成唯一标识
	         function getUUID()
	         {
	             var uuid = new Date().getTime();
	             var randomNum =parseInt(Math.random()*1000);
	             return uuid+randomNum.toString();
	         }
	         //写cookie
	         function setCookie(name,value)
	         {
	             var Days = 365;//这里设置cookie存在时间为一年
	             var exp = new Date();
	             exp.setTime(exp.getTime() + Days*24*60*60*1000);
	             document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
	         }
	         //获取cookie
	         function getCookie(name)
	         {
	             var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
	             if(arr=document.cookie.match(reg))
	                 return unescape(arr[2]);
	             else
	                 return null;
	         }
	 </script>

<?php endif; ?> 
 



 
<?php if ($this->_var['action'] == 'get_password'): ?> 
<?php echo $this->smarty_insert_scripts(array('files'=>'utils.js')); ?> 
<script type="text/javascript">
    <?php $_from = $this->_var['lang']['password_js']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
      var <?php echo $this->_var['key']; ?> = "<?php echo $this->_var['item']; ?>";
    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
    </script>
<div id="page">
  <header id="header">
    <div class="header_l"> <a class="ico_10" onClick="javascript:history.back();"> 返回 </a> </div>
    <h1> 找回密码 </h1>
  </header>
</div>
<section class="wrap">
  <div id="leftTabBox" class="loginBox" style="padding:0 10px">
    <div class="hd"> <span>您可通过手机号码重置密码</span>
    </div>
    <div id="tabBox1-bd">
      <ul>

      	<form  action="user.php" method="post" name="getPassword" onSubmit="return submitForget();">
          <input type="hidden" name="flag" id="flag" value="forget" />
          <div class="table_box"  >
            <dl style="border-bottom: 1px solid #eee; ">
              <dd>
                 <p  class=" key"><i class="iconfont icon-phone"></i></p><span class="val" ><input placeholder="请输入手机号码" class="inputBg" name="mobile" id="mobile_phone" type="text" /></span>
              </dd>
            </dl>
            <dl style="border-bottom: 1px solid #eee; ">
              <dd>
                <p  class=" key"><i class="iconfont icon-Code"></i></p><span class="val" ><input placeholder="请输入验证码" class="inputBg" name="mobile_code" id="mobile_code" type="text" /></span>
              </dd>
              <dd>
              
              <input id="zphone" name="sendsms" type="button" value="获取验证码" onClick="sendSms();" class="c-btn3" style="background: #fff; color:#E5004F ;border: 1px solid #e5004f;border-radius: .6rem;    height: 2rem;
    line-height: 2rem;"/>
              </dd>
            </dl>
            <dl>
              <dd>
                <input name="act" type="hidden" value="send_pwd_sms" />
                <input name="Submit" type="submit" value="<?php echo $this->_var['lang']['submit']; ?>" class="c-btn3" />
              </dd>
            </dl>
          </div>
        </form>

      </ul>
    </div>
  </div>
</section>
<?php endif; ?> 



<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/sms.js"></script>
<div style="width:1px; height:1px; overflow:hidden"><?php $_from = $this->_var['lang']['p_y']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'pv');if (count($_from)):
    foreach ($_from AS $this->_var['pv']):
?><?php echo $this->_var['pv']; ?><?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?></div>
<script type="text/javascript">
var process_request = "<?php echo $this->_var['lang']['process_request']; ?>";
<?php $_from = $this->_var['lang']['passport_js']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
var <?php echo $this->_var['key']; ?> = "<?php echo $this->_var['item']; ?>";
<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
var username_exist = "<?php echo $this->_var['lang']['username_exist']; ?>";
</script>
</body>
</html>
