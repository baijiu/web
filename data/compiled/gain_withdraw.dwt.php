<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8" />
<title><?php echo $this->_var['page_title']; ?> </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/user_main.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css">
<script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>
<body>
<header id="header">
    <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1)"> 返回 </a> </div>
    <h1> 提现 </h1>
</header>
<script src="<?php echo $this->_var['ectouch_themes']; ?>/layui/layui.js"></script>
<script>
    //一般直接写在一个js文件中
    layui.use(['layer', 'form'], function(){
        var layer = layui.layer
            ,form = layui.form;

//  layer.msg('Hello World');
    });
</script>
<div class="nr" style="background-color: rgba(45,41,41,1.00);height: 100px;text-align: center;padding: 30px 0 0 0;margin: 0">
    <div>
        <h1 class="ztys_cheng01 zt_30px"><?php echo $this->_var['user_info']['user_gain']; ?></h1>
        <br>
        <p class="ztyx_bai01">可提现金额(元)</p>
    </div>
</div>
<div style="height: 100px">
    <div class="fd_zuo">
        <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc26.png" style="width: 50px;margin: 10px;">
        <p>提交成功</p>
    </div>
    <div class="fd_zhong"><i class="layui-icon layui-icon-right " style="font-size: 39px"></i>   </div>
    <div class="fd_zuo">
        <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc27.png" style="width: 50px;margin: 10px;">
        <p>等待审核</p>

    </div>
    <div class="fd_zhong"><i class="layui-icon layui-icon-right " style="font-size: 39px"></i>   </div>
    <div class="fd_zuo">
        <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc28.png" style="width:50px;margin: 10px;">
        <p>提现成功</p>
    </div>
</div>
<div class="nr" style="padding: 20px">
    <form  name="formWithdraw" action="user.php" method="post" onSubmit="return before_Withdraw()">
    <input type="text" class="sr01" name="amount" placeholder="最低限额<?php echo $this->_var['min']; ?>元">
    <br>
    <br>
    <input type="text" class="sr01" name="bankname" readonly  placeholder="请输入开户行" value="<?php echo $this->_var['user_info']['bankname']; ?>">
    <br>
    <br>
    <input type="text" class="sr01" name="accountname" readonly placeholder="请输入姓名"  value="<?php echo $this->_var['user_info']['accountname']; ?>">
    <br>
    <br>
    <input type="text" class="sr01" name="accountnumber" readonly placeholder="请输入卡号" value="<?php echo $this->_var['user_info']['accountnumber']; ?>">
    <br>
    <br>
        <input name="act" type="hidden" value="do_gain_withdraw" />
    <input type="submit"  value="申请提现" class="botton3">
    </form>
</div>
<script>

    /* 会员修改密码 */
    function before_Withdraw()
    {
        var frm              = document.forms['formWithdraw'];
        var amount     = frm.elements['amount'].value;
        var bankname     = frm.elements['bankname'].value;
        var accountname     = frm.elements['accountname'].value;
        var accountnumber     = frm.elements['accountnumber'].value;
        var msg = '';
        var reg = null;

        if (amount.length == 0)
        {
            msg += '请输入提现金额\n';
        }

        if (bankname.length == 0)
        {
            msg += '请输入开户银行\n';
        }
        if (accountname.length == 0)
        {
            msg += '请输入开户姓名\n';
        }
        if (accountnumber.length == 0)
        {
            msg += '请输入银行卡号\n';
        }
        if (msg.length > 0)
        {
            alert(msg);
            return false;
        }
        else
        {
            return true;
        }
    }
</script>
<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>
