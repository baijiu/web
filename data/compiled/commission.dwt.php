<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8" />
<title>我的投资 </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/user_main.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery.more.js"></script>
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>

<style>
    .ico_10 {
        background: url(themes/miqinew/images/ico_10.png) no-repeat 30% 50%;
        -webkit-background-size: 1.2rem 1.2rem;
        -moz-background-size: 1.2rem 1.2rem;
        background-size: 1.2rem 1.2rem;
    }
    .jiazai{
        color: #a7a7a7;
        height: 40px;
        text-align: center;
        line-height: 40px;
    }
    .more_loader_spinner {
        text-align: center;
    }
    .laizi{
        overflow: hidden;
        text-overflow: ellipsis;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
    }

    .nr_px2 li {
        height: 35px;
    }
</style>

<body style="background: #FFFFFF;">
<header id="header">
    <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1)"> 返回 </a> </div>
    <h1> 我的投资 </h1>
</header>
<div class="nr" style="background-color: rgba(0,0,0,0.00);text-align: center;margin-top: 0px">
    <ul class="nr_px">
        <li class="laizi">项目名称</li>
        <li>投资金额</li>
        <li>总收益</li>
        <li class="border_right1 laizi">时间</li>
    </ul>


    <div id="more" >
        <div class="single_item info">
        </div>
        <a href="javascript:;" class="get_more" style="text-align:center;"><img src='<?php echo $this->_var['ectouch_themes']; ?>/images/loader.gif' width="12" height="12"></a>
    </div>
    <script type="text/javascript">
        //瀑布流
        var url = 'commission.php?act=ajax';
        $(function() {
            $('#more').more({'address': url})
        });
    </script>



</div>
<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>
