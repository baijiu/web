<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8" />
<title><?php echo $this->_var['page_title']; ?> </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />

<?php echo $this->smarty_insert_scripts(array('files'=>'transport.js,common.js,user.js')); ?>
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>
<body>
<div id="tbh5v0">
  <div class="login">

      
    <?php if ($this->_var['action'] == 'order_list'): ?>
    <header id="header">
      <div class="header_l header_return"> <a class="ico_10" href="user.php"> 返回 </a> </div>
      <h1> <?php echo $this->_var['lang']['label_order']; ?> </h1>
    </header>
    <!--<div class="list_daohang">
    	<div class="abq_dh"><a href="#"><span>全部</span></a></div>
    	<div class="abq_dh"><a href="#"><span>未付款</span></a></div>
    	<div class="abq_dh"><a href="#"><span>待付款</span></a></div>
    	<div class="abq_dh"><a href="#"><span>待收货</span></a></div>
    	<div class="dh_active"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
    </div>-->
    <section class="wrap order_list" style="margin-top: 0rem; ">
      <section class="order_box padd1 radius10 single_item">
      <table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table">

        <tr>
        	<td class="order_content" colspan="2"></td>
        </tr>
        <tr class="order_tr">
          <td class="order_handler"></td>
          <td class="order_tracking"></td>
        </tr>
      </table>
    </section>
    <a href="javascript:;" style="text-align:center" class="get_more"></a>
    </section>
	<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.more.js"></script>
    <script type="text/javascript">
    <?php $_from = $this->_var['lang']['merge_order_js']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
	    var <?php echo $this->_var['key']; ?> = "<?php echo $this->_var['item']; ?>";
    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
    jQuery(function($){
    	$('.order_list').more({'address': 'user.php?act=async_order_list', amount: 3, 'spinner_code':'<div style="text-align:center; margin:10px;"><img src="<?php echo $this->_var['ectouch_themes']; ?>/images/loader.gif" /></div>'})
		$(window).scroll(function () {
			if ($(window).scrollTop() == $(document).height() - $(window).height()) {
				$('.get_more').click();
			}
		});
    });
    </script>
    <?php endif; ?> 
    




     
    <?php if ($this->_var['action'] == 'order_tracking'): ?>
      <header id="header">
          <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1)"> 返回 </a> </div>
          <h1> <?php echo $this->_var['lang']['label_track_packages']; ?> </h1>
      </header>

      <div style="line-height:2em;padding: 20px; background:#fff;">
          <?php if ($this->_var['order']['shipping_status'] == 0): ?>
              <p>订单暂未发货，请稍后再来查询</p>

          <?php else: ?>
              <p>快递：<?php echo $this->_var['order']['shipping_name']; ?></p>
              <p>快递单号：<?php echo $this->_var['order']['invoice_no']; ?></p>

          <?php endif; ?>
      </div>

      <div class="fullscreen">
          <div class="blank"></div>
          <div data-role="content" class="smart-result">
              <div class="content-primary">
                  <table id="queryResult" cellpadding="0" cellspacing="0">

                  </table>
              </div>
          </div>
          <script type="text/javascript">
              jQuery(function($){
                  var resultJson = eval('(<?php echo $this->_var['trackinfo']; ?>)');
                  var resultTable = $("#queryResult");
                  resultTable.empty();
                  if(resultJson.status == 200) { //成功
                      var resultData = resultJson.data;
                      for (var i = resultData.length - 1; i >= 0; i--) {
                          var className = "";
                          if(i%2 == 0){
                              className = "even";
                          }else{
                              className="odd";
                          }
                          if(resultData.length == 1){
                              if(resultJson.ischeck == 1){
                                  className += " checked";
                              }else{
                                  className += " wait";
                              }
                          }else if(i == resultData.length - 1){
                              className += " first-line";
                          }else if(i == 0){
                              className += " last-line";
                              if(resultJson.ischeck == 1){
                                  className += " checked";
                              }else{
                                  className += " wait";
                              }
                          }

                          var index = resultData[i].ftime.indexOf(" ");
                          var result_date = resultData[i].ftime.substring(0,index);
                          var result_time = resultData[i].ftime.substring(index+1);

                          var s_index = result_time.lastIndexOf(":");
                          result_time = result_time.substring(0,s_index);

                          resultTable.append("<tr class='" + className + "'><td class='col1'><span class='result-date'>" + result_date + "</span><span class='result-time'>" + result_time + "</span></td><td class='colstatus'></td><td class='col2'><span>" + resultData[i].context + "</span></td></tr>");
                      }
                      $("body").animate({scrollTop: "1000px"}, 1000);
                  }else if(resultJson.status == 400){
                      resultTable.append("<tr><td>订单暂未发货，请稍后再来查询</td></tr>");
                  }else{
                      resultTable.append("<tr><td>"+ resultJson.message +"</td></tr>");
                  }
              })
          </script>
      </div>
    <?php endif; ?> 
    


    
    <?php if ($this->_var['action'] == order_detail): ?>
	<style type="text/css">
    /* 本例子css */
    .pay_bottom{
        display: inline-block;
        min-width: 60px;
        height: 40px;
        padding: 0 15px;
        border: 0;
        background: #f40;
        text-align: center;
        text-decoration: none;
        line-height: 40px;
        color: #fff;
        font-size: 14px;
        font-weight: 700;
        -webkit-border-radius: 2px;
        background: -webkit-gradient(linear,0 0,0 100%,color-stop(0,#f50),color-stop(1,#f40));
        text-shadow: 0 -1px 1px #ca3511;
        -webkit-box-shadow: 0 -1px 0 #bf3210 inset;
    }
    </style>
    <header id="header">
      <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1)"> 返回 </a> </div>
      <h1> <?php echo $this->_var['lang']['order_status']; ?> </h1>
    </header>
    <section class="wrap">
      <section class="order_box padd1 radius10">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table">
      	<tr>
        	<td>订单状态：<?php echo $this->_var['order']['order_status']; ?> <?php echo $this->_var['order']['pay_status']; ?> <?php echo $this->_var['order']['shipping_time']; ?></td>
        </tr>
        <tr>
        	<td>订单编号：<?php echo $this->_var['order']['order_sn']; ?></td>
        </tr>
        <tr>
        	<td>订单金额：<?php echo $this->_var['order']['formated_total_fee']; ?></td>
        </tr>
        <tr>
        	<td>下单时间：<?php echo $this->_var['order']['formated_add_time']; ?></td>
        </tr>
        <?php if ($this->_var['order']['to_buyer']): ?>
        <tr>
          <td><?php echo $this->_var['lang']['detail_to_buyer']; ?>：<?php echo $this->_var['order']['to_buyer']; ?></td>
        </tr>
        <?php endif; ?>
        <?php if ($this->_var['virtual_card']): ?>
        <tr>
          <td><?php echo $this->_var['lang']['virtual_card_info']; ?>：<br>
          	<?php $_from = $this->_var['virtual_card']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'vgoods');if (count($_from)):
    foreach ($_from AS $this->_var['vgoods']):
?> 
            <?php $_from = $this->_var['vgoods']['info']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'card');if (count($_from)):
    foreach ($_from AS $this->_var['card']):
?> 
            <hr style="border:none;border-top:1px #CCCCCC dashed; margin:5px 0" />
            <?php if ($this->_var['card']['card_sn']): ?><?php echo $this->_var['lang']['card_sn']; ?>:<span style="color:red;"><?php echo $this->_var['card']['card_sn']; ?></span><br><?php endif; ?>
            <?php if ($this->_var['card']['card_password']): ?><?php echo $this->_var['lang']['card_password']; ?>:<span style="color:red;"><?php echo $this->_var['card']['card_password']; ?></span><br><?php endif; ?> 
            <?php if ($this->_var['card']['end_date']): ?><?php echo $this->_var['lang']['end_date']; ?>:<?php echo $this->_var['card']['end_date']; ?><br><?php endif; ?>
            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?> 
            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?></td>
        </tr>
        <?php endif; ?>
        <?php if ($this->_var['order']['invoice_no']): ?>
        <tr>
            <td><a href="user.php?act=order_tracking&order_id=<?php echo $this->_var['order']['order_id']; ?>" class="c-btn3">订单跟踪</a></td>
        </tr>
        <?php endif; ?>
      </table>
      </section>
      

      
      <section class="order_box padd1 radius10">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table">
      	  <?php $_from = $this->_var['goods_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'goods');if (count($_from)):
    foreach ($_from AS $this->_var['goods']):
?>
          <tr>
            <td rowspan="2" width="60" align="center" valign="middle" style="border-bottom:1px #CCCCCC dashed">
            <a href="goods.php?id=<?php echo $this->_var['goods']['goods_id']; ?>" target="_blank"><img src="<?php echo $this->_var['site_url']; ?><?php echo $this->_var['goods']['goods_thumb']; ?>" width="60" height="60" /></a></td>
            <td><?php echo $this->_var['goods']['goods_name']; ?></td>
          </tr>
          <tr>
            <td style="border-bottom:1px #CCCCCC dashed">售价：<?php echo $this->_var['goods']['goods_price']; ?> 数量：<?php echo $this->_var['goods']['goods_number']; ?><br>小计：<?php echo $this->_var['goods']['subtotal']; ?></td>
          </tr>
          <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
      </table>
      <table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table">
        <tr>
          <td align="right"> <?php echo $this->_var['lang']['goods_all_price']; ?><?php if ($this->_var['order']['extension_code'] == "group_buy"): ?><?php echo $this->_var['lang']['gb_deposit']; ?><?php endif; ?>: <?php echo $this->_var['order']['formated_goods_amount']; ?> 
            <?php if ($this->_var['order']['discount'] > 0): ?> 
            - <?php echo $this->_var['lang']['discount']; ?>: <?php echo $this->_var['order']['formated_discount']; ?> 
            <?php endif; ?> 
            <?php if ($this->_var['order']['tax'] > 0): ?> 
            + <?php echo $this->_var['lang']['tax']; ?>: <?php echo $this->_var['order']['formated_tax']; ?> 
            <?php endif; ?> 
            <?php if ($this->_var['order']['shipping_fee'] > 0): ?> 
            + <?php echo $this->_var['lang']['shipping_fee']; ?>: <?php echo $this->_var['order']['formated_shipping_fee']; ?> 
            <?php endif; ?> 
            <?php if ($this->_var['order']['insure_fee'] > 0): ?> 
            + <?php echo $this->_var['lang']['insure_fee']; ?>: <?php echo $this->_var['order']['formated_insure_fee']; ?> 
            <?php endif; ?> 
            <?php if ($this->_var['order']['pay_fee'] > 0): ?> 
            + <?php echo $this->_var['lang']['pay_fee']; ?>: <?php echo $this->_var['order']['formated_pay_fee']; ?> 
            <?php endif; ?> 
            <?php if ($this->_var['order']['pack_fee'] > 0): ?> 
            + <?php echo $this->_var['lang']['pack_fee']; ?>: <?php echo $this->_var['order']['formated_pack_fee']; ?> 
            <?php endif; ?> 
            <?php if ($this->_var['order']['card_fee'] > 0): ?> 
            + <?php echo $this->_var['lang']['card_fee']; ?>: <?php echo $this->_var['order']['formated_card_fee']; ?> 
            <?php endif; ?></td>
        </tr>
        <tr>
          <td align="right"><?php if ($this->_var['order']['money_paid'] > 0): ?> 
            - <?php echo $this->_var['lang']['order_money_paid']; ?>: <?php echo $this->_var['order']['formated_money_paid']; ?> 
            <?php endif; ?> 
            <?php if ($this->_var['order']['surplus'] > 0): ?> 
            - <?php echo $this->_var['lang']['use_surplus']; ?>: <?php echo $this->_var['order']['formated_surplus']; ?> 
            <?php endif; ?> 
            <?php if ($this->_var['order']['integral_money'] > 0): ?> 
            - <?php echo $this->_var['lang']['use_integral']; ?>: <?php echo $this->_var['order']['formated_integral_money']; ?> 
            <?php endif; ?> 
            <?php if ($this->_var['order']['bonus'] > 0): ?> 
            - <?php echo $this->_var['lang']['use_bonus']; ?>: <?php echo $this->_var['order']['formated_bonus']; ?> 
            <?php endif; ?></td>
        </tr>
        <tr>
          <td align="right"><?php echo $this->_var['lang']['order_amount']; ?>: <?php echo $this->_var['order']['formated_order_amount']; ?> 
            <?php if ($this->_var['order']['extension_code'] == "group_buy"): ?><br />
            <?php echo $this->_var['lang']['notice_gb_order_amount']; ?><?php endif; ?></td>
        </tr>

      </table>
      </section>
      


    <?php endif; ?> 
    
    








     
    <?php if ($this->_var['action'] == 'address_list'): ?>
    <header id="header">
      <div class="header_l header_return"> <a class="ico_10" href="user.php"> 返回 </a> </div>
      <h1> <?php echo $this->_var['lang']['consignee_info']; ?> </h1>
    </header>
    <div class="shrxx" style="margin-top: 0rem;">
		<section class="wrap">
		  <?php $_from = $this->_var['consignee_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('sn', 'consignee');if (count($_from)):
    foreach ($_from AS $this->_var['sn'] => $this->_var['consignee']):
?>
		  <section class="order_box padd1 radius10">
		  <table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table">
			<tr>
			  <td width="32%" align="right">姓名：</td>
			  <td width="68%" align="left"><?php echo htmlspecialchars($this->_var['consignee']['consignee']); ?></td>
			</tr>
			<tr>
			  <td align="right">电话：</td>
			  <td align="left"><?php echo $this->_var['consignee']['tel']; ?> </td>
			</tr>
			<tr>
			  <td align="right">地址：</td>
			  <td align="left"><?php echo htmlspecialchars($this->_var['consignee']['address']); ?> 
				<?php if ($this->_var['consignee']['zipcode']): ?> 
				[<?php echo $this->_var['lang']['postalcode']; ?>: <?php echo htmlspecialchars($this->_var['consignee']['zipcode']); ?>] 
				<?php endif; ?></td>
			</tr>
			<tr>
			  <td colspan="2" align="right">
				<div class="shrxx_an">
					<div class="shrxx_bj">
					  <a href="user.php?act=act_edit_address&id=<?php echo $this->_var['consignee']['address_id']; ?>&flag=display">编辑</a>
					</div> 
					<div class="shrxx_sc"> 
					  <a href="user.php?act=drop_consignee&id=<?php echo $this->_var['consignee']['address_id']; ?>" onClick="return confirm('您真的要删除该地址吗？');">删除</a>
					</div>
				</div>
			  </td>
			</tr>
		  </table>
		  </section>
      </div>
      <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
      <a href="user.php?act=act_edit_address&flag=display" class="c-btn3"><?php echo $this->_var['lang']['add_address']; ?></a>

    <?php endif; ?> 
    




    
    <?php if ($this->_var['action'] == 'act_edit_address'): ?>
    <header id="header">
      <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1)"> 返回 </a> </div>
      <h1> <?php echo $this->_var['lang']['consignee_info']; ?> </h1>
    </header>
      
      <?php echo $this->smarty_insert_scripts(array('files'=>'utils.js,transport.js,region.js,shopping_flow.js')); ?> 
      <script type="text/javascript">
		  region.isAdmin = false;
		  <?php $_from = $this->_var['lang']['flow_js']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
		  var <?php echo $this->_var['key']; ?> = "<?php echo $this->_var['item']; ?>";
		  <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
		  
		  onload = function() {
			if (!document.all)
			{
			  document.forms['theForm'].reset();
			}
		  }
		  
	  </script>
      <style>
          .inputBg_touch {

              margin-top: 0px;

              width: 100%;
          }
          .ectouch_table td {
              padding: 10px;
          }
      </style>
	<div class="shrxx" style="margin-top: 0rem;">
		<section class="wrap">
		  <section class="order_box padd1 radius10">
		  <form action="user.php" method="post" name="theForm" onsubmit="return checkConsignee(this)">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table">
			  <tr>
				<td width="70">收货人</td>
				<td align="left" ><input name="consignee" placeholder="<?php echo $this->_var['lang']['consignee_name']; ?><?php echo $this->_var['lang']['require_field']; ?>" type="text" class="inputBg_touch" value="<?php echo htmlspecialchars($this->_var['consignee']['consignee']); ?>" /> </td>
			  </tr>
			  <tr>
				<td>联系电话</td>
				<td align="left" ><input placeholder="<?php echo $this->_var['lang']['phone']; ?><?php echo $this->_var['lang']['require_field']; ?>" name="tel" type="text" class="inputBg_touch" value="<?php echo htmlspecialchars($this->_var['consignee']['tel']); ?>" /></td>
			  </tr>
			  <tr>
				<td>电子邮箱</td>
				<td align="left" ><input name="email" placeholder="<?php echo $this->_var['lang']['email_address']; ?><?php echo $this->_var['lang']['require_field']; ?>" type="text" class="inputBg_touch" value="<?php echo htmlspecialchars($this->_var['consignee']['email']); ?>" /></td>
			  </tr>
			  <tr>
				<td><?php echo $this->_var['lang']['country_province']; ?></td>
				<td align="left" >

				  <select name="province" id="selProvinces" onchange="region.changed(this, 2, 'selCities')">
					<option value="0"><?php echo $this->_var['lang']['please_select']; ?><?php echo $this->_var['name_of_region']['1']; ?></option>
					<?php $_from = $this->_var['province_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'province');if (count($_from)):
    foreach ($_from AS $this->_var['province']):
?>
					<option value="<?php echo $this->_var['province']['region_id']; ?>" <?php if ($this->_var['consignee']['province'] == $this->_var['province']['region_id']): ?>selected<?php endif; ?>><?php echo $this->_var['province']['region_name']; ?></option>
					<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
				  </select>
				  <select name="city" id="selCities" onchange="region.changed(this, 3, 'selDistricts')">
					<option value="0"><?php echo $this->_var['lang']['please_select']; ?><?php echo $this->_var['name_of_region']['2']; ?></option>
					<?php $_from = $this->_var['city_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'city');if (count($_from)):
    foreach ($_from AS $this->_var['city']):
?>
					<option value="<?php echo $this->_var['city']['region_id']; ?>" <?php if ($this->_var['consignee']['city'] == $this->_var['city']['region_id']): ?>selected<?php endif; ?>><?php echo $this->_var['city']['region_name']; ?></option>
					<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
				  </select>
				  <select name="district" id="selDistricts" <?php if (! $this->_var['district_list']): ?>style="display:none"<?php endif; ?>>
					<option value="0"><?php echo $this->_var['lang']['please_select']; ?><?php echo $this->_var['name_of_region']['3']; ?></option>
					<?php $_from = $this->_var['district_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'district');if (count($_from)):
    foreach ($_from AS $this->_var['district']):
?>
					<option value="<?php echo $this->_var['district']['region_id']; ?>" <?php if ($this->_var['consignee']['district'] == $this->_var['district']['region_id']): ?>selected<?php endif; ?>><?php echo $this->_var['district']['region_name']; ?></option>
					<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
				  </select></td>
			  </tr>
			  <tr>
				<td>详细地址</td>
				<td align="left" ><input name="address" placeholder="<?php echo $this->_var['lang']['detailed_address']; ?><?php echo $this->_var['lang']['require_field']; ?>" type="text" class="inputBg_touch" value="<?php echo htmlspecialchars($this->_var['consignee']['address']); ?>" /></td>
			  </tr>
			  <tr>
				<td>邮政编码</td>
				<td align="left" ><input placeholder="<?php echo $this->_var['lang']['postalcode']; ?>" name="zipcode" type="text" class="inputBg_touch" value="<?php echo htmlspecialchars($this->_var['consignee']['zipcode']); ?>" /></td>
			  </tr>
			  <tr>
				<td align="center" colspan="2"><?php if ($this->_var['consignee']['consignee'] && $this->_var['consignee']['email']): ?>
				  <input type="submit" name="submit"  class="c-btn3" value="<?php echo $this->_var['lang']['confirm_edit']; ?>"  style="margin-right:5px;" />
				  <?php else: ?>
				  <input type="submit" name="submit"  class="c-btn3"  value="<?php echo $this->_var['lang']['add_address']; ?>"/>
				  <?php endif; ?>
				  <input type="hidden" name="act" value="act_edit_address" />
				  <input name="address_id" type="hidden" value="<?php echo $this->_var['consignee']['address_id']; ?>" /></td>
			  </tr>
			</table>
		  </form>
		  </section>
		</section>
	</div>
    <?php endif; ?> 
    

	
    <?php echo $this->fetch('library/page_footer.lbi'); ?>
  </div>
</div>
<div style="width:1px; height:1px; overflow:hidden"><?php $_from = $this->_var['lang']['p_y']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'pv');if (count($_from)):
    foreach ($_from AS $this->_var['pv']):
?><?php echo $this->_var['pv']; ?><?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?></div>
</body>
<script type="text/javascript">
<?php $_from = $this->_var['lang']['clips_js']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
var <?php echo $this->_var['key']; ?> = "<?php echo $this->_var['item']; ?>";
<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
</script>
</html>
