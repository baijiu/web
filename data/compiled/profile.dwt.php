<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8" />
<title><?php echo $this->_var['page_title']; ?> </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/user_main.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>
<body>
<header id="header">
    <div class="header_l header_return"> <a class="ico_10" href="user.php"> 返回 </a> </div>
    <h1> 个人资料 </h1>
</header>


<form class="layui-form"  action="">

<div class="nr" style="height:206px">
    <div class="touxiang">
        <div class="tx_zuo zt_jc">头像</div>
        <div class="botton" style="margin: 20px 20px 20px 0;">
            <button type="button" class="layui-btn" id="uphimgbt">
                <i class="layui-icon">&#xe67c;</i>上传头像
            </button>
        </div>
        <div class="tx_you">
            <img id="headimg" width="60px" <?php if ($this->_var['user_info']['head_img'] != ''): ?> src="<?php echo $this->_var['user_info']['head_img']; ?>" <?php else: ?>src="<?php echo $this->_var['ectouch_themes']; ?>/images/get_avatar.png" width="60px"<?php endif; ?>>
        </div>

    </div>
    <div class="qcfd_xhx"></div>
    <div class="gynr">
        <div class="gynr_zuo zt_jc">账号</div>
        <input class="gynr_you"  type="text" value="<?php echo $this->_var['user_info']['user_name']; ?>" readonly>
    </div>

    <div class="qcfd_xhx"></div>
    <div class="gynr">
        <div class="gynr_zuo zt_jc">手机</div>
        <input class="gynr_you" id="mobile" name="mobile" type="text" placeholder="请输入手机号"  value="<?php echo $this->_var['user_info']['mobile_phone']; ?>" <?php if ($this->_var['user_info']['mobile_phone']): ?> readonly <?php endif; ?> lay-verify="mobile" >
    </div>
</div>
<div class="nr">
    <div style="height:20px;"></div>
    <div class="gynr">
        <div class="gynr_zuo zt_jc">姓&nbsp;&nbsp;&nbsp;&nbsp;名</div>
        <input class="gynr_you" type="text" value="<?php echo $this->_var['user_info']['accountname']; ?>" placeholder="请输入姓名" name="accountname"  lay-verify="username">
    </div>
    <div class="qcfd_xhx"></div>
    <div class="gynr">
        <div class="gynr_zuo zt_jc">身份证</div>
        <input class="gynr_you" type="text" value="<?php echo $this->_var['user_info']['id_card']; ?>" placeholder="请输入身份证号码" name="id_card"  lay-verify="id_card">
    </div>


    <div class="qcfd_xhx"></div>
    <div class="gynr">
        <div class="gynr_zuo zt_jc">银&nbsp;&nbsp;&nbsp;&nbsp;行</div>
        <input class="gynr_you" type="text" value="<?php echo $this->_var['user_info']['bankname']; ?>" placeholder="请输入银行名称"  name="bankname"  lay-verify="bankname">
    </div>

    <div class="qcfd_xhx"></div>
        <div class="gynr">
            <div class="gynr_zuo zt_jc">地&nbsp;&nbsp;&nbsp;&nbsp;址</div>
            <input class="gynr_you" type="text" value="<?php echo $this->_var['user_info']['bank_address']; ?>" placeholder="请输入银行名称开户行地址" name="bank_address"  lay-verify="bank_address">
        </div>


    <div class="qcfd_xhx"></div>
    <div class="gynr">
        <div class="gynr_zuo zt_jc">卡&nbsp;&nbsp;&nbsp;&nbsp;号</div>
        <input class="gynr_you" type="text" value="<?php echo $this->_var['user_info']['accountnumber']; ?>" placeholder="请输入银行卡号"   name="accountnumber"   lay-verify="accountnumber">
    </div>

    <div class="qcfd_xhx"></div>
    <div class="gynr" style="line-height: 2rem;height: 2rem;">
        <div class="gynr_zuo zt_jc">验证码</div>
        <input style="width: 30%" class="gynr_you" type="text" id="smscode" name="smscode"   placeholder="请输入验证码" lay-verify="smscode">
        <input class="yzm" type="button" onClick="sendSms();"  id="zphone"  value="获取验证码" >
    </div>

    <div class="qcfd" style="height:15px"></div>
</div>
<div class="botton2" style="margin: 15px">
    <input type="button" lay-submit  value="保存" lay-filter="formDemo" class="shangchuan zt_20px ztyx_bai01">
</div>
</form>

<script src="<?php echo $this->_var['ectouch_themes']; ?>/layui/layui.js"></script>


<script>
    layui.use('layer', function(){
        var layer = layui.layer;

    });

    function sendSms(){

        var mobile = $('#mobile').val();
        $.post('sms.php?act=edit_profile_send', {"mobile": mobile},function(result){
            if (result.code==2){
                RemainTime();
                layer.msg('手机验证码已经成功发送到您的手机');

            }else{
                if(result.msg){
                    layer.msg(result.msg);

                }else{
                    layer.msg('手机验证码发送失败');
                }
            }
        }, "json");
    }


    var iTime = 59;
    var Account;
    function RemainTime(){
        document.getElementById('zphone').disabled = true;
        var iSecond,sSecond="",sTime="";
        if (iTime >= 0){
            iSecond = parseInt(iTime%60);
            if (iSecond >= 0){
                sSecond = iSecond + "秒";
            }
            sTime=sSecond;
            if(iTime==0){
                clearTimeout(Account);
                sTime='获取验证码';
                iTime = 59;
                document.getElementById('zphone').disabled = false;
            }else{
                Account = setTimeout("RemainTime()",1000);
                iTime=iTime-1;
            }
        }else{
            sTime='没有倒计时';
        }
        document.getElementById('zphone').value = sTime;
    }

    layui.use('upload', function(){
        var upload = layui.upload;

        //执行实例
        var uploadInst = upload.render({
            elem: '#uphimgbt' //绑定元素
            ,url: 'user.php?act=up_headimg' //上传接口
            , exts: 'jpg|png|gif|jpeg|'        //可传输文件的后缀
            , accept: 'file'              //video audio images
            ,done: function(res){
                if(res.error==1){
                    layer.msg(res.content);
                }else{
                    layer.msg(res.content);
                    $("#headimg").attr('src',res.file_url);
                }
                //上传完毕回调
            }
            ,error: function(){
                layer.msg('请求异常, 请重试');
                //请求异常回调
            }
        });
    });

    layui.use('form', function () {
        var form = layui.form;
        form.verify({
            mobile:function(val,item)
            {
                if(!val)
                {
                    return '手机号不能为空';
                }
            },
            username:function(val,item)
            {
                if(!val)
                {
                    return '姓名不能为空';
                }
            },
            id_card:function(val,item)
            {
                if(!val)
                {
                    return '请输入身份证号';
                }
            }
            ,bankname:function(val,item)
            {
                if(!val)
                {
                    return '银行名称不能为空';
                }
            }
            ,bank_address:function(val,item)
                         {
                             if(!val)
                             {
                                 return '开户行地址不能为空';
                             }
                         }
            ,accountnumber:function(val,item)
            {
                if(!val)
                {
                    return '银行卡号不能为空';
                }
                if(!/^[0-9]+$/.test(val)){
                    return '请输入正确的银行卡号';
                }
            }
            ,smscode:function(val,item)
            {
                if(!val)
                {
                    return '验证码不能为空';
                }
            }
        });
        form.on('submit(formDemo)', function (data) {
            $.ajax({
                url: 'user.php?act=act_edit_profile',
                data: data.field,
                dataType:'JSON',
                type:"POST",
                success: function (data) {
                    console.log(data)
                    if(data.error==0){
                        layer.msg("保存成功")
                    }else{
                        layer.msg(data.content)
                    }

                }

            });
        });

    });



</script>


<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>
