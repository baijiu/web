<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8" />
<title>公告中心 </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/user_main.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>
<body style="background-color: #fff">
<header id="header">
    <div class="header_l header_return"> <a class="ico_10" href="user.php"> 返回 </a> </div>
    <h1  style="background-color:#f1f1f1"> 公告中心 </h1>
</header>


<div class="nr" style="margin: 0">
    <?php $_from = $this->_var['article']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'article_0_20817200_1593520736');if (count($_from)):
    foreach ($_from AS $this->_var['article_0_20817200_1593520736']):
?>
        <a href="article.php?id=<?php echo $this->_var['article_0_20817200_1593520736']['article_id']; ?>">
            <div class="nr_ggzx">
                <p><?php echo $this->_var['article_0_20817200_1593520736']['title']; ?> <i class="layui-icon layui-icon-right" style="float: right"></i></p>
            </div>
        </a>

        <?php endforeach; else: ?>
        <li style="text-align:center; color:#999;margin-top: 10px">暂无公告</li>
    <?php endif; unset($_from); ?><?php $this->pop_vars();; ?>


    <div class="qcfd" style="height: 3px"></div>
</div>

<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>
