
<?php if ($this->_var['full_page']): ?>
<?php echo $this->fetch('pageheader.htm'); ?>
<?php echo $this->smarty_insert_scripts(array('files'=>'../js/utils.js,listtable.js')); ?>
<div class="form-div">
  <form action="javascript:search()" name="searchForm" >
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />
    项目名称<input type="text" name="keyword" id="keyword" />
    <input type="submit" value="搜索" class="button" />
  </form>
</div>

<form method="POST" action="product.php?act=batch_remove" name="listForm">

<div class="list-div" id="listDiv">
<?php endif; ?>

<table cellspacing='1' cellpadding='3' id='list-table'>
  <tr>
    <th>
      <a href="javascript:listTable.sort('id'); ">编号</a>
    </th>
    <th>项目名称</th>
    <th>日化率</th>
    <th><a href="javascript:listTable.sort('goods_price'); ">起投金额</a></th>
    <th><a href="javascript:listTable.sort('total_amount'); ">项目规模</a></th>
    <th><a href="javascript:listTable.sort('add_time'); ">添加时间</a></th>
    <th>上架</th>
    <th><?php echo $this->_var['lang']['handler']; ?></th>
  </tr>
  <?php $_from = $this->_var['goods_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'list');if (count($_from)):
    foreach ($_from AS $this->_var['list']):
?>
  <tr>
    <td  align="center"><?php echo $this->_var['list']['id']; ?></td>
    <td  align="center"><?php echo htmlspecialchars($this->_var['list']['goods_name']); ?></td>
    <td  align="center"><?php echo $this->_var['list']['daily_rate']; ?>%</td>
    <td align="center"><span><?php echo $this->_var['list']['goods_price']; ?></span></td>
    <td align="center"><span><?php echo $this->_var['list']['total_amount']; ?></span></td>
    <td align="center"><span><?php echo $this->_var['list']['date']; ?></span></td>
    <td align="center"><img src="images/<?php if ($this->_var['list']['is_on_sale']): ?>yes<?php else: ?>no<?php endif; ?>.gif" onclick="listTable.toggle(this, 'toggle_on_sale', <?php echo $this->_var['list']['id']; ?>)" /></td>

    <td align="center" nowrap="true"><span>
      <a href="product.php?act=edit&id=<?php echo $this->_var['list']['id']; ?>" title="<?php echo $this->_var['lang']['edit']; ?>"><img src="images/icon_edit.gif" border="0" height="16" width="16" /></a>&nbsp;
      <a href="javascript:;" onclick="removeproduct(<?php echo $this->_var['list']['id']; ?>)" title="<?php echo $this->_var['lang']['remove']; ?>"><img src="images/icon_drop.gif" border="0" height="16" width="16"></a></span>
    </td>
   </tr>
   <?php endforeach; else: ?>
    <tr><td class="no-records" colspan="7">没有项目</td></tr>
  <?php endif; unset($_from); ?><?php $this->pop_vars();; ?>
  <tr>&nbsp;
    <td align="right" nowrap="true" colspan="7"><?php echo $this->fetch('page.htm'); ?></td>
  </tr>
</table>

<?php if ($this->_var['full_page']): ?>
</div>

<div>


</div>
</form>
<!-- end cat list -->
<script type="text/javascript" language="JavaScript">
  listTable.recordCount = <?php echo $this->_var['record_count']; ?>;
  listTable.pageCount = <?php echo $this->_var['page_count']; ?>;

  <?php $_from = $this->_var['filter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
  listTable.filter.<?php echo $this->_var['key']; ?> = '<?php echo $this->_var['item']; ?>';
  <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
  
    onload = function()
    {

      // 开始检查订单
      startCheckOrder();
    }
 function search()
 {
    listTable.filter.keyword = Utils.trim(document.forms['searchForm'].elements['keyword'].value);

    listTable.filter.page = 1;
    listTable.loadList();
 }
    function removeproduct(id)
    {
      if(confirm("确定要删除该项目吗？")){
        window.location.href="product.php?act=remove&id="+id;
      }

    }

 
</script>
<?php echo $this->fetch('pagefooter.htm'); ?>
<?php endif; ?>
