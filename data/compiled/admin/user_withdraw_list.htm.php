<?php if ($this->_var['full_page']): ?>
<?php echo $this->fetch('pageheader.htm'); ?>
<?php echo $this->smarty_insert_scripts(array('files'=>'../js/utils.js,listtable.js')); ?>

<div class="form-div">
  <form action="javascript:searchUser()" name="searchForm">
    <img src="images/icon_search.gif" width="26" height="22" border="0" alt="SEARCH" />

    <input type="text" name="keyword" placeholder="会员账号"/>
    &nbsp;&nbsp;审核状态:<select name="state">
    <option value="">全部</option>
    <option value="0">申请中</option>
    <option value="1">已通过</option>
    <option value="2">取消</option>
  </select>

    开始时间<input type="date" name="createtime1"/>
    结束时间<input type="date" name="createtime2"/>
	&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="<?php echo $this->_var['lang']['button_search']; ?>"/>
  </form>
</div>
<form method="POST" action="" name="listForm" onsubmit="return confirm_bath()">

<div class="list-div" id="listDiv">
<?php endif; ?>
<style>
.trtd{text-align:center}
.ziti{font-size:20px;font-weight:bold}
.ziti{color:#FF0000}
.trzb{border-bottom:1px solid #B2B2B2}
</style>


<!--用户列表部分-->
<table cellpadding="3" cellspacing="1" class="zb">
  <tr>
    <th width="5%">编号</th>
    <th>账号</th>
	<th><a href="javascript:listTable.sort('user_id'); ">会员ID</a></th>
    <th><a href="javascript:listTable.sort('amount'); ">金额</a></th>
      <th><a href="javascript:listTable.sort('lv'); ">手续费</a></th>
      <th><a href="javascript:listTable.sort('amountrun'); ">实际到账</a></th>
    <th>开户行</th>
    <th>姓名</th>
    <th>卡号</th>

    <th><a href="javascript:listTable.sort('state'); ">状态</a></th>
	<th><a href="javascript:listTable.sort('createtime'); ">申请时间</a></th>
	<th><a href="javascript:listTable.sort('truntime'); ">审核时间</a></th>

    <th>类型</th>
    <th width="5%">操作</th>
  <tr>
  <?php $_from = $this->_var['transaction_log']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'in');if (count($_from)):
    foreach ($_from AS $this->_var['in']):
?>
  <tr align="center">
    <td class="trzb"><?php echo $this->_var['in']['id']; ?></td>
	<td class="trzb" align="center"><?php echo $this->_var['in']['user_name']; ?></td>
    <td class="trzb"><?php echo $this->_var['in']['user_id']; ?></td>
    <td class="trzb"><?php echo $this->_var['in']['amount']; ?></td>
	<td class="trzb"><?php echo $this->_var['in']['lv']; ?></td>
    <td class="trzb"><?php echo $this->_var['in']['amountrun']; ?></td>
  <td class="trzb"><?php echo $this->_var['in']['bankname']; ?><br><?php echo $this->_var['in']['bank_address']; ?>

  </td>
  <td class="trzb"><?php echo $this->_var['in']['accountname']; ?></td>
  <td class="trzb"><?php echo $this->_var['in']['accountnumber']; ?></td>
    <td class="trzb">
      <?php if ($this->_var['in']['state'] == 0): ?><span style="color:red">待审核</span>
      <?php elseif ($this->_var['in']['state'] == 1): ?><span style="color:green">已通过</span>
      <?php elseif ($this->_var['in']['state'] == 2): ?><span style="color:#0000FF">已取消</span>
      <?php endif; ?></td>
    <td class="trzb"><?php echo $this->_var['in']['createtime']; ?></td>
	<td class="trzb"><?php echo $this->_var['in']['truntime']; ?></td>
  <td class="trzb">
    <?php if ($this->_var['in']['ti_type'] == 0): ?><span >余额</span>
    <?php elseif ($this->_var['in']['ti_type'] == 1): ?><span >佣金</span>
    <?php endif; ?>
  </td>
  <td class="trzb">

    <?php if ($this->_var['in']['state'] == 0): ?>
    <a href="javascript:confirm_redirect('确定要审核通过吗？', 'user_withdraw.php?act=check_ok&id=<?php echo $this->_var['in']['id']; ?>')" title="审核通过"><img src="images/tongguo.png" border="0" height="20" width="20" /></a>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <a href="javascript:confirm_redirect('确定要取消提现吗？', 'user_withdraw.php?act=check_no&id=<?php echo $this->_var['in']['id']; ?>')" title="取消提现"><img src="images/quxiao.png" border="0" height="20" width="20" /></a>
    <?php endif; ?>

  </td>

  </tr>
  <?php endforeach; else: ?>
  <tr><td class="no-records" colspan="13"><?php echo $this->_var['lang']['no_records']; ?></td></tr>
  <?php endif; unset($_from); ?><?php $this->pop_vars();; ?>
  <tr>

      <td align="right" nowrap="true" colspan="13">
      <?php echo $this->fetch('page.htm'); ?>
      </td>
  </tr>
</table>

<?php if ($this->_var['full_page']): ?>
</div>

</form>
<script type="text/javascript" language="JavaScript">
<!--
listTable.recordCount = <?php echo $this->_var['record_count']; ?>;
listTable.pageCount = <?php echo $this->_var['page_count']; ?>;

<?php $_from = $this->_var['filter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
listTable.filter.<?php echo $this->_var['key']; ?> = '<?php echo $this->_var['item']; ?>';
<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>


onload = function()
{
    document.forms['searchForm'].elements['keyword'].focus();
    // 开始检查订单
    startCheckOrder();
}

/**
 * 搜索用户
 */
function searchUser()
{
    listTable.filter['keywords'] = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
	
	listTable.filter['createtime1'] = Utils.trim(document.forms['searchForm'].elements['createtime1'].value);
	listTable.filter['createtime2'] = Utils.trim(document.forms['searchForm'].elements['createtime2'].value);
    listTable.filter['state'] = Utils.trim(document.forms['searchForm'].elements['state'].value);
    listTable.filter['page'] = 1;
    listTable.loadList();
}

//-->
</script>

<?php echo $this->fetch('pagefooter.htm'); ?>
<?php endif; ?>