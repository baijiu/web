
<?php echo $this->fetch('pageheader.htm'); ?>
<?php echo $this->smarty_insert_scripts(array('files'=>'../data/static/js/utils.js,./js/selectzone.js,./js/colorselector.js')); ?>

<div class="tab-div">
  <div id="tabbar-div">
    <p>
      <span class="tab-front" id="general-tab">通用信息</span>
    <span class="tab-back" id="detail-tab">产品简介</span>
    </p>
  </div>
  <div id="tabbody-div">
    <form  action="product.php" method="post" enctype="multipart/form-data" name="theForm" onsubmit="return validate();">
    
    <table width="90%" id="general-table">
      <tr>
        <td class="narrow-label">项目名称</td>
        <td><input type="text" name="goods_name" size ="40" maxlength="160" value="<?php echo htmlspecialchars($this->_var['stockgoods']['goods_name']); ?>" /><?php echo $this->_var['lang']['require_field']; ?></td>
      </tr>
      <tr>
        <td class="narrow-label">项目周期</td>
        <td><input type="text" name="cycle" maxlength="60" value="<?php echo $this->_var['stockgoods']['cycle']; ?>" />&nbsp;天/小时</td>
      </tr>

      <tr>
        <td class="narrow-label">起投金额</td>
        <td><input type="text" name="goods_price" maxlength="60" value="<?php echo $this->_var['stockgoods']['goods_price']; ?>" />&nbsp;元</td>
      </tr>

      <tr>
        <td class="narrow-label">项目规模</td>
        <td><input type="text" name="total_amount" maxlength="60" value="<?php echo $this->_var['stockgoods']['total_amount']; ?>" />&nbsp;元</td>
      </tr>


      <tr>
        <td class="narrow-label">日/时化率</td>
        <td><input type="text" name="daily_rate" maxlength="60" value="<?php echo $this->_var['stockgoods']['daily_rate']; ?>" />&nbsp;%</td>
      </tr>

      <tr>
        <td class="narrow-label">可投次数</td>
        <td><input type="text" name="ketoucishu" maxlength="60" value="<?php echo $this->_var['stockgoods']['ketoucishu']; ?>" />&nbsp;次</td>
      </tr>


      <tr>
        <td class="label">还款方式:</td>
        <td>
          <select name="type">


            <option value="0" <?php if ($this->_var['stockgoods']['type'] == 0): ?> selected="selected" <?php endif; ?>>每日还息，到期还本</option>
            <option value="1" <?php if ($this->_var['stockgoods']['type'] == 1): ?> selected="selected" <?php endif; ?>>等额本息，每日返还</option>
            <option value="2" <?php if ($this->_var['stockgoods']['type'] == 2): ?> selected="selected" <?php endif; ?>>到期返本返息</option>

            <option value="3" <?php if ($this->_var['stockgoods']['type'] == 3): ?> selected="selected" <?php endif; ?>>时时还息，到期还本</option>


          </select>
        </td>
      </tr>
      <tr>
        <td class="narrow-label">项目类型 </td>
        <td>
          <select name="cat_id">
            <option value="0">请选择</option>
            <?php echo $this->html_options(array('options'=>$this->_var['piao'],'selected'=>$this->_var['status_id'])); ?>
          </select>
         </td>
      </tr>


      <tr>
        <td class="label">推荐指数:</td>
        <td>
          <select name="tuijianzhishu">


            <option value="1" <?php if ($this->_var['stockgoods']['tuijianzhishu'] == 1): ?> selected="selected" <?php endif; ?>>★</option>
            <option value="2" <?php if ($this->_var['stockgoods']['tuijianzhishu'] == 2): ?> selected="selected" <?php endif; ?>>★★</option>
            <option value="3" <?php if ($this->_var['stockgoods']['tuijianzhishu'] == 3): ?> selected="selected" <?php endif; ?>>★★★</option>
            <option value="4" <?php if ($this->_var['stockgoods']['tuijianzhishu'] == 4): ?> selected="selected" <?php endif; ?>>★★★★</option>
            <option value="5" <?php if ($this->_var['stockgoods']['tuijianzhishu'] == 5): ?> selected="selected" <?php endif; ?>>★★★★★</option>

          </select>
        </td>
      </tr>


        <tr>
          <td class="narrow-label">已投资金额</td>
          <td><input type="text" name="yitou_amount" maxlength="60" value="<?php echo $this->_var['stockgoods']['yitou_amount']; ?>" />&nbsp;元</td>
        </tr>






      <tr>
        <td class="narrow-label">上市时间</td>
        <td><input type="text" name="shangyingtime" maxlength="60" value="<?php echo $this->_var['stockgoods']['shangyingtime']; ?>" /></td>

      </tr>

 <tr>
        <td class="narrow-label">缩略图</td>
        <td><input type="file" name="file">
          </td>
      </tr>
     

     
    </table>

    <table width="90%" id="detail-table" style="display:none">
     <tr><td><?php echo $this->_var['FCKeditor']; ?></td></tr>
    </table>

    
    <div class="button-div">
      <input type="hidden" name="act" value="<?php echo $this->_var['form_action']; ?>" />
      <input type="hidden" name="old_title" value="<?php echo $this->_var['stockgoods']['goods_name']; ?>"/>
      <input type="hidden" name="id" value="<?php echo $this->_var['stockgoods']['id']; ?>" />
      <input type="submit" value="<?php echo $this->_var['lang']['button_submit']; ?>" class="button"  />
      <input type="reset" value="<?php echo $this->_var['lang']['button_reset']; ?>" class="button" />
    </div>
    </form>
  </div>

</div>
<?php echo $this->smarty_insert_scripts(array('files'=>'./js/validator.js,./js/tab.js')); ?>
<script language="JavaScript">


function validate()
{

  var validator = new Validator('theForm');
  validator.required('goods_name', '项目名称不能为空');
  validator.required('goods_price', '起投金额不能为空');
  validator.required('cycle', '项目周期不能为空');
  validator.required('total_amount', '项目规模不能为空');
  validator.required('daily_rate', '日化率不能为空');
  validator.required('ketoucishu', '可投次数不能为空');


  validator.required('shangyingtime', '上映时间不能为空');



  return validator.passed();
}



document.getElementById("tabbar-div").onmouseover = function(e)
{
    var obj = Utils.srcElement(e);

    if (obj.className == "tab-back")
    {
        obj.className = "tab-hover";
    }
}

document.getElementById("tabbar-div").onmouseout = function(e)
{
    var obj = Utils.srcElement(e);

    if (obj.className == "tab-hover")
    {
        obj.className = "tab-back";
    }
}

document.getElementById("tabbar-div").onclick = function(e)
{
    var obj = Utils.srcElement(e);

    if (obj.className == "tab-front")
    {
        return;
    }
    else
    {
        objTable = obj.id.substring(0, obj.id.lastIndexOf("-")) + "-table";

        var tables = document.getElementsByTagName("table");
        var spans  = document.getElementsByTagName("span");

        for (i = 0; i < tables.length; i++)
        {
            if (tables[i].id == objTable)
            {
                tables[i].style.display = (Browser.isIE) ? "block" : "table";
            }
            else
            {
                tables[i].style.display = "none";
            }
        }
        for (i = 0; spans.length; i++)
        {
            if (spans[i].className == "tab-front")
            {
                spans[i].className = "tab-back";
                obj.className = "tab-front";
                break;
            }
        }
    }
}

</script>
<?php echo $this->fetch('pagefooter.htm'); ?>