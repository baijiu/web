<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no">
	<meta name="format-detection" content="telephone=no" />
	<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css?id=10" rel="stylesheet" type="text/css" />
	<title>支付方式</title>
    <style>
        body,div,ul,li,h5,h4,h3,h2,h1,p{margin:0; padding:0;}
        a,li,p{font-size:12px;}
        a{text-decoration:none;}
        li{list-style:none;}
        body,th,td,input,select,textarea,button {line-height:1 ;font-family:"微软雅黑", "黑体","宋体";}
        input,select,textarea,button {vertical-align:middle;outline:none;}
        body{background-color:#eeeeee;}
        input{font-size:14px;  padding:8px 0;}
        button,input{background:none; border:none;}

        .haeder{height:44px; background:#20282c; position:relative;}
        .haeder-tittle{text-align:center; height:44px; line-height:44px; color:#000;font-weight:bold}
        .haeder-back-btn{position:absolute; left:1%; width:48px; height:44px; background:url(themes/miqinew/images/back.png) no-repeat center center; background-size:50% 50%;}
        .haeder-finish-btn{position:absolute; right:1%; top:16px; width:48px; height:44px; color:#fff; font-weight:bold; font-size:14px;}
        .haeder-back-btn:hover{}
        .bill-rate-container,.roll-out-container{margin-top:12px; color:#8e8e93; background:#fff;}
        .roll-out-container li{line-height:44px; height:44px; margin: 0 20px; border-bottom:1px #eeeeee solid; font-size:14px; color:#333;}
        .roll-out-container{border-top:1px #eeeeee solid;border-bottom:1px #eeeeee solid;}
        .bill-money{float:right; margin-right:30px;}
        .roll-income-input{margin-left:13%; font-weight:bold;}
        .bill-sub-btn{width:100%; line-height:44px; font-size:14px; font-weight:bold; text-align:center; color:#FFF; background:#64ba55; margin:30px auto;}
        .bill-sub-btn:hover{background:#569f49;}
        .firm-payment{border-top:1px #EEEEEE solid; display:block; color:#64ba55; line-height:40px; position:fixed; bottom:0; text-align:center; background:#fff; width:100%; font-size:16px;}
        .firm-payment hover{color:#569f49;}
        .bill-btn-container{width:90%; padding:0 5%; overflow-x:hidden;}
        .bill-sub-btn,.sub-capion1,.verification-code,.selected{border-radius:4px;-moz-border-radius:4px;-ms-border-radius:4px;-webkit-border-radius:4px;-o-border-radius:4px;}

    </style>
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
	<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">


        function ok(){
            var type = $("input[name='gatewayType']:checked").val();
			window.location.href = "product.php?order_sn=<?php echo $this->_var['order_sn']; ?>&act=pay_" + type;
        }
	
	</script>
</head>
<body>

<header id="header">
	<div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1);"> 返回 </a> </div>
	<h1> 请选择支付方式 </h1>

</header>

	<div class="roll-out-container">
		<ul>

			<li>
				<span class=""><input checked="checked" type="radio" name="gatewayType" value="balance"  style="vertical-align: middle;width:20px;height: 20px;"/></span>
				<span>
                    余额支付
				</span>
			</li>
			<!--<li>
				<span class=""><input type="radio" name="gatewayType" value="WXZF" checked="checked" style="vertical-align: middle;width:20px;height: 20px;"/></span>
				<span>
                      <img src="/images/WePayLogo.png" style="height:30px;vertical-align: middle;"/>
				</span>
			</li>
       
            <li>
				<span class=""><input type="radio" name="gatewayType" value="ALIPAY"  style="vertical-align: middle;width:20px;height: 20px;"/></span>
				<span>
                      <img src="/images/alipay_logo.png" style="height:30px;vertical-align: middle;"/>
				</span>
				
			</li>-->
  
		</ul>
		<button id="submit" type="button"  onclick="ok();" class="bill-sub-btn">确&nbsp;定</button>
	</div>

<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>