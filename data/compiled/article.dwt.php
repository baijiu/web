<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8" />
<title><?php echo $this->_var['page_title']; ?> </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>

<body style="background: #fff" >
<header id="header" style="background: #f1f1f1" >
  <div class="header_l header_return"> <a class="ico_10"  href="javascript:history.back(-1)"> 返回 </a> </div>
  <h1  style="background: #f1f1f1" > 文章详情 </h1>
</header>

<section class="wrap">
  <div class="art_content ">
    <h2><span><?php echo $this->_var['article']['title']; ?></span> <?php echo $this->_var['article']['add_time']; ?></h2>
    <div>
    
    	<?php if ($this->_var['article']['article_id'] == 87): ?>
      
        
        <video controls autoplay name="media" style="width:100%"><source src="/images/upload/media/20200420/20200420151139_49078.mp4" type="video/mp4" ></video>
        
        <?php endif; ?>
      <p> <?php echo $this->_var['article']['content']; ?> </p>
    </div>
  </div>
</section>
<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>
