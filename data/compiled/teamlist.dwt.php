<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
    <meta charset="utf-8" />
    <title>我的团队 </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="format-detection" content="telephone=no" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/user_main.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery.more.js"></script>
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>

</head>

<style>
    .ico_10 {
        background: url(themes/miqinew/images/ico_10.png) no-repeat 30% 50%;
        -webkit-background-size: 1.2rem 1.2rem;
        -moz-background-size: 1.2rem 1.2rem;
        background-size: 1.2rem 1.2rem;
    }
    .jiazai{
        color: #a7a7a7;
        height: 40px;
        text-align: center;
        line-height: 40px;
    }
    .more_loader_spinner {
        text-align: center;
    }
</style>
<body>
<header id="header">
    <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1)"> 返回 </a> </div>
    <h1> 我的团队 </h1>
</header>
<?php $_from = $this->_var['team_all']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'info');if (count($_from)):
    foreach ($_from AS $this->_var['info']):
?>

<div class="nr" style="padding: 6px;    margin-top: 1px;">
    <div class="touxiang2">
        <?php if ($this->_var['info']['head_img'] != ''): ?><img width="65px" src="<?php echo $this->_var['info']['head_img']; ?>"><?php else: ?><img width="65px" src="<?php echo $this->_var['ectouch_themes']; ?>/images/get_avatar.png"><?php endif; ?>
    </div>
    <div style="padding: 0 10px 0 78px">
        <div><?php echo $this->_var['info']['user_name']; ?></div>
        <div class="jinerticheng"><?php echo $this->_var['info']['teamname']; ?></div>
        <div class="zt_hui01 zt_13px" style="margin-top: 18px;">总投资：<span style="color: red;">￥<?php echo $this->_var['info']['zongchongzhi']; ?></span></div>
        <div class="rtsj zt_hui01 zt_13pxv jinerticheng2">总业绩：<span style="color: red;">￥<?php echo $this->_var['info']['zongyeji']; ?></span></div>

    </div>
    <div class="qcfd"></div>
</div>
    <?php endforeach; else: ?>
    <li style="text-align:center; color:#999;margin-top: 10px">暂无成员</li>
<?php endif; unset($_from); ?><?php $this->pop_vars();; ?>





<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>
