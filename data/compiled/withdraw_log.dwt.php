<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
    <meta charset="utf-8" />
    <title>提现记录 </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="format-detection" content="telephone=no" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/user_main.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>
<body>
<header id="header">
    <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1)"> 返回 </a> </div>
    <h1> 提现记录 </h1>
</header>
<?php $_from = $this->_var['user_account']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'list');if (count($_from)):
    foreach ($_from AS $this->_var['list']):
?>
<div class="nr" style="margin: 0px;height: 135px;margin:10px 0;padding: 8px">

    <div style="border-bottom: 1px #c3c3c3 solid;padding: 5px">
        <p><span class="ztys_hong01 zt_jc" style="    color: #E71F19;font-weight: bold;    font-size: 16px;">￥<?php echo $this->_var['list']['amount']; ?></span><span class="zt_hui02" style="float: right">
                <?php if ($this->_var['list']['ti_type'] == 0): ?>余额提现
                <?php elseif ($this->_var['list']['ti_type'] == 1): ?>佣金提现
                <?php endif; ?>

            </span></p>
    </div>
    <div>
        <p style="margin-top: 5px">手续费：￥:<?php echo $this->_var['list']['lv']; ?></p>
        <p style="margin-top: 5px">实际到账：￥<?php echo $this->_var['list']['amountrun']; ?></p>
        <?php if ($this->_var['list']['remark']): ?>
        <p style="margin-top: 5px">管理员备注：<?php echo $this->_var['list']['remark']; ?></p>
        <?php else: ?>
            <p style="margin-top: 5px">管理员备注：N/A</p>
        <?php endif; ?>
        <p style="margin-top: 5px">提现时间：<?php echo $this->_var['list']['createtime']; ?></p>
        <input type="button" value="<?php echo $this->_var['list']['zt']; ?>" class="botton5">
    </div>
</div>
<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
<?php if ($this->_var['see'] == 1): ?>
<?php echo $this->fetch('library/pages.lbi'); ?>
<?php endif; ?>
<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>
