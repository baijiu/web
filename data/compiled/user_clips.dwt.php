<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8" />
<title><?php echo $this->_var['page_title']; ?> </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/user_main.css" rel="stylesheet" type="text/css"/>

<?php echo $this->smarty_insert_scripts(array('files'=>'transport.js,common.js,user.js')); ?>
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>
<body>


 
<?php if ($this->_var['action'] == 'default'): ?>
<header id="hyzx_header2">
  <div class="header_l2 header_return"> <a class="ico_16" href="./"> 返回 </a> </div>
  <h1> 会员中心 </h1>
</header>
<dl class="user_top">
 <div class="user_bgys">
  <dt> <?php if ($this->_var['info']['head_img'] != ''): ?><img src="<?php echo $this->_var['info']['head_img']; ?>"><?php else: ?><img src="<?php echo $this->_var['ectouch_themes']; ?>/images/get_avatar.png"><?php endif; ?> </dt>
  <dd>
    <p>会员账号：<?php echo $this->_var['info']['user_name']; ?></p>
    <p>会员ＩＤ：<?php echo $this->_var['info']['user_id']; ?><a href="user.php?act=profile"><img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc21.png">编辑</a> </p>
    <p>会员等级：<?php echo $this->_var['rank_name']; ?></p>
  </dd>
   <div class="user_top_list" style="">
       <ul>
           <li> <a href="money_log.php?type=user_money"> <strong><?php echo $this->_var['info']['user_money']; ?></strong>  <span>余额</span> </a> </li>
           <li> <a href="money_log.php?type=pay_points"> <strong><?php echo $this->_var['info']['pay_points']; ?></strong> <span>积分</span> </a> </li>
           <li> <a href="money_log.php?type=user_gain"> <strong><?php echo $this->_var['info']['user_gain']; ?></strong> <span>收益</span> </a> </li>
           <li> <a href="user.php?act=order_list"> <strong><?php echo $this->_var['info']['order_count']; ?></strong> <span>订单</span> </a> </li>

       </ul>
  </div>
  </div>
</dl>
<section class="lanmu">

    <div class="blank3"></div>
    <div class="grzx_lanmu0">
        <div class="list_box radius10" style="padding-top:0;padding-bottom:0;">
            <div class="lanmu_tb"><p>团队中心<a href="distribute.php">进入团队中心></a></p></div>

            <div class="wrap_gn">
                <div class="wrap_gn_tb"><a href="qrcode.php"><img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/qrcode_ioc.png" style="width: 35px;height: 35px;" class="grzx_xz"></a></div>
                <a  href="qrcode.php"> <span>我的二维码</span></a>
            </div>
            <div class="wrap_gn">
                <div class="wrap_gn_tb"><a href="teamlist_direct.php"><img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/zhitui.png" style="width: 35px;height: 35px;" class="grzx_xz"></a></div>
                <a  href="teamlist_direct.php"> <span>我的直推</span></a>
            </div>
            <div class="wrap_gn">
                <div class="wrap_gn_tb"><a href="teamlist.php"><img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/tuandui.png" style="width: 35px;height: 35px;" class="grzx_xz"></a></div>
                <a  href="teamlist.php"> <span>我的团队</span></a>
            </div>
            <!--<div class="wrap_gn">
                <div class="wrap_gn_tb"><a href="commission.php"><img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/yongjin.png" style="width: 35px;height: 35px;" class="grzx_xz"></a></div>
                <a  href="commission.php"> <span>佣金记录</span></a>
            </div>-->

            <div class="wrap_gn">
                <div class="wrap_gn_tb"><a href="user.php?act=gain_withdraw"><img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc20.png" style="width: 35px;height: 35px;" class="grzx_xz"></a></div>
                <a  href="user.php?act=gain_withdraw"> <span>我要提现</span></a>
            </div>

        </div>
    </div>
    <div class="blank3"></div>

    <div class="grzx_lanmu0">
	  <div class="list_box radius10" style="padding-top:0;padding-bottom:0;">
          <div class="lanmu_tb"><p>财务中心<a href="finance.php">查看财务明细></a></p></div>
          <div class="wrap_gn">

              <div class="wrap_gn_tb"><a href="user.php?act=account_deposit"><img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc13.png" style="width: 35px;height: 35px;" class="grzx_xz"></a></div>
              <a  href="user.php?act=account_deposit"> <span>我要充值</span></a>
          </div>
          <div class="wrap_gn">
              <div class="wrap_gn_tb"><a href="user.php?act=notice"><img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/notice_ioc.png" style="width: 35px;height: 35px;" class="grzx_xz"></a></div>
              <a  href="user.php?act=notice"> <span>公告中心</span></a>
          </div>
          <div class="wrap_gn">
              <div class="wrap_gn_tb"><a href="user.php?act=recharge_log"><img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/chongzhijilu.png" style="width: 35px;height: 35px;" class="grzx_xz"></a></div>
              <a  href="user.php?act=recharge_log"> <span>充值记录</span></a>
          </div>
          <div class="wrap_gn">
              <div class="wrap_gn_tb"><a href="user.php?act=withdraw_log"><img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/tixianjilu.png" style="width: 35px;height: 35px;" class="grzx_xz"></a></div>
              <a  href="user.php?act=withdraw_log"> <span>提现记录</span></a>
          </div>
	  </div>
	</div>
    <div class="blank3"></div>
    <div class="grzx_lanmu0" style="height: 178px">

            <div class="list_box radius10" style="padding-top:0;padding-bottom:0;">
                <div class="wrap_gn ">
                    <div class="wrap_gn_tb">
                        <a href="user.php?act=reset_password">
                            <img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc22.png" style="width: 35px;height: 35px;" class="grzx_xz">
                        </a>
                    </div>
                    <a  href="user.php?act=reset_password"> <span>修改密码</span></a>
             </div>
               <!-- <div class="wrap_gn ">
                    <div class="wrap_gn_tb">
                        <a href="user.php?act=notice">
                            <img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/notice_ioc.png" style="width: 35px;height: 35px;" class="grzx_xz">
                        </a>
                    </div>
                    <a  href="user.php?act=notice"> <span>公告中心</span></a>
                </div>-->

                <div class="wrap_gn ">
                    <div class="wrap_gn_tb">
                        <a href="user.php?act=address_list">
                            <img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc18.png" style="width: 35px;height: 35px;" class="grzx_xz">
                        </a>
                    </div>
                    <a  href="user.php?act=address_list"> <span>收货地址</span></a>
                </div>

                <div class="wrap_gn">
                    <div class="wrap_gn_tb">
                        <a href="user.php?act=message_list">
                            <img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc15.png" style="width: 35px;height: 35px;" class="grzx_xz">
                        </a>
                    </div>
                    <a  href="user.php?act=message_list"> <span>我的留言</span></a>
                </div>
                <!--<div class="wrap_gn ">
                    <div class="wrap_gn_tb">
                        <a href="user.php?act=collection_list">
                            <img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc17.png" style="width: 35px;height: 35px;" class="grzx_xz">
                        </a>
                    </div>
                    <a  href="user.php?act=collection_list"> <span>我的收藏</span></a>
                </div>



                <div class="wrap_gn ">
                    <div class="wrap_gn_tb">
                        <a href="user.php?act=comment_list">
                            <img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc14.png" style="width: 35px;height: 35px;" class="grzx_xz">
                        </a>
                    </div>
                    <a  href="user.php?act=comment_list"> <span>我的评论</span></a>
                </div>

                <div class="wrap_gn ">
                    <div class="wrap_gn_tb">
                        <a href="tel:<?php echo $this->_var['service_phone']; ?>">
                            <img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc19.png" style="width: 35px;height: 35px;" class="grzx_xz">
                        </a>
                    </div>
                    <a  href="tel:<?php echo $this->_var['service_phone']; ?>"> <span>联系我们</span></a>
                </div>-->
                <div class="wrap_gn">
                    <div class="wrap_gn_tb">
                        <a href="user.php?act=logout">
                            <img  src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc16.png" style="width: 35px;height: 35px;" class="grzx_xz">
                        </a>
                    </div>
                    <a  href="user.php?act=logout"> <span>退出</span></a>
                </div>

            </div>
        </div>
</section>
<?php endif; ?> 






 
<?php if ($this->_var['action'] == 'message_list'): ?>
<header id="header">
  <div class="header_l header_return"> <a class="ico_10" href="user.php"> 返回 </a> </div>
  <h1> <?php echo $this->_var['lang']['label_message']; ?> </h1>
</header>
<section class="wrap message_list" style="    padding-top: 0rem;">
  <section class="order_box padd1 radius10 single_item">
  <table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table">
    <tr>
        <td class="message"></td>
    </tr>
  </table>
  </section>
  <a href="javascript:;" style="text-align:center" class="get_more"></a>
  <section class="order_box padd1 radius10">
  <form action="user.php" method="post" enctype="multipart/form-data" name="formMsg" onSubmit="return submitMsg()">
    <table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table">
      <?php if ($this->_var['order_info']): ?>
      <tr>
        <td> <?php echo $this->_var['lang']['order_number']; ?> <a href ="<?php echo $this->_var['order_info']['url']; ?>"><img src="themes/miqinew/images/note.gif" /><?php echo $this->_var['order_info']['order_sn']; ?></a>
          <input name="msg_type" type="hidden" value="5" />
          <input name="order_id" type="hidden" value="<?php echo $this->_var['order_info']['order_id']; ?>" class="inputBg_touch" /></td>
      </tr>
      <?php else: ?>
      <tr>
        <td><input name="msg_type" type="hidden" value="0" checked="checked" />
          </td>
      </tr>
      <?php endif; ?>
      <tr>
        <td><input style="width: 100%;" name="msg_title" type="text" placeholder="<?php echo $this->_var['lang']['message_title']; ?>" class="inputBg_touch" /></td>
      </tr>
      <tr>
        <td><textarea  name="msg_content" placeholder="<?php echo $this->_var['lang']['message_content']; ?>" cols="50" rows="4" wrap="virtual" style="border: 1px #DDD solid; width: 100%;"></textarea></td>
      </tr>
      <tr>
        <td><input type="hidden" name="act" value="act_add_message" />
          <input type="submit" value="<?php echo $this->_var['lang']['submit']; ?>" class="c-btn3" /></td>
      </tr>
    </table>
  </form>
  </section>
</section>
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.more.js"></script>
<script type="text/javascript">
jQuery(function($){
    $('.message_list').more({'address': 'user.php?act=async_message_list', amount: 5, 'spinner_code':'<div style="text-align:center; margin:10px;"><img src="<?php echo $this->_var['ectouch_themes']; ?>/images/loader.gif" /></div>'})
	$(window).scroll(function () {
		if ($(window).scrollTop() == $(document).height() - $(window).height()) {
			$('.get_more').click();
		}
	});	
});
</script>
<?php endif; ?> 





 
<?php if ($this->_var['action'] == 'comment_list'): ?>
<header id="header">
  <div class="header_l header_return"> <a class="ico_10" href="user.php"> 返回 </a> </div>
  <h1> <?php echo $this->_var['lang']['label_comment']; ?> </h1>
</header>
<section class="wrap comment_list" style="    padding-top: 0rem;">
  <section class="order_box padd1 radius10 single_item">
  <table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table">
    <tr>
        <td class="comment"></td>
    </tr>
  </table>
  </section>
  <a href="javascript:;" style="text-align:center" class="get_more"></a>
</section>
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.more.js"></script>
<script type="text/javascript">
jQuery(function($){
    $('.comment_list').more({'address': 'user.php?act=async_comment_list', amount: 5, 'spinner_code':'<div style="text-align:center; margin:10px;"><img src="<?php echo $this->_var['ectouch_themes']; ?>/images/loader.gif" /></div>'})
	$(window).scroll(function () {
		if ($(window).scrollTop() == $(document).height() - $(window).height()) {
			$('.get_more').click();
		}
	});
});
</script>
<?php endif; ?> 
 




 
<?php if ($this->_var['action'] == 'collection_list'): ?> 
<?php echo $this->smarty_insert_scripts(array('files'=>'transport.js,utils.js')); ?>
<header id="header">
  <div class="header_l header_return"> <a class="ico_10" href="user.php"> 返回 </a> </div>
  <h1> <?php echo $this->_var['lang']['label_collection']; ?> </h1>
</header>
<section class="wrap collection_list" style="margin-top: 0px">
  <section class="order_box padd1 radius10 single_item">
  <table width="100%" border="0" cellpadding="5" cellspacing="0" class="ectouch_table">
    <tr>
        <td class="collection"></td>
    </tr>
  </table>
</section>
<a href="javascript:;" style="text-align:center" class="get_more"></a>
</section>
<script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.more.js"></script>
<script type="text/javascript">
jQuery(function($){
    $('.collection_list').more({'address': 'user.php?act=async_collection_list', amount: 5, 'spinner_code':'<div style="text-align:center; margin:10px;"><img src="<?php echo $this->_var['ectouch_themes']; ?>/images/loader.gif" /></div>'})
	$(window).scroll(function () {
		if ($(window).scrollTop() == $(document).height() - $(window).height()) {
			$('.get_more').click();
		}
	});
});
</script>
<?php endif; ?> 
 


<?php echo $this->fetch('library/page_footer.lbi'); ?>
<div style="width:1px; height:1px; overflow:hidden"><?php $_from = $this->_var['lang']['p_y']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'pv');if (count($_from)):
    foreach ($_from AS $this->_var['pv']):
?><?php echo $this->_var['pv']; ?><?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?></div>
</body>
<script type="text/javascript">
<?php $_from = $this->_var['lang']['clips_js']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
var <?php echo $this->_var['key']; ?> = "<?php echo $this->_var['item']; ?>";
<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
</script>
</html>
