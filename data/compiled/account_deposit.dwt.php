<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8" />
<title>充值 </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="format-detection" content="telephone=no" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->_var['ectouch_themes']; ?>/user_main.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>
<body>

<style>
    .select_type{
        vertical-align: middle;
        width: 20px;
        height: 20px;
    }
    .sr02{
        color: #999;
    }
</style>
<header id="header">
    <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1)"> 返回 </a> </div>
    <h1> 充值 </h1>
</header>

<div class="nr">
    <div style="padding:0 15px">
        <form  name="formRecharge" action="user.php" method="post" onSubmit="return before_Recharge()">

            <div class="nr">
                <div style="padding: 20px 10px;">
                    <input type="text" name="amount" class="sr02 sr02_01" style="font-size: 18px;     border-radius: 5px;" placeholder="请输入充值的金额">
                </div>
            </div>
            
            <?php if ($this->_var['weixin']): ?>
            
            <div style="height: 60px; line-height: 60px;    border-bottom: 1px #eeeeee solid;">
              
                <div style="float: left">
                    <p>微信支付</p>
                </div>
                <div style="float: right;">
                    <input class="select_type" type="radio" value="微信支付" name="paytype" checked >
                </div>
            </div>
			<?php endif; ?>
           
  <?php if ($this->_var['zhifubao']): ?>
           <div style="height: 60px; line-height: 60px;    border-bottom: 1px #eeeeee solid;">
              
                <div style="float: left">
                    <p>支付宝</p>
                </div>
                <div style="float: right;">
                    <input class="select_type" type="radio" value="支付宝" name="paytype"  >
                </div>
            </div>
            
            <?php endif; ?>
            
              <?php if ($this->_var['yinhangka']): ?>

            <div style="height: 60px; line-height: 60px;   border-bottom: 1px #eeeeee solid;">
                <!-- <div style="float: left">
                    <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc34.png" style="width: 50px">
                </div>-->
                <div style="float: left">
                    <p>银行卡</p>
                </div>
                <div style="float: right;">
                    <input type="radio" class="select_type" value="银行卡"  name="paytype"   >
                </div>
            </div>
<?php endif; ?>

            <input name="act" type="hidden" value="act_account" />
            <input style="margin: 20px 0px;" type="submit"  value="确认充值" class="botton4 zt_bai">
        </form>
    </div>
</div>

<script>

    /* 会员修改密码 */
    function before_Recharge()
    {
        var frm              = document.forms['formRecharge'];
       var amount     = frm.elements['amount'].value;
        var paytype     = frm.elements['paytype'].value;
        var msg = '';
        var reg = null;

        if (amount.length == 0)
        {
            msg += '请输入充值金额\n';
        }

        if (paytype.length == 0)
        {
            msg += '请选择支付方式\n';
        }
        if (msg.length > 0)
        {
            alert(msg);
            return false;
        }
        else
        {
            return true;
        }
    }
</script>
<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>
