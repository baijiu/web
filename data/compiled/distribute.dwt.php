<!DOCTYPE html>
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
    <meta charset="utf-8" />
    <title>团队中心 </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="format-detection" content="telephone=no" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/images/touch-icon.png" rel="apple-touch-icon-precomposed" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/ectouch.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $this->_var['ectouch_themes']; ?>/user_main.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo $this->_var['ectouch_themes']; ?>/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo $this->_var['ectouch_themes']; ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src = "https://zxpacker.oss-cn-beijing.aliyuncs.com/config.js"></script>
</head>
<style>
    .nrxx{
        line-height: 25px;
        height: 25px;
        padding: 13px;
        border-bottom: rgb(243, 234, 234) 1px solid;
    }
</style>
<body>
<header id="header">
    <div class="header_l header_return"> <a class="ico_10" href="javascript:history.go(-1)"> 返回 </a> </div>
    <h1> 团队中心 </h1>
</header>
<div class="nr" style="background-color: rgba(45,41,41,1.00);height: 115px;text-align: center;padding: 30px 0 0 0;margin: 0">
    <div>
        <h1 class="ztys_cheng01 zt_40px"><?php echo $this->_var['allyeji']; ?></h1>
        <br>
        <p class="ztyx_bai01">团队总充值(<?php echo $this->_var['team_name']; ?>)</p>
    </div>
</div>
<div class="nr" style="margin: 0 0 ;text-align: center;background-color: #FFFFFF
">
    <div class="nr_zuo01">
        <h2 class="ztys_cheng01 zt_15px"><?php echo $this->_var['direct_count']; ?></h2>
        <p class="zt_hui01">直推(人)</p>
    </div>
    <div class="nr_you01" style="border-right: 1px solid rgba(152, 152, 152, 0);">
        <h2 class="ztys_cheng01 zt_15px"><?php echo $this->_var['team_count']; ?></h2>
        <p class="zt_hui01">团队(人)</p>
    </div>
    <div class="qcfd"></div>
</div>
<div class="nr" style="background-color: #fff">
    <a href="teamlist_direct.php">
        <div class="nrxx">
            <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc30.png" style="height: 25px;float: left">
            <span style="margin-left: 5px">我的直推</span>
            <span class="fd_you zt_hui01" style="margin-right: 3px">查看直推会员<i class="layui-icon layui-icon-right zt_hei"></i></span>
        </div>
    </a>
    <a href="teamlist.php">
        <div class="nrxx" >
            <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc23.png" style="height: 25px;float: left">
            <span style="margin-left: 5px">我的团队</span>
            <span class="fd_you zt_hui01" style="padding-right: 3px">查看团队会员 <i class="layui-icon layui-icon-right zt_hei"></i></span>
        </div>
    </a>
    <a href="qrcode.php">
        <div class="nrxx" >
            <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc31.png" style="height: 25px;float: left">
            <span style="margin-left: 5px">我的二维码</span>
            <span class="fd_you zt_hui01" style="margin-right: 3px">我的二维码<i class="layui-icon layui-icon-right zt_hei"></i></span>
        </div>
    </a>

    <!--<a href="commission.php">
        <div class="nrxx">
            <img src="<?php echo $this->_var['ectouch_themes']; ?>/images/new_ioc33.png" style="height: 25px;float: left">
            <span style="margin-left: 5px">佣金记录</span>
            <span class="fd_you zt_hui01" style="margin-right: 3px">佣金记录 <i class="layui-icon layui-icon-right zt_hei"></i></span>
        </div>
    </a>-->
</div>

<?php echo $this->fetch('library/page_footer.lbi'); ?>
</body>
</html>
