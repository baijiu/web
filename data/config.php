<?php
// database host
$db_host   = "localhost";

$db_port   = 3306;

// database name
$db_name   = "baijiu";

// database username
$db_user   = "root";

// database password
$db_pass   = "root";

// table prefix
$prefix    = "ecs_";

$timezone    = "Asia/Shanghai";

$cookie_path    = "/";

$cookie_domain    = "";

$session = "1440";

define('EC_CHARSET','utf-8');

//define('ADMIN_PATH','admin');

define('AUTH_KEY', 'this is a key');

define('OLD_AUTH_KEY', '');

define('API_TIME', '2020-06-30 20:48:15');


//时区设置
$config['TIMEZONE'] = $timezone; //时区设置

//日志和错误调试配置
$config['DEBUG'] = true; //是否开启调试模式，true开启，false关闭
$config['LOG_ON'] = false; //是否开启出错信息保存到文件，true开启，false不开启
$config['LOG_PATH'] = './data/log/'; //出错信息存放的目录，出错信息以天为单位存放，一般不需要修改
$config['ERROR_URL'] = ''; //出错信息重定向页面，为空采用默认的出错页面，一般不需要修改

//网址配置
$config['URL_REWRITE_ON'] = false; //是否开启重写，true开启重写,false关闭重写
$config['URL_MODULE_DEPR'] = '/'; //模块分隔符，一般不需要修改
$config['URL_ACTION_DEPR'] = '/'; //操作分隔符，一般不需要修改
$config['URL_PARAM_DEPR'] = '/'; //参数分隔符，一般不需要修改
$config['URL_HTML_SUFFIX'] = '.html'; //伪静态后缀设置，，例如 .html ，一般不需要修改
$config['URL_HTTP_HOST'] = ''; //设置网址域名

//模块配置
$config['MODULE_PATH'] = './module/'; //模块存放目录，一般不需要修改
$config['MODULE_SUFFIX'] = 'Mod.class.php'; //模块后缀，一般不需要修改
$config['MODULE_INIT'] = 'init.php'; //初始程序，一般不需要修改
$config['MODULE_DEFAULT'] = 'index'; //默认模块，一般不需要修改
$config['MODULE_EMPTY'] = 'empty'; //空模块，一般不需要修改	

//操作配置
$config['ACTION_DEFAULT'] = 'index'; //默认操作，一般不需要修改
$config['ACTION_EMPTY'] = '_empty'; //空操作，一般不需要修改

//模型配置
$config['MODEL_PATH'] = './model/'; //模型存放目录，一般不需要修改
$config['MODEL_SUFFIX'] = 'Model.class.php'; //模型后缀，一般不需要修改

//静态页面缓存
$config['HTML_CACHE_ON'] = false; //是否开启静态页面缓存，true开启.false关闭
$config['HTML_CACHE_PATH'] = './data/html_cache/'; //静态页面缓存目录，一般不需要修改
$config['HTML_CACHE_RULE']['index']['index'] = 3600; //缓存时间,单位：秒

//数据库配置
$config['DB_TYPE'] = 'mysql'; //数据库类型，一般不需要修改
$config['DB_HOST'] = $db_host;//数据库主机，一般不需要修改
$config['DB_PORT'] = $db_port;//数据库端口，mysql默认是3306，一般不需要修改
$config['DB_USER'] = $db_user;//数据库用户名
$config['DB_PWD'] = $db_pass;//数据库密码
$config['DB_NAME'] = $db_name;//数据库名
$config['DB_CHARSET'] = 'utf8';//数据库编码，一般不需要修改
$config['DB_PREFIX'] = $prefix;//数据库前缀
$config['DB_CACHE_ON'] = false; //是否开启数据库缓存，true开启，false不开启
$config['DB_CACHE_TYPE'] = 'Memcache'; //缓存类型，FileCache或Memcache或SaeMemcache
$config['DB_CACHE_TIME'] = 600; //缓存时间,0不缓存，-1永久缓存,单位：秒
$config['DB_RUN_QUERY'] = 0; //运行sql语句
$config['DB_BACKUP_PATH'] = './data/backups/'; //数据库备份目录
$config['DB_PCONNECT'] = false; //true表示使用永久连接，false表示不使用永久连接，一般不使用永久连接

//文件缓存配置
$config['DB_CACHE_PATH'] = './data/db_cache/'; //数据库查询内容缓存目录，地址相对于入口文件，一般不需要修改
$config['DB_CACHE_CHECK'] = false; //是否对缓存进行校验，一般不需要修改
$config['DB_CACHE_FILE'] = 'cachedata'; //缓存的数据文件名
$config['DB_CACHE_SIZE'] = '15M'; //预设的缓存大小，最小为10M，最大为1G
$config['DB_CACHE_FLOCK'] = true; //是否存在文件锁，设置为false，将模拟文件锁,，一般不需要修改

//memcache配置，可配置多台memcache服务器
$config['MEM_SERVER'] = array(array('127.0.0.1', 11211), array('localhost', 11211));
$config['MEM_GROUP'] = 'db';

// 模板目录
$config['TPL_TEMPLATE_PATH'] = './template/'; //模板目录，包含主题目录
$config['TPL_TEMPLATE_SUFFIX'] = '.html'; //模板后缀，一般不需要修改
$config['TPL_CACHE_ON'] = false; //是否开启模板缓存，true开启,false不开启
$config['TPL_CACHE_TYPE'] = ''; //数据缓存类型，为空或Memcache或SaeMemcache，其中为空为普通文件缓存

//普通文件缓存
$config['TPL_CACHE_PATH'] = './data/tpl_cache/'; //模板缓存目录，一般不需要修改
$config['TPL_CACHE_SUFFIX'] = '.php'; //模板缓存后缀,一般不需要修改

//memcache配置
$config['MEM_SERVER'] = array(array('127.0.0.1', 11211), array('localhost', 11211));
$config['MEM_GROUP'] = 'tpl';

//自动加载扩展目录
$config['AUTOLOAD_DIR'] = array(); //自动加载扩展目录
$config['CLASS_REFRESH'] = true; //后台类自动刷新模式
$config['RUN_LOG_ON'] = false; //后台运行日志

//是否缓存权限信息
$config['AUTH_POWER_CACHE'] = false; //设置false,每次从数据库读取，开发时建议设置为false

$config['site_name'] = ''; //站点名称
$config['site_url']="http://".$_SERVER['HTTP_HOST']."/"; //电脑版地址
$config['site_email'] = ''; //管理员邮箱
$config['site_phone'] = ''; //公司电话
$config['site_fax'] = ''; //传真号码
$config['site_address'] = ''; //公司地址
$config['site_postcode'] = ''; //公司邮编
$config['site_image_maxwidth'] = '600'; //内容图片最大宽度
$config['site_summary_length'] = '360'; //内容摘要长度
$config['site_copyright'] = 'Copyright © 2014, All Rights Reserved'; //版权信息
$config['site_statcode'] = ''; //第三方统计代码
$config['site_icp'] = ''; //ICP 备案信息
$config['site_title'] = ''; //首页标题
$config['site_keywords'] = ''; //站点关键字
$config['site_description'] = ''; //站点描述
$config['site_closed'] = false; //是否关闭网站
$config['site_closedreason'] = '网站暂未开放。'; //关闭网站的原因
$config['site_themes'] = 'default'; //模板主题，一般不需要修改

$config['site_pagebreak'] = '&lt;hr style=&quot;page-break-after:always;&quot; class=&quot;ke-pagebreak&quot; /&gt;'; //内容分页标签
$config['site_attachment'] = 'data/attachment/'; //附件目录
$config['site_pagesize'] = 12; //网站分页数量
//邮箱服务器配置
$config['SMTP_HOST'] = 'smtp.163.com'; //smtp服务器地址
$config['SMTP_PORT'] = '25'; //smtp服务器端口
$config['SMTP_SSL'] = false; //是否启用SSL安全连接，gmail需要启用sll安全连接
$config['SMTP_USERNAME'] = ''; //smtp服务器帐号，如：你的qq邮箱
$config['SMTP_PASSWORD'] = '!'; //smtp服务器帐号密码，如你的qq邮箱密码
$config['SMTP_FROM_TO'] = ''; //发件人邮件地址
$config['SMTP_FROM_NAME'] = '系统邮件'; //发件人姓名
$config['SMTP_CHARSET'] = 'utf-8'; //发送的邮件内容编码
//EcTouch版本
$config['ver'] = '0.1'; //版本号
$config['ver_name'] = ''; //版本名称
$config['ver_date'] = ''; //版本时间
$config['ver_author'] = '';
$config['ver_email'] = '';
$config['ver_url'] = '';
?>
