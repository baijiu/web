<?php
/* 
*甜心  100分销微分销分销中心
*/
define('IN_ECTOUCH', true);

require(dirname(__FILE__) . '/include/init.php');
require(ROOT_PATH . 'include/lib_weixintong.php');
/* 载入语言文件 */
require_once(ROOT_PATH . 'lang/' .$_CFG['lang']. '/user.php');
$user_id = $_SESSION['user_id'];
if(empty($_SESSION['user_id'])){
    ecs_header("Location: user.php\n");
    exit;
}
$action  = isset($_REQUEST['act']) ? trim($_REQUEST['act']) : 'default';
assign_template();
$position = assign_ur_here(0, $_LANG['user_center']);
$smarty->assign('ur_here',    $position['ur_here']);
$smarty->assign('helps',      get_shop_help());        // 网店帮助
$smarty->assign('data_dir',   DATA_DIR);   // 数据目录
$smarty->assign('action',     $action);
$smarty->assign('lang',       $_LANG);


//团队中心欢迎页
if ($action == 'default')
{
    $direct_count = $db->getone("select count(*) from ecs_users where parent_id = '".$user_id."'");
    $user_info = $db->getrow("select * from ecs_users where user_id = '".$user_id."'");
    $team_count = team_count($user_id);

    $gain_all = $db->getone("select sum(gain) from ecs_commission_log where user_id = '".$user_id."'");
    $gain_all = $gain_all?$gain_all:0;

    $all_uid = all_uid($user_id);
    //$allyeji = $db->getone("select sum(amount) from ecs_product_order where user_id in($all_uid) and is_pay = 1");

    $allyeji = $db->getone("select sum(amount) from ecs_recharge where user_id in($all_uid) and paystate = 1");
    $allyeji = $allyeji?$allyeji:0;

    $team_rank =  array('普通团长','一级团长','二级团长','三级团长','四级团长','五级团长');
    $team_name =$team_rank[$user_info['team_level']];
        $smarty->assign('team_name',     $team_name);
    $smarty->assign('allyeji',     $allyeji);
    $smarty->assign('gain_all',     $gain_all);
    $smarty->assign('team_count',     $team_count);
    $smarty->assign('direct_count',     $direct_count);
    $smarty->assign('user',     $user_info);
    $smarty->display('distribute.dwt');
}


function team_count($auid){
    $up_uid = "'$auid'";
    $all_count = 0;
    do{
        $count = 0;
        $sql = "SELECT user_id FROM ecs_users WHERE parent_id IN($up_uid)";
        $query = $GLOBALS['db']->query($sql);
        $up_uid = '';
        while ($rt = $GLOBALS['db']->fetch_array($query))
        {
            $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
            $count++;
        }
        $all_count += $count;
    }while($up_uid);
    return $all_count;
}



function all_uid($auid){
    $db =  $GLOBALS['db'];

    $up_uid = "'$auid'";
    $all_uid = "'$auid'";

    do{

        $sql = "SELECT user_id FROM ecs_users WHERE parent_id IN($up_uid)";
        $query = $db->query($sql);
        $up_uid = '';
        while ($rt = $db->fetch_array($query))
        {
            $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
            $all_uid .= $all_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
        }



    }while($up_uid);

    return $all_uid;
}



?>