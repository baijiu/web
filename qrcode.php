<?php
define('IN_ECTOUCH', true);
require(dirname(__FILE__) . '/include/init.php');
include_once(ROOT_PATH .'include/lib_clips.php');

$user_id = $_SESSION['user_id'];

if(!$_SESSION['user_id']){//没有登录商城账号
    ecs_header("Location: user.php\n");
    exit;
}

$qCodePath = 'http://qr.liantu.com/api.php?text=http://'.$_SERVER['HTTP_HOST'].'/user.php?act=register%26parent_id='.$user_id;

$bgimg = 'http://'.$_SERVER['HTTP_HOST'].'/images/qrcode_bg.png';//背景图

$dir = $_SERVER['DOCUMENT_ROOT'].'/images/qrcode/'.$_SESSION['user_id'].'.png';//合成后的图片
hc3($dir,$qCodePath,$bgimg);

$qr_path = 'http://'.$_SERVER['HTTP_HOST'].'/images/qrcode/'.$_SESSION['user_id'].'.png';

$smarty->assign('qr_path', $qr_path);

$smarty->display('qrcode.dwt');



 function downloadimageformweixin($url) {  
          
        $ch = curl_init ();  
        curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );  
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );  
        curl_setopt ( $ch, CURLOPT_URL, $url );  
        ob_start ();  
        curl_exec ( $ch );  
        $return_content = ob_get_contents ();  
        ob_end_clean ();  
         
							
        $return_code = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );  
        return $return_content;  
    }


//文字合成图片
function hc1($dir,$src,$content1='',$content2='',$content3='',$content4='',$content5='',$content6='')
{
    //1.获取图片信息
    $info = getimagesize($src);
    //2.通过编号获取图像类型
    $type = image_type_to_extension($info[2],false);
    //3.在内存中创建和图像类型一样的图像
    $fun = "imagecreatefrom".$type;
    //4.图片复制到内存
    $image = $fun($src);
    $background = imagecolorallocate($image, 255, 255, 255);
    imagecolortransparent($image, $background);
    //imagealphablending($image, false);
    imagesavealpha($image, true);

    /*操作图片*/
    //1.设置字体的路径
    $font = "msyh.ttf";
    //2.填写水印内容
    $size=15;

    //3.设置字体颜色和透明度
    $color = imagecolorallocatealpha($image, 255, 255,255,255);
    //4.写入文字
    imagettftext($image, "$size", 0, 90, 255, $color, $font, $content1);

    //设置字体颜色和透明度
    $color = imagecolorallocatealpha($image, 255, 255,255,0);
    //写入文字
    imagettftext($image, "$size", 90, 40, 250, $color, $font, $content2);

    //设置字体颜色和透明度
    $color = imagecolorallocatealpha($image, 255, 255,255,0);
    //写入文字
    imagettftext($image,"$size", 130, 112,360, $color, $font, $content3);

    //设置字体颜色和透明度
    $color = imagecolorallocatealpha($image, 255, 255,255,0);
    //写入文字
    imagettftext($image,"$size", 0, 160, 400, $color, $font, $content4);

    //设置字体颜色和透明度
    $color = imagecolorallocatealpha($image, 255, 255,255,0);
    //写入文字
    imagettftext($image, "$size", 40, 310, 380, $color, $font, $content5);

    //设置字体颜色和透明度
    $color = imagecolorallocatealpha($image, 255, 255,255,0);
    //写入文字
    imagettftext($image,"$size", 0, 185, 40, $color, $font, $content6);
    //浏览器输出
    /*header("Content-type:".$info['mime']);
    $fun = "image".$type;
    $fun($image); */
    //保存图片
    $ok = imagepng($image, $dir);
    /*销毁图片*/
    imagedestroy($image);
    if($ok){
        return true;
    }else{
        return false;
    }
}

//推片合成图片
function hc3($dir,$qCodePath,$bigImgPath)
{
    //$logo  = imagecreatefromstring(downloadimageformweixin($qCodePath));
    $logo  = imagecreatefromstring(file_get_contents($qCodePath));
    if ($logo !== FALSE) {
        $QR = imagecreatefromstring(file_get_contents($bigImgPath));
        $QR_width = imagesx($QR);//背景图片宽度
        $QR_height = imagesy($QR);//背景图片高度
        $logo_width = imagesx($logo);//logo图片宽度
        $logo_height = imagesy($logo);//logo图片高度
        $logo_qr_width = $QR_width / 5;
        $scale = $logo_width/$logo_qr_width;
        $logo_qr_height = $logo_height/$scale;
        $from_width = ($QR_width - $logo_qr_width) / 2;
        imagecopyresampled($QR, $logo, 210, 780, 0, 0, 330,
            330, $logo_width, $logo_height);
        //浏览器输出
        /*header('Content-Type:image/png');
        imagepng($QR);*/
        //保存图片
        imagepng($QR, $dir);
        imagedestroy($QR);

    }
}

//print_r($qr_path);exit;

?>	








